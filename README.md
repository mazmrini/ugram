# GLO3112-H18 : ugram-team-10
## Membres
- Thami Amrani
- Marc-Antoine Côté Ortiz
- Esma Mouine
- Mazine Mrini
- Gabriel Warren 
##  Compilation du front-end
```bash
  npm install 
  npm start 
```
##  Compilation du back-end
```bash
  mvn clean install
  mvn exec:java
  * Il est nécessaire de configurer les clés d'environnement d'AWS
```


## Lien
# Back-end : 
  http://ugram-team-10-prod.us-east-1.elasticbeanstalk.com/
# Front-end :
  http://web2ugramteam10.s3-website.us-east-2.amazonaws.com/
# Google Analytics :
  Voir le pdf **google-analytics** à la racine du projet. Au besoin, le fichier contient aussi un lien vers le rapport en ligne.
## Documentation 
    http://ugram-team-10-prod.us-east-1.elasticbeanstalk.com/swagger-ui.html
## Autres
Lors qu'un utilisateur est supprimer, nous avons configuré un lambda pour supprimer toutes les photos de l'utilisateur 

Pour le resizing d'une photo, nous avons configuré un lambda
## Fonctionnalités
- (5) L'usager doit pouvoir rechercher par mot clé ou description avec autocomplétion

  Sur la page de recherche 
- (5) L'usager doit pouvoir prendre une photo avec sa webcam

  Lors de l'ajout d'une photo 
- (5) L'usager doit pouvoir consulter les mots-clés les plus populaires

  Sur la page de recherche 

