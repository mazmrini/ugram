package com.ulaval.web.ugram.api.controller;

import com.ulaval.web.ugram.service.login.LoginService;
import com.ulaval.web.ugram.service.user.UserService;
import com.ulaval.web.ugram.service.user.dto.CreateUserApiDto;
import com.ulaval.web.ugram.service.user.dto.UserApiDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@ApiIgnore
@RestController
public class AuthController {

    @Autowired
    private LoginService loginService;

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.POST, value = "/login")
    public ResponseEntity<UserApiDto> login(@RequestHeader HttpHeaders headers) {
        UserApiDto userDto = loginService.login(headers);

        return ResponseEntity.ok(userDto);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/sign-up")
    public ResponseEntity signUp(@RequestHeader HttpHeaders headers,
                                 @RequestBody CreateUserApiDto requestUserDto) {
        userService.createUser(headers, requestUserDto);

        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
