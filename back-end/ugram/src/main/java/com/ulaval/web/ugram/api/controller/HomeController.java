package com.ulaval.web.ugram.api.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    public static final String WELCOME_MESSAGE = "Welcome to Ugram API";

    @RequestMapping(method = RequestMethod.GET, value = "/")
    public ResponseEntity<String> getHome() {
        return ResponseEntity.ok(WELCOME_MESSAGE);
    }
}
