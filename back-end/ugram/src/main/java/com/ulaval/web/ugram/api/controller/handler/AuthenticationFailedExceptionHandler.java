package com.ulaval.web.ugram.api.controller.handler;

import com.ulaval.web.ugram.service.exception.AuthenticationFailedException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class AuthenticationFailedExceptionHandler {

    @ExceptionHandler(AuthenticationFailedException.class)
    public ResponseEntity handleException(AuthenticationFailedException exception) {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }
}
