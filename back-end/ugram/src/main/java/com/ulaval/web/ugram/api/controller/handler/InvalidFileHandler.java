package com.ulaval.web.ugram.api.controller.handler;

import com.ulaval.web.ugram.util.converter.exception.InvalidFileException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class InvalidFileHandler {

    @ExceptionHandler(InvalidFileException.class)
    public ResponseEntity handleMissingParameterException(InvalidFileException exception) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
}
