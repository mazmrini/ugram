package com.ulaval.web.ugram.api.controller.handler;

import com.ulaval.web.ugram.domain.exception.InvalidParameterException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class InvalidParameterHandler {

    @ExceptionHandler(InvalidParameterException.class)
    public ResponseEntity handleMissingParameterException(InvalidParameterException exception) {
        return ResponseEntity.badRequest().build();
    }
}
