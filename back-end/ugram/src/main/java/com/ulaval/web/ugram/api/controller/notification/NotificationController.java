package com.ulaval.web.ugram.api.controller.notification;

import com.ulaval.web.ugram.service.notification.NotificationService;
import com.ulaval.web.ugram.service.notification.dto.NotificationApiDto;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NotificationController {

    private NotificationService notificationService;

    @Autowired
    public NotificationController(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/notification/{userId}")
    public ResponseEntity getNotification(
        @PathVariable("userId") String userId,
        @RequestHeader HttpHeaders headers) {

        List<NotificationApiDto> notificationApiDtos = notificationService.getNotification(headers, userId);
        return ResponseEntity.status(HttpStatus.OK).body(notificationApiDtos);
    }
}
