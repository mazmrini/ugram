package com.ulaval.web.ugram.api.controller.picture.global;

import com.ulaval.web.ugram.service.picture.PictureService;
import com.ulaval.web.ugram.service.picture.dto.PictureApiDto;
import com.ulaval.web.ugram.service.picture.tag.dto.TagApiDto;
import com.ulaval.web.ugram.util.pagination.PaginationResponse;
import com.ulaval.web.ugram.util.pagination.Paginator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PictureController {

    private static final String PAGE = "1";
    private static final String PER_PAGE = "6";
    private static final String QUANTITY = "10";
    private PictureService pictureService;
    private Paginator<PictureApiDto> paginator;

    @Autowired
    public PictureController(PictureService pictureService, Paginator paginator) {
        this.pictureService = pictureService;
        this.paginator = paginator;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/pictures")
    public ResponseEntity getPictures(
        @RequestParam(value = "page", required = false, defaultValue = PAGE) int page,
        @RequestParam(value = "perPage", required = false, defaultValue = PER_PAGE) int perPage) {
        List<PictureApiDto> pictureApiDto = pictureService.getPictures();
        PaginationResponse paginationResponse = paginator.paginate(pictureApiDto, page, perPage);
        return ResponseEntity.status(HttpStatus.OK).body(paginationResponse);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/pictures/tags")
    public ResponseEntity getTrendingTags(
        @RequestParam(value = "quantity", required = false, defaultValue = QUANTITY) int quantity) {
        List<TagApiDto> allTags = pictureService.getTags();

        quantity = quantity > allTags.size() ? allTags.size() : quantity;

        return ResponseEntity.status(HttpStatus.OK).body(allTags.subList(0, quantity));
    }
}
