package com.ulaval.web.ugram.api.controller.picture.user;

import com.ulaval.web.ugram.service.picture.PictureService;
import com.ulaval.web.ugram.service.picture.dto.PictureApiDto;
import com.ulaval.web.ugram.service.picture.dto.UpdatePictureApiDto;
import com.ulaval.web.ugram.service.picture.dto.UploadPictureDto;
import com.ulaval.web.ugram.service.picture.dto.UploadPictureResponseDto;
import com.ulaval.web.ugram.service.user.dto.PutCommentApiDto;
import com.ulaval.web.ugram.service.user.dto.PutLikeApiDto;
import com.ulaval.web.ugram.util.pagination.PaginationResponse;
import com.ulaval.web.ugram.util.pagination.Paginator;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class UserPictureController {

    private static final String PAGE = "1";
    private static final String PER_PAGE = "6";
    private PictureService pictureService;
    private Paginator<PictureApiDto> paginator;

    @Autowired
    public UserPictureController(PictureService pictureService, Paginator paginator) {
        this.pictureService = pictureService;
        this.paginator = paginator;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/users/{userId}/pictures")
    @ResponseBody
    public ResponseEntity postPictures(@PathVariable("userId") String userId,
                                       @RequestHeader HttpHeaders headers,
                                       @RequestParam("file") MultipartFile file,
                                       @RequestParam(required = false, value = "description") String description,
                                       @RequestParam(required = false, value = "mentions") List<String> mentions,
                                       @RequestParam(required = false, value = "tags") List<String> tags)
        throws IOException {
        UploadPictureDto uploadPictureDto = new UploadPictureDto();
        uploadPictureDto.userId = userId;
        uploadPictureDto.file = file;
        uploadPictureDto.description = description;
        handleAbsentParameters(mentions, tags, uploadPictureDto);

        UploadPictureResponseDto uploadPictureResponseDto = pictureService.postPicture(uploadPictureDto, headers);

        return ResponseEntity.status(HttpStatus.OK).body(uploadPictureResponseDto);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/users/{userId}/pictures/{pictureId}")
    public ResponseEntity getPicturesByPictureId(
        @PathVariable("userId") String userId,
        @PathVariable("pictureId") long pictureId) {

        PictureApiDto pictureApiDto = pictureService.getPictureByPictureId(userId, pictureId);
        return ResponseEntity.status(HttpStatus.OK).body(pictureApiDto);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/users/{userId}/pictures/{pictureId}")
    public ResponseEntity deletePictureByPictureId(@RequestHeader HttpHeaders headers,
                                                   @PathVariable("userId") String userId,
                                                   @PathVariable("pictureId") long pictureId) {
        pictureService.deletePictureByPictureId(headers, userId, pictureId);

        return ResponseEntity.noContent().build();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/users/{userId}/pictures")
    public ResponseEntity getPicturesByUserId(
        @PathVariable("userId") String userId,
        @RequestParam(value = "page", required = false, defaultValue = PAGE) int page,
        @RequestParam(value = "perPage", required = false, defaultValue = PER_PAGE) int perPage) {

        List<PictureApiDto> pictureApiDto = pictureService.getPicturesByUserId(userId);
        PaginationResponse paginationResponse = paginator.paginate(pictureApiDto, page, perPage);
        return ResponseEntity.status(HttpStatus.OK).body(paginationResponse);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/users/{userId}/pictures/{pictureId}")
    public ResponseEntity<PictureApiDto> putPicture(@RequestHeader HttpHeaders headers,
                                                    @PathVariable("userId") String userId,
                                                    @PathVariable("pictureId") long pictureId,
                                                    @RequestBody UpdatePictureApiDto updatePictureApiDto) {
        PictureApiDto pictureApiDto = pictureService.putPicture(headers, userId, pictureId, updatePictureApiDto);

        return ResponseEntity.ok(pictureApiDto);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/users/{userId}/pictures/{pictureId}/comment")
    public ResponseEntity putComment(@RequestHeader HttpHeaders headers,
                                     @PathVariable("userId") String userId,
                                     @PathVariable("pictureId") long pictureId,
                                     @RequestBody PutCommentApiDto putCommentApiDto) {
        pictureService.putComment(userId, putCommentApiDto, pictureId, headers);

        return ResponseEntity.noContent().build();
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/users/{userId}/pictures/{pictureId}/like")
    public ResponseEntity putLike(@RequestHeader HttpHeaders headers,
                                  @PathVariable("userId") String userId,
                                  @PathVariable("pictureId") long pictureId,
                                  @RequestBody PutLikeApiDto putLikeApiDto) {
        pictureService.putLike(userId, putLikeApiDto, pictureId, headers);

        return ResponseEntity.noContent().build();
    }

    private void handleAbsentParameters(List<String> mentions, List<String> tags, UploadPictureDto uploadPictureDto) {
        if (mentions == null) {
            uploadPictureDto.mentions = new ArrayList<>();
        }
        else {
            uploadPictureDto.mentions = mentions;
        }

        if (tags == null) {
            uploadPictureDto.tags = new ArrayList<>();
        }
        else {
            uploadPictureDto.tags = tags;
        }
    }


}
