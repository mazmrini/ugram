package com.ulaval.web.ugram.api.controller.user;

import com.ulaval.web.ugram.service.user.UserService;
import com.ulaval.web.ugram.service.user.dto.UpdateUserApiDto;
import com.ulaval.web.ugram.service.user.dto.UserApiDto;
import com.ulaval.web.ugram.util.pagination.PaginationResponse;
import com.ulaval.web.ugram.util.pagination.Paginator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsersController {

    private static final String PAGE = "1";
    private static final String PER_PAGE = "6";

    private Paginator<UserApiDto> paginator;
    private UserService userService;

    @Autowired
    public UsersController(UserService userService,
                           Paginator paginator) {
        this.userService = userService;
        this.paginator = paginator;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/users")
    public ResponseEntity<PaginationResponse> getUsers(
        @RequestParam(value = "page", required = false, defaultValue = PAGE) int page,
        @RequestParam(value = "perPage", required = false, defaultValue = PER_PAGE) int perPage) {
        List<UserApiDto> usersApiDto = userService.getUsers();
        PaginationResponse paginationResponse = paginator.paginate(usersApiDto, page, perPage);

        return ResponseEntity.ok(paginationResponse);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/users/{userId}")
    public ResponseEntity<UserApiDto> getUser(@PathVariable("userId") String userId) {
        UserApiDto userApiDto = userService.getUser(userId);

        return ResponseEntity.ok(userApiDto);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/users/{userId}")
    public ResponseEntity<UserApiDto> putUser(@RequestHeader HttpHeaders headers,
                                              @PathVariable("userId") String userId,
                                              @RequestBody UpdateUserApiDto updateUserApiDto) {
        UserApiDto userApiDto = userService.putUser(headers, userId, updateUserApiDto);

        return ResponseEntity.ok(userApiDto);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/users/{userId}")
    public ResponseEntity deleteUser(@RequestHeader HttpHeaders headers,
                                     @PathVariable("userId") String userId) {
        userService.deleteUser(headers, userId);

        return ResponseEntity.noContent().build();
    }
}
