package com.ulaval.web.ugram.api.controller.user.handler;

import com.ulaval.web.ugram.config.RestException;
import com.ulaval.web.ugram.service.exception.FacebookAccountAlreadyLinkedException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class FacebookAccountAlreadyLinkedExceptionHandler {
    @ExceptionHandler(FacebookAccountAlreadyLinkedException.class)
    public ResponseEntity<RestException> handleException(FacebookAccountAlreadyLinkedException exception) {
        RestException restException = new RestException().withMessage(exception.getMessage());

        return ResponseEntity.badRequest().body(restException);
    }
}
