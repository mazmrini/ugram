package com.ulaval.web.ugram.api.controller.user.handler;

import com.ulaval.web.ugram.config.RestException;
import com.ulaval.web.ugram.domain.user.exception.ForbiddenException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ForbiddenHandler {

    @ExceptionHandler(ForbiddenException.class)
    public ResponseEntity<RestException> handleForbiddenExceptionException(ForbiddenException forbiddenException) {
        RestException restException = new RestException().withMessage(forbiddenException.getMessage());

        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(restException);
    }
}
