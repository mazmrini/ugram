package com.ulaval.web.ugram.api.controller.user.handler;

import com.ulaval.web.ugram.config.RestException;
import com.ulaval.web.ugram.infrastructure.exception.ItemNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ItemNotFoundHandler {

    @ExceptionHandler(ItemNotFoundException.class)
    public ResponseEntity<RestException> handleItemNotFoundException(ItemNotFoundException itemNotFoundException) {
        RestException restException = new RestException().withMessage(itemNotFoundException.getMessage());

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(restException);
    }
}
