package com.ulaval.web.ugram.api.controller.user.handler;

import com.ulaval.web.ugram.config.RestException;
import com.ulaval.web.ugram.service.exception.UsernameAlreadyExistException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class UsernameAlreadyExistHandler {

    @ExceptionHandler(UsernameAlreadyExistException.class)
    public ResponseEntity<RestException> handleException(UsernameAlreadyExistException exception) {
        RestException restException = new RestException().withMessage(exception.getMessage());

        return ResponseEntity.badRequest().body(restException);
    }
}
