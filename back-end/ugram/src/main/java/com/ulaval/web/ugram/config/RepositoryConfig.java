package com.ulaval.web.ugram.config;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.s3.AmazonS3;
import com.ulaval.web.ugram.domain.notification.NotificationRepository;
import com.ulaval.web.ugram.domain.picture.PictureRepository;
import com.ulaval.web.ugram.domain.user.UserRepository;
import com.ulaval.web.ugram.infrastructure.NoSqlRequestFactory;
import com.ulaval.web.ugram.infrastructure.notification.assembler.NotificationInfraAssembler;
import com.ulaval.web.ugram.infrastructure.notification.dao.NoSqlNotificationDao;
import com.ulaval.web.ugram.infrastructure.notification.dao.NotificationDao;
import com.ulaval.web.ugram.infrastructure.notification.repository.NoSqlNotificationRepository;
import com.ulaval.web.ugram.infrastructure.picture.NoSqlPictureRepository;
import com.ulaval.web.ugram.infrastructure.picture.assembler.PictureInfraAssembler;
import com.ulaval.web.ugram.infrastructure.picture.dao.NoSqlPictureDao;
import com.ulaval.web.ugram.infrastructure.picture.dao.PictureDao;
import com.ulaval.web.ugram.infrastructure.picture.dao.upload.PictureFileDao;
import com.ulaval.web.ugram.infrastructure.picture.dao.upload.S3PictureFileDao;
import com.ulaval.web.ugram.infrastructure.picture.tag.NoSqlTagDao;
import com.ulaval.web.ugram.infrastructure.picture.tag.TagDao;
import com.ulaval.web.ugram.infrastructure.picture.tag.assembler.TagInfraAssembler;
import com.ulaval.web.ugram.infrastructure.user.NoSqlUserRepository;
import com.ulaval.web.ugram.infrastructure.user.assembler.UserInfraAssembler;
import com.ulaval.web.ugram.infrastructure.user.dao.NoSqlUserDao;
import com.ulaval.web.ugram.infrastructure.user.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RepositoryConfig {

    @Autowired
    private AmazonDynamoDB amazonDynamoDB;

    @Autowired
    private DynamoDB dynamoDB;

    @Autowired
    private UserInfraAssembler userInfraAssembler;

    @Autowired
    private NoSqlRequestFactory noSqlRequestFactory;

    @Autowired
    private PictureInfraAssembler pictureInfraAssembler;
    @Autowired
    private TagInfraAssembler tagInfraAssembler;

    @Autowired
    private NotificationInfraAssembler notificationInfraAssembler;

    @Autowired
    private AmazonS3 amazonS3;

    @Bean
    public UserDao userDao() {
        return new NoSqlUserDao(userInfraAssembler, amazonDynamoDB, dynamoDB, noSqlRequestFactory);
    }

    @Bean
    public UserRepository userRepository() {
        return new NoSqlUserRepository(userDao(), userInfraAssembler);
    }

    @Bean
    public NotificationRepository notificationRepository() {
        return new NoSqlNotificationRepository(notificationDao(), notificationInfraAssembler);
    }

    @Bean
    public NotificationDao notificationDao() {
        return new NoSqlNotificationDao(dynamoDBMapper(), noSqlRequestFactory, dynamoDB);
    }

    @Bean
    public DynamoDBMapper dynamoDBMapper() {
        return new DynamoDBMapper(amazonDynamoDB);
    }

    @Bean
    public PictureDao pictureDao() {
        return new NoSqlPictureDao(pictureInfraAssembler, amazonDynamoDB, dynamoDB, noSqlRequestFactory);
    }

    @Bean
    public PictureFileDao pictureFileDao() {
        return new S3PictureFileDao(amazonS3);
    }

    @Bean
    public TagDao tagDao() {
        return new NoSqlTagDao(amazonDynamoDB, dynamoDB, noSqlRequestFactory, tagInfraAssembler);
    }

    @Bean
    public PictureRepository pictureRepository() {
        return new NoSqlPictureRepository(
            pictureDao(),
            userDao(),
            pictureInfraAssembler,
            pictureFileDao(),
            tagDao(),
            tagInfraAssembler);
    }
}
