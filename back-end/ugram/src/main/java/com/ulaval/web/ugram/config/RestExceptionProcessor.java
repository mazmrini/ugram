package com.ulaval.web.ugram.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice("com.ulaval.web.ugram")
public class RestExceptionProcessor {

    private static final Logger logger = LoggerFactory.getLogger(RestExceptionProcessor.class);

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public RestException unexpectedException(Exception exception) {
        String message = "Unexpected error";

        logger.error(message, exception);

        return new RestException().withMessage(message);
    }
}
