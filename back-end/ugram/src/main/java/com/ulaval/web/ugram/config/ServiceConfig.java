package com.ulaval.web.ugram.config;

import com.ulaval.web.ugram.service.AuthenticationService;
import com.ulaval.web.ugram.service.FacebookAuthenticationService;
import com.ulaval.web.ugram.service.dto.FacebookInformation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServiceConfig {

    @Bean
    public AuthenticationService<FacebookInformation> authenticationService() {
        return new FacebookAuthenticationService();
    }
}
