package com.ulaval.web.ugram.domain.exception;

public class InvalidParameterException extends RuntimeException {

    private static final String MESSAGE = "Missing parameter.";

    public InvalidParameterException() {
        super(MESSAGE);
    }
}
