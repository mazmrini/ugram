package com.ulaval.web.ugram.domain.notification;

import java.time.Instant;

public class Notification {

    private String userId;
    private String fromUserId;
    private long pictureId;
    private NotificationEnum notificationType;
    private long id;

    public Notification(String userId, String fromUserId, long pictureId, NotificationEnum notificationEnum) {
        this.id = Instant.now().toEpochMilli();
        this.userId = userId;
        this.fromUserId = fromUserId;
        this.pictureId = pictureId;
        this.notificationType = notificationEnum;
    }

    public String getUserId() {
        return userId;
    }

    public String getFromUserId() {
        return fromUserId;
    }

    public long getPictureId() {
        return pictureId;
    }

    public NotificationEnum getNotificationType() {
        return notificationType;
    }

    public long getId() {
        return id;
    }
}
