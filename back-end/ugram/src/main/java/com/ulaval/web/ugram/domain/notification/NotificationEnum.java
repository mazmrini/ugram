package com.ulaval.web.ugram.domain.notification;

public enum NotificationEnum {
    LIKE("LIKE"),
    COMMENT("COMMENT");

    private String notificationType;

    NotificationEnum(String notificationType) {
        this.notificationType = notificationType;
    }

    public String toString() {
        return notificationType;
    }
}