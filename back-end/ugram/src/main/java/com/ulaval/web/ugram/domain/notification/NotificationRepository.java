package com.ulaval.web.ugram.domain.notification;

import com.ulaval.web.ugram.infrastructure.notification.dto.NotificationsInfraDto;

public interface NotificationRepository {

    void addNotification(Notification notification);

    NotificationsInfraDto getNotifications(String userId);

    void deleteNotifications(String userId);

}
