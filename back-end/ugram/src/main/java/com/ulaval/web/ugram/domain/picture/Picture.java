package com.ulaval.web.ugram.domain.picture;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Picture {

    private List<String> likes;
    private List<Map> comments;
    private long id;
    private long createdDate;
    private String description;
    private List<String> mentions;
    private List<String> tags;
    private String url;
    private String userId;

    public Picture(long id,
                   long createdDate,
                   String description,
                   List<String> mentions,
                   List<String> tags,
                   String url,
                   String userId, List<Map> comments, List<String> likes) {
        this.id = id;
        this.createdDate = createdDate;
        this.description = description;
        this.mentions = mentions;
        this.tags = tags;
        this.url = url;
        this.userId = userId;
        this.comments = comments;
        this.likes = likes;
    }

    public Picture(String description,
                   List<String> mentions,
                   List<String> tags,
                   String userId) {
        this.id = Instant.now().toEpochMilli();
        this.createdDate = Instant.now().toEpochMilli();
        this.description = description;
        this.mentions = mentions;
        this.tags = tags;
        this.url = "";
        this.userId = userId;
        this.comments = new ArrayList<>();
        this.likes = new ArrayList<>();
    }

    public Picture(String description, List<String> mentions, List<String> tags, String userId, long id) {
        this.id = id;
        this.createdDate = Instant.now().toEpochMilli();
        this.description = description;
        this.mentions = mentions;
        this.tags = tags;
        this.url = "";
        this.userId = userId;
    }

    public List<String> getLikes() {
        return likes;
    }

    public List<Map> getComments() {
        return comments;
    }

    public long getId() {
        return id;
    }

    public long getCreatedDate() {
        return createdDate;
    }

    public String getDescription() {
        return description;
    }

    public List<String> getMentions() {
        return mentions;
    }

    public List<String> getTags() {
        return tags;
    }

    public String getUrl() {
        return url;
    }

    public String getUserId() {
        return userId;
    }

    public void updateUrl(String url) {
        this.url = url;
    }
}
