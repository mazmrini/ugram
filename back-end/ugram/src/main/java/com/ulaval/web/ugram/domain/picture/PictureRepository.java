package com.ulaval.web.ugram.domain.picture;

import com.ulaval.web.ugram.domain.picture.tag.Tag;
import java.io.File;
import java.util.List;

public interface PictureRepository {

    List<Picture> getPictures();

    void postPicture(Picture picture, File file);

    List<Picture> getPicturesByUserId(String userId);

    void deletePictureByPictureId(long pictureId, String userId);

    Picture getPictureByPictureId(String userId, long pictureId);

    void putPicture(Picture picture);

    List<Tag> getTags();

    void putComment(String userId, String fromUserId, long pictureId, String comment);

    void putLike(String userId, String fromUserId, long pictureId);
}
