package com.ulaval.web.ugram.domain.picture.tag;

public class Tag implements Comparable<Tag> {

    private String tag;
    private long usage;

    public Tag(String tag, long usage) {
        this.tag = tag;
        this.usage = usage;
    }

    public String getTag() {
        return tag;
    }

    public long getUsage() {
        return usage;
    }

    @Override
    public int compareTo(Tag otherTag) {
        return Long.compare(otherTag.getUsage(), this.usage);
    }
}
