package com.ulaval.web.ugram.domain.picture.validator;

import com.google.common.base.Strings;
import com.ulaval.web.ugram.domain.exception.InvalidParameterException;
import com.ulaval.web.ugram.service.picture.dto.UpdatePictureApiDto;
import com.ulaval.web.ugram.service.user.dto.PutCommentApiDto;
import com.ulaval.web.ugram.service.user.dto.PutLikeApiDto;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class PictureValidator {


    public void validateUpdatePictureApiDto(UpdatePictureApiDto updatePictureApiDto) {
        if (!isUpdatePictureApiDto(updatePictureApiDto)) {
            throw new InvalidParameterException();
        }
    }

    private boolean isUpdatePictureApiDto(UpdatePictureApiDto updatePictureApiDto) {
        return isDescriptionValid(updatePictureApiDto.description)
            || isMentionsValid(updatePictureApiDto.mentions)
            || isTagsValid(updatePictureApiDto.tags);
    }

    private boolean isDescriptionValid(String description) {
        return !Strings.isNullOrEmpty(description);
    }

    private boolean isMentionsValid(List<String> mentions) {
        return mentions != null;
    }

    private boolean isTagsValid(List<String> tags) {
        return tags != null;
    }

    public void validatePutCommentApiDto(PutCommentApiDto putCommentApiDto) {
        if (!isCommentValid(putCommentApiDto.comment) || !isFromUserIdValid(putCommentApiDto.fromUserId)) {
            throw new InvalidParameterException();
        }
    }

    private boolean isCommentValid(String comment) {
        return !Strings.isNullOrEmpty(comment);
    }

    public void validatePutLikeApiDto(PutLikeApiDto putLikeApiDto) {
        if (!isFromUserIdValid(putLikeApiDto.fromUserId)) {
            throw new InvalidParameterException();
        }
    }

    private boolean isFromUserIdValid(String fromUserId) {
        return !Strings.isNullOrEmpty(fromUserId);
    }
}
