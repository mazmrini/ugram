package com.ulaval.web.ugram.domain.user;

import com.ulaval.web.ugram.domain.user.exception.ForbiddenException;
import java.time.Instant;

public class User {

    private String id;
    private String authId;
    private String firstName;
    private String email;
    private String lastName;
    private long phoneNumber;
    private String pictureUrl;
    private long registrationDate;

    public User(String id,
                String authId,
                String firstName,
                String lastName,
                String email,
                long phoneNumber,
                String pictureUrl,
                long registrationDate) {
        this.id = id;
        this.authId = authId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.pictureUrl = pictureUrl;
        this.registrationDate = registrationDate;
    }

    public User(String id,
                String authId,
                String firstName,
                String lastName,
                String email,
                long phoneNumber,
                String pictureUrl) {
        this.id = id;
        this.authId = authId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.pictureUrl = pictureUrl;
        this.registrationDate = Instant.now().toEpochMilli();
    }

    public String getId() {
        return id;
    }

    public String getAuthId() {
        return authId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getEmail() {
        return email;
    }

    public String getLastName() {
        return lastName;
    }

    public long getPhoneNumber() {
        return phoneNumber;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public long getRegistrationDate() {
        return registrationDate;
    }

    public void validateAuthorization(String authId) {
        if (!this.authId.equals(authId)) {
            throw new ForbiddenException();
        }
    }
}
