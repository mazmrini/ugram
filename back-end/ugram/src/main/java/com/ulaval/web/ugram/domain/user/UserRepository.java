package com.ulaval.web.ugram.domain.user;

import java.util.List;

public interface UserRepository {

    List<User> fetchUsers();

    User fetchUser(String userId);

    User fetchUserByFacebookId(String facebookId);

    void deleteUser(String userId);

    void createUser(User user);

    void putUser(User user);
}
