package com.ulaval.web.ugram.domain.user.exception;

public class ForbiddenException extends RuntimeException {

    private static final String MESSAGE = "You can't execute this action.";

    public ForbiddenException() {
        super(MESSAGE);
    }
}
