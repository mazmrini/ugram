package com.ulaval.web.ugram.domain.user.validator;

import com.google.common.base.Strings;
import com.ulaval.web.ugram.domain.exception.InvalidParameterException;
import com.ulaval.web.ugram.service.user.dto.CreateUserApiDto;
import com.ulaval.web.ugram.service.user.dto.UpdateUserApiDto;
import org.springframework.stereotype.Component;

@Component
public class UserValidator {

    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-]+)*@"
        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final int PHONE_LENGHT = 10;

    public void validateUpdateUserApiDto(UpdateUserApiDto updateUserApiDto) {
        validateBaseParameters(updateUserApiDto.firstName,
            updateUserApiDto.lastName,
            updateUserApiDto.email,
            updateUserApiDto.phoneNumber);
    }

    public void validateCreateUserApiDto(CreateUserApiDto createUserApiDto) {
        validateBaseParameters(createUserApiDto.firstName,
            createUserApiDto.lastName,
            createUserApiDto.email,
            createUserApiDto.phoneNumber);
        validateId(createUserApiDto.id);
    }

    private void validateBaseParameters(String firstName, String lastName, String email, long phoneNumber) {
        if (Strings.isNullOrEmpty(firstName)
            || Strings.isNullOrEmpty(lastName)
            || !isValid(email)
            || String.valueOf(phoneNumber).length() != PHONE_LENGHT) {
            throw new InvalidParameterException();
        }
    }

    private void validateId(String id) {
        if (Strings.isNullOrEmpty(id)) {
            throw new InvalidParameterException();
        }
    }

    private boolean isValid(String email) {
        return !Strings.isNullOrEmpty(email) && email.matches(EMAIL_PATTERN);
    }
}
