package com.ulaval.web.ugram.infrastructure;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.spec.DeleteItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.model.Condition;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Component;

@Component
public class NoSqlRequestFactory {

    public ScanRequest createScanRequest() {
        return new ScanRequest();
    }

    public UpdateItemSpec createUpdateItemSpec() {
        return new UpdateItemSpec();
    }

    public DeleteItemSpec createDeleteItemSpec() {
        return new DeleteItemSpec();
    }

    public GetItemRequest createGetItemRequest() {
        return new GetItemRequest();
    }

    public Condition createCondition() {
        return new Condition();
    }

    public Map<String, Condition> createScanFilter() {
        return new HashMap<>();
    }

    public Item createItem() {
        return new Item();
    }
}
