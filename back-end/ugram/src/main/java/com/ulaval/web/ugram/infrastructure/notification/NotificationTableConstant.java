package com.ulaval.web.ugram.infrastructure.notification;

public class NotificationTableConstant {

    public static final String TABLE_NAME = "Notification";
    public static final String PRIMARY_KEY = "userId";
}
