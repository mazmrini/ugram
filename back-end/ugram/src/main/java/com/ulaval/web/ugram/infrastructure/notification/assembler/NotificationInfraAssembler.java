package com.ulaval.web.ugram.infrastructure.notification.assembler;

import com.ulaval.web.ugram.domain.notification.Notification;
import com.ulaval.web.ugram.infrastructure.notification.dto.NotificationInfraDto;
import org.springframework.stereotype.Component;

@Component
public class NotificationInfraAssembler {

    public NotificationInfraDto assemble(Notification notification) {
        NotificationInfraDto notificationInfraDto = new NotificationInfraDto();
        notificationInfraDto.setId(notification.getId());
        notificationInfraDto.setFromUserId(notification.getFromUserId());
        notificationInfraDto.setPictureId(notification.getPictureId());
        notificationInfraDto.setNotificationType(notification.getNotificationType().toString());
        return notificationInfraDto;
    }
}
