package com.ulaval.web.ugram.infrastructure.notification.dao;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.PaginatedQueryList;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.DeleteItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.ulaval.web.ugram.infrastructure.NoSqlRequestFactory;
import com.ulaval.web.ugram.infrastructure.notification.NotificationTableConstant;
import com.ulaval.web.ugram.infrastructure.notification.dto.NotificationsInfraDto;
import java.util.ArrayList;
import java.util.HashMap;
import org.springframework.stereotype.Component;

@Component
public class NoSqlNotificationDao implements NotificationDao {

    private DynamoDB dynamoDB;
    private NoSqlRequestFactory noSqlRequestFactory;
    private DynamoDBMapper dynamoMapper;
    private Table table;

    public NoSqlNotificationDao(DynamoDBMapper dynamoMapper, NoSqlRequestFactory noSqlRequestFactory,
                                DynamoDB dynamoDB) {
        this.dynamoMapper = dynamoMapper;
        this.noSqlRequestFactory = noSqlRequestFactory;
        this.dynamoDB = dynamoDB;
        table = dynamoDB.getTable(NotificationTableConstant.TABLE_NAME);
    }

    @Override
    public void addNotification(NotificationsInfraDto notificationsInfraDto) {
        dynamoMapper.save(notificationsInfraDto);
    }

    @Override
    public NotificationsInfraDto getNotifications(String userId) {
        HashMap<String, AttributeValue> eav = new HashMap<>();
        eav.put(":u", new AttributeValue().withS(userId));

        DynamoDBQueryExpression<NotificationsInfraDto> queryExpression
            = new DynamoDBQueryExpression<NotificationsInfraDto>()
            .withKeyConditionExpression("userId = :u")
            .withExpressionAttributeValues(eav);
        PaginatedQueryList<NotificationsInfraDto> notifications
            = dynamoMapper.query(NotificationsInfraDto.class, queryExpression);

        return reformatNotification(notifications);
    }

    @Override
    public void deleteNotifications(String userId) {
        DeleteItemSpec deletePictureRequest = createDeletePictureRequest(userId);
        executeDeleteRequest(deletePictureRequest);
    }

    private NotificationsInfraDto reformatNotification(PaginatedQueryList<NotificationsInfraDto> notifications) {
        NotificationsInfraDto notificationsInfraDto;
        if (notifications.size() != 0) {
            notificationsInfraDto = notifications.get(0);
        }
        else {
            notificationsInfraDto = new NotificationsInfraDto();
            notificationsInfraDto.setNotifications(new ArrayList<>());
        }
        return notificationsInfraDto;
    }

    private void executeDeleteRequest(DeleteItemSpec deleteItemSpec) {
        try {
            table.deleteItem(deleteItemSpec);
        }
        catch (Exception exception) {
            // No notification found
        }
    }

    private DeleteItemSpec createDeletePictureRequest(String userId) {
        return noSqlRequestFactory.createDeleteItemSpec()
            .withPrimaryKey(NotificationTableConstant.PRIMARY_KEY, userId)
            .withConditionExpression("userId = :val")
            .withValueMap(new ValueMap().withString(":val", userId));
    }

}
