package com.ulaval.web.ugram.infrastructure.notification.dao;

import com.ulaval.web.ugram.infrastructure.notification.dto.NotificationsInfraDto;

public interface NotificationDao {

    void addNotification(NotificationsInfraDto notificationsInfraDto);

    NotificationsInfraDto getNotifications(String userId);

    void deleteNotifications(String userId);
}
