package com.ulaval.web.ugram.infrastructure.notification.dto;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import java.util.List;

@DynamoDBTable(tableName = "Notification")
public class NotificationsInfraDto {

    public String userId;
    public List<NotificationInfraDto> notifications;

    @DynamoDBHashKey
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @DynamoDBAttribute
    public List<NotificationInfraDto> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<NotificationInfraDto> notifications) {
        this.notifications = notifications;
    }

    public void addNotification(NotificationInfraDto notificationInfraDto) {
        notifications.add(notificationInfraDto);
    }
}
