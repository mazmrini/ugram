package com.ulaval.web.ugram.infrastructure.notification.repository;

import com.ulaval.web.ugram.domain.notification.Notification;
import com.ulaval.web.ugram.domain.notification.NotificationRepository;
import com.ulaval.web.ugram.infrastructure.notification.assembler.NotificationInfraAssembler;
import com.ulaval.web.ugram.infrastructure.notification.dao.NotificationDao;
import com.ulaval.web.ugram.infrastructure.notification.dto.NotificationInfraDto;
import com.ulaval.web.ugram.infrastructure.notification.dto.NotificationsInfraDto;
import org.springframework.stereotype.Repository;

@Repository
public class NoSqlNotificationRepository implements NotificationRepository {

    private NotificationInfraAssembler notificationInfraAssembler;
    private NotificationDao notificationDao;

    public NoSqlNotificationRepository(NotificationDao notificationDao,
                                       NotificationInfraAssembler notificationInfraAssembler) {
        this.notificationDao = notificationDao;
        this.notificationInfraAssembler = notificationInfraAssembler;
    }

    @Override
    public void addNotification(Notification notification) {
        NotificationInfraDto notificationInfra = notificationInfraAssembler.assemble(notification);
        NotificationsInfraDto notifications = notificationDao.getNotifications(notification.getUserId());
        notifications.setUserId(notification.getUserId());
        notifications.addNotification(notificationInfra);

        notificationDao.addNotification(notifications);
    }

    @Override
    public NotificationsInfraDto getNotifications(String userId) {
        return notificationDao.getNotifications(userId);
    }

    @Override
    public void deleteNotifications(String userId) {
        notificationDao.deleteNotifications(userId);
    }
}
