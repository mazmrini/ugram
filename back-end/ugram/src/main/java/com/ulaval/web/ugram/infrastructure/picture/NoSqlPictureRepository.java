package com.ulaval.web.ugram.infrastructure.picture;

import com.ulaval.web.ugram.domain.picture.Picture;
import com.ulaval.web.ugram.domain.picture.PictureRepository;
import com.ulaval.web.ugram.domain.picture.tag.Tag;
import com.ulaval.web.ugram.infrastructure.picture.assembler.PictureInfraAssembler;
import com.ulaval.web.ugram.infrastructure.picture.dao.PictureDao;
import com.ulaval.web.ugram.infrastructure.picture.dao.upload.PictureFileDao;
import com.ulaval.web.ugram.infrastructure.picture.dto.PictureDtoInfra;
import com.ulaval.web.ugram.infrastructure.picture.tag.assembler.TagInfraAssembler;
import com.ulaval.web.ugram.infrastructure.picture.tag.dto.TagDtoInfra;
import com.ulaval.web.ugram.infrastructure.picture.tag.TagDao;
import com.ulaval.web.ugram.infrastructure.user.dao.UserDao;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class NoSqlPictureRepository implements PictureRepository {

    private PictureDao pictureDao;
    private UserDao userDao;
    private PictureInfraAssembler pictureAssembler;
    private PictureFileDao pictureFileDao;
    private TagDao tagDao;
    private TagInfraAssembler tagInfraAssembler;

    @Autowired
    public NoSqlPictureRepository(PictureDao pictureDao,
                                  UserDao userDao,
                                  PictureInfraAssembler pictureAssembler,
                                  PictureFileDao pictureFileDao,
                                  TagDao tagDao,
                                  TagInfraAssembler tagInfraAssembler) {
        this.pictureDao = pictureDao;
        this.userDao = userDao;
        this.pictureAssembler = pictureAssembler;
        this.pictureFileDao = pictureFileDao;
        this.tagDao = tagDao;
        this.tagInfraAssembler = tagInfraAssembler;
    }

    @Override
    public List<Picture> getPictures() {
        List<PictureDtoInfra> picturesDtoInfra = pictureDao.getPictures();
        return pictureAssembler.assemble(picturesDtoInfra);
    }

    @Override
    public void postPicture(Picture picture, File file) {
        String pictureUrl = pictureFileDao.uploadPicture(file);
        picture.updateUrl(pictureUrl);
        PictureDtoInfra pictureDtoInfra = pictureAssembler.toDto(picture);

        pictureDao.createPicture(pictureDtoInfra);
        tagDao.putTags(pictureDtoInfra.tags);
    }

    @Override
    public List<Picture> getPicturesByUserId(String userId) {
        userDao.fetchUser(userId);
        List<PictureDtoInfra> picturesDtoInfra = pictureDao.getPicturesByUserId(userId);
        return pictureAssembler.assemble(picturesDtoInfra);
    }

    @Override
    public void deletePictureByPictureId(long pictureId, String userId) {
        pictureDao.deletePictureByPictureId(pictureId, userId);
    }

    @Override
    public Picture getPictureByPictureId(String userId, long pictureId) {
        PictureDtoInfra pictureDtoInfra = pictureDao.getPictureByPictureId(userId, pictureId);
        return pictureAssembler.assemble(pictureDtoInfra);
    }

    @Override
    public void putPicture(Picture picture) {
        PictureDtoInfra pictureDtoInfra = pictureAssembler.toDto(picture);
        pictureDao.putPicture(pictureDtoInfra);
    }

    @Override
    public List<Tag> getTags() {
        List<TagDtoInfra> tagsDtoInfra = tagDao.getTags();

        return tagInfraAssembler.toDomain(tagsDtoInfra);
    }

    @Override
    public void putComment(String userId, String fromUserId, long pictureId, String comment) {
        PictureDtoInfra pictureDtoInfra = pictureDao.getPictureByPictureId(userId, pictureId);
        List<Map> comments = assembleComments(fromUserId, comment, pictureDtoInfra);
        pictureDao.putComment(userId, pictureId, comments);
    }

    @Override
    public void putLike(String userId, String fromUserId, long pictureId) {
        PictureDtoInfra pictureDtoInfra = pictureDao.getPictureByPictureId(userId, pictureId);
        List<String> likes = assembleLikes(fromUserId, pictureDtoInfra);
        pictureDao.putLike(userId, pictureId, likes);
    }

    private List<Map> assembleComments(String fromUserId, String comment,
                                       PictureDtoInfra pictureDtoInfra) {
        List<Map> comments = pictureDtoInfra.comments;
        if (comments == null) {
            comments = new ArrayList<>();
        }
        Map<String, String> newComment = new HashMap<>();
        newComment.put("fromUserId", fromUserId);
        newComment.put("comment", comment);
        comments.add(newComment);
        return comments;
    }

    private List<String> assembleLikes(String fromUserId, PictureDtoInfra pictureDtoInfra) {
        List<String> likes = pictureDtoInfra.likes;
        if (likes.contains(fromUserId)) {
            likes.remove(fromUserId);
        }
        else {
            likes.add(fromUserId);
        }
        return likes;
    }
}
