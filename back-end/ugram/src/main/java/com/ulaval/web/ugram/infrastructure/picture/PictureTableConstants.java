package com.ulaval.web.ugram.infrastructure.picture;

public class PictureTableConstants {

    public static final String TABLE_NAME = "Picture";
    public static final String PRIMARY_KEY = "id";
    public static final String CREATED_DATE_KEY = "createdDate";
    public static final String DESCRIPTION_KEY = "description";
    public static final String MENTIONS_KEY = "mentions";
    public static final String TAGS_KEY = "tags";
    public static final String URL_KEY = "url";
    public static final String USER_ID_KEY = "userId";
    public static final String COMMENTS_KEY = "comments";
    public static final String LIKES_KEY = "likes";
}
