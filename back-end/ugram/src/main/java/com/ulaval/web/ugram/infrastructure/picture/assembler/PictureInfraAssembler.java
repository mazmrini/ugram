package com.ulaval.web.ugram.infrastructure.picture.assembler;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.ulaval.web.ugram.domain.picture.Picture;
import com.ulaval.web.ugram.infrastructure.picture.PictureTableConstants;
import com.ulaval.web.ugram.infrastructure.picture.dto.PictureDtoInfra;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

@Component
public class PictureInfraAssembler {

    private static final String COMMENT_KEY = "comment";
    private static final String FROM_USER_ID_KEY = "fromUserId";

    public List<Picture> assemble(List<PictureDtoInfra> picturesDtoInfra) {
        List<Picture> pictures = new ArrayList<>();

        for (PictureDtoInfra pictureDtoInfra : picturesDtoInfra) {
            Picture picture = assemble(pictureDtoInfra);
            pictures.add(picture);
        }

        return pictures;
    }

    public Picture assemble(PictureDtoInfra pictureDtoInfra) {
        return new Picture(pictureDtoInfra.id,
            pictureDtoInfra.createdDate,
            pictureDtoInfra.description,
            pictureDtoInfra.mentions,
            pictureDtoInfra.tags,
            pictureDtoInfra.url,
            pictureDtoInfra.userId,
            pictureDtoInfra.comments,
            pictureDtoInfra.likes);
    }

    public List<PictureDtoInfra> assemble(ScanResult scanResult) {
        List<PictureDtoInfra> picturesDtoInfra = new ArrayList<>();

        for (Map<String, AttributeValue> item : scanResult.getItems()) {
            PictureDtoInfra pictureDtoInfra = assemble(item);
            picturesDtoInfra.add(pictureDtoInfra);
        }

        return picturesDtoInfra;
    }

    public PictureDtoInfra assemble(Map<String, AttributeValue> results) {
        PictureDtoInfra pictureDtoInfra = new PictureDtoInfra();
        pictureDtoInfra.id = results.get(PictureTableConstants.PRIMARY_KEY) == null ? 0
            : Long.parseLong(results.get(PictureTableConstants.PRIMARY_KEY).getN());
        pictureDtoInfra.createdDate = results.get(PictureTableConstants.CREATED_DATE_KEY) == null ? 0
            : Long.parseLong(results.get(PictureTableConstants.CREATED_DATE_KEY).getN());
        pictureDtoInfra.description = results.get(PictureTableConstants.DESCRIPTION_KEY).getS() == null ? ""
            : results.get(PictureTableConstants.DESCRIPTION_KEY).getS();
        pictureDtoInfra.mentions = results.get(PictureTableConstants.MENTIONS_KEY) == null ? new ArrayList<>()
            : mapToStringList(results.get(PictureTableConstants.MENTIONS_KEY).getL());
        pictureDtoInfra.tags = results.get(PictureTableConstants.TAGS_KEY) == null ? new ArrayList<>()
            : mapToStringList(results.get(PictureTableConstants.TAGS_KEY).getL());
        pictureDtoInfra.url = results.get(PictureTableConstants.URL_KEY) == null ? ""
            : results.get(PictureTableConstants.URL_KEY).getS();
        pictureDtoInfra.userId = results.get(PictureTableConstants.USER_ID_KEY) == null ? ""
            : results.get(PictureTableConstants.USER_ID_KEY).getS();
        pictureDtoInfra.likes = results.get(PictureTableConstants.LIKES_KEY) == null ? new ArrayList<>()
            : mapToStringList(results.get(PictureTableConstants.LIKES_KEY).getL());
        pictureDtoInfra.comments = results.get(PictureTableConstants.COMMENTS_KEY) == null ? new ArrayList<>()
            : toListMap(results.get(PictureTableConstants.COMMENTS_KEY).getL());
        return pictureDtoInfra;
    }


    public PictureDtoInfra toDto(Picture picture) {
        PictureDtoInfra pictureDtoInfra = new PictureDtoInfra();
        pictureDtoInfra.id = picture.getId();
        pictureDtoInfra.createdDate = picture.getCreatedDate();
        pictureDtoInfra.description = picture.getDescription();
        pictureDtoInfra.mentions = picture.getMentions();
        pictureDtoInfra.tags = picture.getTags();
        pictureDtoInfra.url = picture.getUrl();
        pictureDtoInfra.userId = picture.getUserId();
        pictureDtoInfra.comments = picture.getComments();
        pictureDtoInfra.likes = picture.getLikes();

        return pictureDtoInfra;
    }

    private List<String> mapToStringList(List<AttributeValue> attributeValues) {
        return attributeValues.stream().map((value) -> value.getS()).collect(Collectors.toList());
    }

    private List<Map> toListMap(List<AttributeValue> attributeValues) {
        List<Map> comments = new ArrayList<>();
        for (AttributeValue attribute : attributeValues) {
            Map<String, String> comment = new HashMap<>();
            comment.put(FROM_USER_ID_KEY, attribute.getM().get(FROM_USER_ID_KEY).getS());
            comment.put(COMMENT_KEY, attribute.getM().get(COMMENT_KEY).getS());
            comments.add(comment);
        }
        return comments;
    }
}
