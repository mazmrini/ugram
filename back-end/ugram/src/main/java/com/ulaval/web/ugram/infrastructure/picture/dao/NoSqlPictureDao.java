package com.ulaval.web.ugram.infrastructure.picture.dao;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.DeleteItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.google.common.base.Strings;
import com.ulaval.web.ugram.infrastructure.NoSqlRequestFactory;
import com.ulaval.web.ugram.infrastructure.exception.ItemNotFoundException;
import com.ulaval.web.ugram.infrastructure.picture.PictureTableConstants;
import com.ulaval.web.ugram.infrastructure.picture.assembler.PictureInfraAssembler;
import com.ulaval.web.ugram.infrastructure.picture.dto.PictureDtoInfra;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class NoSqlPictureDao implements PictureDao {

    private static final String ITEM_NOT_FOUND_MESSAGE = "message:Picture not found";
    private static final String DELETE_MESSAGE = "message:User can't delete this picture";
    private static final String PUT_MESSAGE = "message:User can't update this picture";
    private static final String EMPTY_STRING = " ";

    private PictureInfraAssembler pictureInfraAssembler;
    private AmazonDynamoDB dynamoDb;
    private NoSqlRequestFactory noSqlRequestFactory;
    private Table table;

    @Autowired
    public NoSqlPictureDao(PictureInfraAssembler pictureInfraAssembler, AmazonDynamoDB amazonDynamoDB,
                           DynamoDB dynamoDB, NoSqlRequestFactory noSqlRequestFactory) {
        this.pictureInfraAssembler = pictureInfraAssembler;
        this.dynamoDb = amazonDynamoDB;
        this.noSqlRequestFactory = noSqlRequestFactory;
        table = dynamoDB.getTable(PictureTableConstants.TABLE_NAME);
    }

    @Override
    public List<PictureDtoInfra> getPictures() {
        ScanRequest scanRequest = createScanRequestWithTableName();
        ScanResult scanResult = executeScanRequest(scanRequest);
        return pictureInfraAssembler.assemble(scanResult);
    }

    @Override
    public void createPicture(PictureDtoInfra pictureDtoInfra) {
        fixDynamoNullIssue(pictureDtoInfra);
        table.putItem(noSqlRequestFactory.createItem()
            .withLong(PictureTableConstants.PRIMARY_KEY, pictureDtoInfra.id)
            .withString(PictureTableConstants.DESCRIPTION_KEY, pictureDtoInfra.description)
            .withNumber(PictureTableConstants.CREATED_DATE_KEY, pictureDtoInfra.createdDate)
            .withList(PictureTableConstants.MENTIONS_KEY, pictureDtoInfra.mentions)
            .withList(PictureTableConstants.TAGS_KEY, pictureDtoInfra.tags)
            .withString(PictureTableConstants.URL_KEY, pictureDtoInfra.url)
            .withString(PictureTableConstants.USER_ID_KEY, pictureDtoInfra.userId)
            .withList(PictureTableConstants.LIKES_KEY, pictureDtoInfra.likes)
            .withList(PictureTableConstants.COMMENTS_KEY, pictureDtoInfra.comments));
    }

    @Override
    public PictureDtoInfra getPictureByPictureId(String userId, long pictureId) {
        GetItemRequest request = createGetItemByPictureIdRequest(PictureTableConstants.PRIMARY_KEY,
            PictureTableConstants.USER_ID_KEY, userId, pictureId);
        Map<String, AttributeValue> result = executeGetItemRequest(request);
        if (result == null) {
            throw new ItemNotFoundException(ITEM_NOT_FOUND_MESSAGE);
        }

        return pictureInfraAssembler.assemble(result);
    }

    @Override
    public void deletePictureByPictureId(long pictureId, String userId) {
        DeleteItemSpec deleteItemSpec = createDeletePictureRequest(pictureId, userId);
        executeDeleteRequest(deleteItemSpec);
    }

    @Override
    public void putPicture(PictureDtoInfra pictureDtoInfra) {
        fixDynamoNullIssue(pictureDtoInfra);
        UpdateItemSpec updateItemSpec = createUpdateRequest(pictureDtoInfra);
        executeUpdateRequest(updateItemSpec);
    }

    @Override
    public void putComment(String userId, long pictureId, List<Map> comments) {
        UpdateItemSpec updateItemSpec = noSqlRequestFactory.createUpdateItemSpec()
            .withPrimaryKey(PictureTableConstants.PRIMARY_KEY, pictureId, PictureTableConstants.USER_ID_KEY, userId)
            .withUpdateExpression("set comments = :c")
            .withValueMap(new ValueMap()
                .withList(":c", comments));
        executeUpdateRequest(updateItemSpec);
    }

    @Override
    public void putLike(String userId, long pictureId, List<String> likes) {
        UpdateItemSpec updateItemSpec = noSqlRequestFactory.createUpdateItemSpec()
            .withPrimaryKey(PictureTableConstants.PRIMARY_KEY, pictureId, PictureTableConstants.USER_ID_KEY, userId)
            .withUpdateExpression("set likes = :l")
            .withValueMap(new ValueMap()
                .withList(":l", likes));
        executeUpdateRequest(updateItemSpec);
    }

    @Override
    public List<PictureDtoInfra> getPicturesByUserId(String userId) {
        ScanRequest scanRequest = createScanRequestWithConditionExpression(userId);
        ScanResult scanResult = executeScanRequest(scanRequest);

        return pictureInfraAssembler.assemble(scanResult);
    }

    private void executeUpdateRequest(UpdateItemSpec updateItemSpec) {
        try {
            table.updateItem(updateItemSpec);
        }
        catch (Exception exception) {
            throw new ItemNotFoundException(PUT_MESSAGE);
        }
    }

    private UpdateItemSpec createUpdateRequest(PictureDtoInfra pictureDtoInfra) {
        return noSqlRequestFactory.createUpdateItemSpec()
            .withPrimaryKey(PictureTableConstants.PRIMARY_KEY, pictureDtoInfra.id,
                PictureTableConstants.USER_ID_KEY, pictureDtoInfra.userId)
            .withUpdateExpression("set description = :d, mentions = :m, tags =:t")
            .withConditionExpression("userId = :val")
            .withValueMap(new ValueMap()
                .withString(":d", pictureDtoInfra.description)
                .withList(":m", pictureDtoInfra.mentions)
                .withList(":t", pictureDtoInfra.tags)
                .withString(":val", pictureDtoInfra.userId));
    }

    private void fixDynamoNullIssue(PictureDtoInfra pictureDtoInfra) {
        if (Strings.isNullOrEmpty(pictureDtoInfra.description)) {
            pictureDtoInfra.description = EMPTY_STRING;
        }

        if (pictureDtoInfra.mentions == null) {
            pictureDtoInfra.mentions = new ArrayList<>();
        }

        if (pictureDtoInfra.tags == null) {
            pictureDtoInfra.tags = new ArrayList<>();
        }
    }

    private void executeDeleteRequest(DeleteItemSpec deleteItemSpec) {
        try {
            table.deleteItem(deleteItemSpec);
        }
        catch (Exception exception) {
            throw new ItemNotFoundException(DELETE_MESSAGE);
        }
    }

    private DeleteItemSpec createDeletePictureRequest(long pictureId, String userId) {
        return noSqlRequestFactory.createDeleteItemSpec()
            .withPrimaryKey(PictureTableConstants.PRIMARY_KEY, pictureId, PictureTableConstants.USER_ID_KEY, userId)
            .withConditionExpression("userId = :val")
            .withValueMap(new ValueMap().withString(":val", userId));
    }

    private ScanResult executeScanRequest(ScanRequest scanRequest) {
        return dynamoDb.scan(scanRequest);
    }

    private ScanRequest createScanRequestWithTableName() {
        return noSqlRequestFactory.createScanRequest().withTableName(PictureTableConstants.TABLE_NAME);
    }

    private ScanRequest createScanRequestWithConditionExpression(String userId) {
        Map<String, AttributeValue> expressionAttributeValues = new HashMap<>();
        expressionAttributeValues.put(":val", new AttributeValue().withS(userId));
        return noSqlRequestFactory.createScanRequest().withTableName(PictureTableConstants.TABLE_NAME)
            .withFilterExpression("userId = :val")
            .withExpressionAttributeValues(expressionAttributeValues);
    }

    private GetItemRequest createGetItemByPictureIdRequest(String key, String triKey, String userId, long pictureId) {
        HashMap<String, AttributeValue> keyToGet = new HashMap<>();
        keyToGet.put(key, new AttributeValue().withN(String.valueOf(pictureId)));
        keyToGet.put(triKey, new AttributeValue().withS(userId));

        return noSqlRequestFactory.createGetItemRequest()
            .withKey(keyToGet)
            .withTableName(PictureTableConstants.TABLE_NAME);
    }

    private Map<String, AttributeValue> executeGetItemRequest(GetItemRequest request) {
        return dynamoDb.getItem(request).getItem();
    }
}
