package com.ulaval.web.ugram.infrastructure.picture.dao;

import com.ulaval.web.ugram.infrastructure.picture.dto.PictureDtoInfra;
import java.util.List;
import java.util.Map;

public interface PictureDao {

    List<PictureDtoInfra> getPictures();

    void createPicture(PictureDtoInfra pictureDtoInfra);

    PictureDtoInfra getPictureByPictureId(String userId, long pictureId);

    List<PictureDtoInfra> getPicturesByUserId(String userId);

    void deletePictureByPictureId(long pictureId, String userId);

    void putPicture(PictureDtoInfra pictureDtoInfra);

    void putComment(String userId, long pictureId, List<Map> comments);

    void putLike(String userId, long pictureId, List<String> likes);
}
