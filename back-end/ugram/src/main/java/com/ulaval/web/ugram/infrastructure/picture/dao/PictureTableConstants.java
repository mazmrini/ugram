package com.ulaval.web.ugram.infrastructure.picture.dao;

public class PictureTableConstants {

    public static final String TABLE_NAME = "Picture";
    public static final String PRIMARY_KEY = "id";
    public static final String USER_ID_KEY = "userId";
    public static final String DATE_KEY = "createdDate";
    public static final String DESCRIPTION_KEY = "description";
    public static final String MENTION_KEY = "mentions";
    public static final String TAG_KEY = "tags";
    public static final String URL_KEY = "url";
}
