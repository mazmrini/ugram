package com.ulaval.web.ugram.infrastructure.picture.dao.upload;

import java.io.File;

public interface PictureFileDao {
    String uploadPicture(File file);
}
