package com.ulaval.web.ugram.infrastructure.picture.dao.upload;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import java.io.File;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class S3PictureFileDao implements PictureFileDao {

    private static final String BUCKET_NAME = "ugramteam10";
    private static final String JPG = ".jpg";

    private AmazonS3 amazonS3;

    @Autowired
    public S3PictureFileDao(AmazonS3 amazonS3) {
        this.amazonS3 = amazonS3;
    }

    @Override
    public String uploadPicture(File file) {
        UUID pictureId = UUID.randomUUID();

        amazonS3.putObject(new PutObjectRequest(BUCKET_NAME, pictureId.toString() + JPG, file)
            .withCannedAcl(CannedAccessControlList.PublicRead));

        return generatePictureUrl(pictureId);
    }

    private String generatePictureUrl(UUID uuid) {
        return "https://s3.amazonaws.com/ugramteam10/" + uuid.toString() + ".jpg";
    }
}
