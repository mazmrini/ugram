package com.ulaval.web.ugram.infrastructure.picture.dto;

import java.util.List;
import java.util.Map;

public class PictureDtoInfra {

    public long id;
    public long createdDate;
    public String description;
    public List<String> mentions;
    public List<String> tags;
    public String url;
    public String userId;
    public List<Map> comments;
    public List<String> likes;
}
