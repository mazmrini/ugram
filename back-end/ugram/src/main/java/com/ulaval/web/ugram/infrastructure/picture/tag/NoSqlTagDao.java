package com.ulaval.web.ugram.infrastructure.picture.tag;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.NameMap;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.ulaval.web.ugram.infrastructure.NoSqlRequestFactory;
import com.ulaval.web.ugram.infrastructure.picture.tag.assembler.TagInfraAssembler;
import com.ulaval.web.ugram.infrastructure.picture.tag.dto.TagDtoInfra;
import java.util.List;

public class NoSqlTagDao implements TagDao {

    private AmazonDynamoDB dynamoDb;
    private NoSqlRequestFactory noSqlRequestFactory;
    private Table table;
    private TagInfraAssembler tagInfraAssembler;

    public NoSqlTagDao(AmazonDynamoDB amazonDynamoDB,
                       DynamoDB dynamoDB,
                       NoSqlRequestFactory noSqlRequestFactory,
                       TagInfraAssembler tagInfraAssembler) {
        this.dynamoDb = amazonDynamoDB;
        this.table = dynamoDB.getTable(TagsTableConstants.TABLE_NAME);
        this.noSqlRequestFactory = noSqlRequestFactory;
        this.tagInfraAssembler = tagInfraAssembler;
    }

    @Override
    public void putTags(List<String> tags) {
        for (String tag: tags) {
            UpdateItemSpec updateRequest = createUpdateRequest(tag);
            table.updateItem(updateRequest);
        }
    }

    @Override
    public List<TagDtoInfra> getTags() {
        ScanRequest scanRequest = createScanRequestWithTableName();
        ScanResult scanResult = executeScanRequest(scanRequest);

        return tagInfraAssembler.assemble(scanResult);
    }

    private ScanRequest createScanRequestWithTableName() {
        return noSqlRequestFactory.createScanRequest().withTableName(TagsTableConstants.TABLE_NAME);
    }

    private ScanResult executeScanRequest(ScanRequest scanRequest) {
        return dynamoDb.scan(scanRequest);
    }

    private UpdateItemSpec createUpdateRequest(String tag) {
        return noSqlRequestFactory.createUpdateItemSpec()
            .withPrimaryKey(TagsTableConstants.PRIMARY_KEY, tag)
            .withUpdateExpression("ADD #u :val")
            .withNameMap(new NameMap().with("#u", TagsTableConstants.USAGE))
            .withValueMap(new ValueMap().withNumber(":val", 1));
    }
}
