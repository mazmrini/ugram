package com.ulaval.web.ugram.infrastructure.picture.tag;

import com.ulaval.web.ugram.infrastructure.picture.tag.dto.TagDtoInfra;
import java.util.List;

public interface TagDao {
    void putTags(List<String> tags);

    List<TagDtoInfra> getTags();
}
