package com.ulaval.web.ugram.infrastructure.picture.tag;

public class TagsTableConstants {

    public static final String TABLE_NAME = "Tags";
    public static final String PRIMARY_KEY = "tag";
    public static final String USAGE = "usage";
}
