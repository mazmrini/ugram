package com.ulaval.web.ugram.infrastructure.picture.tag.assembler;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.ulaval.web.ugram.domain.picture.tag.Tag;
import com.ulaval.web.ugram.infrastructure.picture.tag.dto.TagDtoInfra;
import com.ulaval.web.ugram.infrastructure.picture.tag.TagsTableConstants;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Component;

@Component
public class TagInfraAssembler {

    public List<TagDtoInfra> assemble(ScanResult scanResult) {
        List<TagDtoInfra> tagsDtoInfra = new ArrayList<>();

        for (Map<String, AttributeValue> item : scanResult.getItems()) {
            TagDtoInfra tagDtoInfra = assemble(item);
            tagsDtoInfra.add(tagDtoInfra);
        }

        return tagsDtoInfra;
    }

    public TagDtoInfra assemble(Map<String, AttributeValue> results) {
        TagDtoInfra tagDtoInfra = new TagDtoInfra();
        tagDtoInfra.tag = results.get(TagsTableConstants.PRIMARY_KEY).getS();
        tagDtoInfra.usage = Long.parseLong(results.get(TagsTableConstants.USAGE).getN());

        return tagDtoInfra;
    }

    public List<Tag> toDomain(List<TagDtoInfra> tagsDtoInfra) {
        List<Tag> tags = new ArrayList<>();

        for (TagDtoInfra tagDtoInfra: tagsDtoInfra) {
            Tag tag = toDomain(tagDtoInfra);
            tags.add(tag);
        }

        return tags;
    }

    public Tag toDomain(TagDtoInfra tagDtoInfra) {
        return new Tag(tagDtoInfra.tag, tagDtoInfra.usage);
    }

}
