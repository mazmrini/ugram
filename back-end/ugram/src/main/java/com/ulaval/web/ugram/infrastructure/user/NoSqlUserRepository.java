package com.ulaval.web.ugram.infrastructure.user;

import com.ulaval.web.ugram.domain.user.User;
import com.ulaval.web.ugram.domain.user.UserRepository;
import com.ulaval.web.ugram.infrastructure.user.assembler.UserInfraAssembler;
import com.ulaval.web.ugram.infrastructure.user.dao.UserDao;
import com.ulaval.web.ugram.infrastructure.user.dto.UserInfraDto;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class NoSqlUserRepository implements UserRepository {

    private UserInfraAssembler userInfraAssembler;
    private UserDao userDao;

    @Autowired
    public NoSqlUserRepository(UserDao userDao, UserInfraAssembler userInfraAssembler) {
        this.userDao = userDao;
        this.userInfraAssembler = userInfraAssembler;
    }

    @Override
    public List<User> fetchUsers() {
        List<UserInfraDto> userInfraDto = userDao.fetchUsers();

        return userInfraAssembler.toDomain(userInfraDto);
    }

    @Override
    public User fetchUser(String userId) {
        UserInfraDto userInfraDto = userDao.fetchUser(userId);

        return userInfraAssembler.toDomain(userInfraDto);
    }

    @Override
    public User fetchUserByFacebookId(String facebookId) {
        UserInfraDto userInfraDto = userDao.fetchUserByFacebookId(facebookId);

        return userInfraAssembler.toDomain(userInfraDto);
    }

    @Override
    public void putUser(User user) {
        UserInfraDto userInfraDto = userInfraAssembler.toInfra(user);

        userDao.putUser(userInfraDto);
    }

    @Override
    public void deleteUser(String userId) {
        userDao.deleteUser(userId);
    }

    @Override
    public void createUser(User user) {
        UserInfraDto userInfraDto = userInfraAssembler.toInfra(user);

        userDao.createUser(userInfraDto);
    }
}
