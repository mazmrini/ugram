package com.ulaval.web.ugram.infrastructure.user.assembler;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.ulaval.web.ugram.domain.user.User;
import com.ulaval.web.ugram.infrastructure.user.dao.UserTableConstants;
import com.ulaval.web.ugram.infrastructure.user.dto.UserInfraDto;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Component;

@Component
public class UserInfraAssembler {

    public UserInfraDto assemble(Map<String, AttributeValue> results) {
        UserInfraDto userInfraDto = new UserInfraDto();
        userInfraDto.lastName = results.get(UserTableConstants.LAST_NAME_KEY) == null ? ""
            : results.get(UserTableConstants.LAST_NAME_KEY).getS();
        userInfraDto.firstName = results.get(UserTableConstants.FIRST_NAME_KEY) == null ? ""
            : results.get(UserTableConstants.FIRST_NAME_KEY).getS();
        userInfraDto.email = results.get(UserTableConstants.EMAIL_KEY) == null ? ""
            : results.get(UserTableConstants.EMAIL_KEY).getS();
        userInfraDto.id = results.get(UserTableConstants.ID) == null ? ""
            : results.get(UserTableConstants.ID).getS();
        userInfraDto.authId = results.get(UserTableConstants.AUTH_ID_KEY) == null ? ""
            : results.get(UserTableConstants.AUTH_ID_KEY).getS();
        userInfraDto.pictureUrl = results.get(UserTableConstants.PICTURE_URL_KEY) == null ? ""
            : results.get(UserTableConstants.PICTURE_URL_KEY).getS();
        userInfraDto.phoneNumber = results.get(UserTableConstants.PHONE_NUMBER_KEY) == null ? 0
            : Long.parseLong(results.get(UserTableConstants.PHONE_NUMBER_KEY).getN());
        userInfraDto.registrationDate = results.get(UserTableConstants.REGISTRATION_DATE_KEY) == null ? 0
            : Long.parseLong(results.get(UserTableConstants.REGISTRATION_DATE_KEY).getN());
        return userInfraDto;
    }

    public List<UserInfraDto> assemble(ScanResult scanResult) {
        List<UserInfraDto> userInfraDtos = new ArrayList<>();
        for (Map<String, AttributeValue> item : scanResult.getItems()) {
            UserInfraDto userInfraDto = assemble(item);
            userInfraDtos.add(userInfraDto);
        }
        return userInfraDtos;
    }

    public User toDomain(UserInfraDto userInfraDto) {
        return new User(userInfraDto.id,
            userInfraDto.authId,
            userInfraDto.firstName,
            userInfraDto.lastName,
            userInfraDto.email,
            userInfraDto.phoneNumber,
            userInfraDto.pictureUrl,
            userInfraDto.registrationDate);
    }

    public List<User> toDomain(List<UserInfraDto> userInfraDtos) {
        List<User> users = new ArrayList<>();
        for (UserInfraDto userInfraDto : userInfraDtos) {
            User user = toDomain(userInfraDto);
            users.add(user);
        }
        return users;
    }

    public UserInfraDto toInfra(User user) {
        UserInfraDto userInfraDto = new UserInfraDto();
        userInfraDto.id = user.getId();
        userInfraDto.authId = user.getAuthId();
        userInfraDto.firstName = user.getFirstName();
        userInfraDto.lastName = user.getLastName();
        userInfraDto.email = user.getEmail();
        userInfraDto.phoneNumber = user.getPhoneNumber();
        userInfraDto.pictureUrl = user.getPictureUrl();
        userInfraDto.registrationDate = user.getRegistrationDate();

        return userInfraDto;
    }
}
