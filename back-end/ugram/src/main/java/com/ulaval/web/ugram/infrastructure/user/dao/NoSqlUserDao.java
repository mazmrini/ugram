package com.ulaval.web.ugram.infrastructure.user.dao;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.DeleteItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.dynamodbv2.model.Condition;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.ulaval.web.ugram.infrastructure.NoSqlRequestFactory;
import com.ulaval.web.ugram.infrastructure.exception.ItemNotFoundException;
import com.ulaval.web.ugram.infrastructure.user.assembler.UserInfraAssembler;
import com.ulaval.web.ugram.infrastructure.user.dto.UserInfraDto;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class NoSqlUserDao implements UserDao {

    private static final String ITEM_NOT_FOUND_MESSAGE = "message:User does not exist";

    private NoSqlRequestFactory noSqlRequestFactory;
    private UserInfraAssembler userInfraAssembler;
    private AmazonDynamoDB dynamoDb;
    private Table table;

    @Autowired
    public NoSqlUserDao(UserInfraAssembler userInfraAssembler,
                        AmazonDynamoDB amazonDynamoDB,
                        DynamoDB dynamoDB,
                        NoSqlRequestFactory noSqlRequestFactory) {
        dynamoDb = amazonDynamoDB;
        table = dynamoDB.getTable(UserTableConstants.TABLE_NAME);
        this.userInfraAssembler = userInfraAssembler;
        this.noSqlRequestFactory = noSqlRequestFactory;
    }

    @Override
    public UserInfraDto fetchUser(String userId) {
        GetItemRequest request = createGetItemRequest(UserTableConstants.PRIMARY_KEY, userId);
        Map<String, AttributeValue> result = executeGetItemRequest(request);

        return userInfraAssembler.assemble(result);
    }

    @Override
    public UserInfraDto fetchUserByFacebookId(String facebookId) {
        ScanRequest scanRequest = createScanRequestWithTableName();
        scanRequest.setScanFilter(createFacebookScanFilter(facebookId));

        ScanResult scanResult = executeScanRequest(scanRequest);

        if (scanResult.getItems().isEmpty()) {
            throw new ItemNotFoundException(ITEM_NOT_FOUND_MESSAGE);
        }

        return userInfraAssembler.assemble(scanResult.getItems().get(0));
    }

    @Override
    public List<UserInfraDto> fetchUsers() {
        ScanRequest scanRequest = createScanRequestWithTableName();
        ScanResult scanResult = executeScanRequest(scanRequest);

        return userInfraAssembler.assemble(scanResult);
    }

    @Override
    public void putUser(UserInfraDto userInfraDto) {
        UpdateItemSpec updateItemSpec = createUpdateRequest(userInfraDto);

        try {
            table.updateItem(updateItemSpec);
        }
        catch (Exception exception) {
            throw new ItemNotFoundException(ITEM_NOT_FOUND_MESSAGE);
        }
    }

    @Override
    public void deleteUser(String userId) {
        DeleteItemSpec deleteItemSpec = noSqlRequestFactory.createDeleteItemSpec()
            .withPrimaryKey(UserTableConstants.PRIMARY_KEY, userId);
        try {
            table.deleteItem(deleteItemSpec);
        }
        catch (Exception exception) {
            throw new ItemNotFoundException(ITEM_NOT_FOUND_MESSAGE);
        }
    }

    @Override
    public void createUser(UserInfraDto userInfraDto) {
        table.putItem(noSqlRequestFactory.createItem()
            .withPrimaryKey(UserTableConstants.PRIMARY_KEY, userInfraDto.id)
            .withString(UserTableConstants.FIRST_NAME_KEY, userInfraDto.firstName)
            .withString(UserTableConstants.LAST_NAME_KEY, userInfraDto.lastName)
            .withString(UserTableConstants.EMAIL_KEY, userInfraDto.email)
            .withString(UserTableConstants.PICTURE_URL_KEY, userInfraDto.pictureUrl)
            .withNumber(UserTableConstants.PHONE_NUMBER_KEY, userInfraDto.phoneNumber)
            .withNumber(UserTableConstants.REGISTRATION_DATE_KEY, userInfraDto.registrationDate)
            .withString(UserTableConstants.AUTH_ID_KEY, userInfraDto.authId));
    }

    private Map<String, Condition> createFacebookScanFilter(String facebookId) {
        Map<String, Condition> scanFilter = noSqlRequestFactory.createScanFilter();

        Condition facebookCondition = noSqlRequestFactory.createCondition()
            .withAttributeValueList(new AttributeValue(facebookId))
            .withComparisonOperator(ComparisonOperator.EQ);

        scanFilter.put(UserTableConstants.AUTH_ID_KEY, facebookCondition);

        return scanFilter;
    }

    private UpdateItemSpec createUpdateRequest(UserInfraDto userInfraDto) {
        return noSqlRequestFactory.createUpdateItemSpec()
            .withPrimaryKey(UserTableConstants.PRIMARY_KEY, userInfraDto.id)
            .withUpdateExpression("set firstName = :f, lastName = :l, email =:e, phoneNumber =:p")
            .withValueMap(new ValueMap()
                .withString(":f", userInfraDto.firstName)
                .withString(":l", userInfraDto.lastName)
                .withString(":e", userInfraDto.email)
                .withNumber(":p", userInfraDto.phoneNumber));
    }

    private GetItemRequest createGetItemRequest(String key, String userId) {
        HashMap<String, AttributeValue> keyToGet = new HashMap<>();
        keyToGet.put(key, new AttributeValue(userId));

        return noSqlRequestFactory.createGetItemRequest()
            .withKey(keyToGet)
            .withTableName(UserTableConstants.TABLE_NAME);
    }

    private Map<String, AttributeValue> executeGetItemRequest(GetItemRequest request) {
        Map<String, AttributeValue> returnedItem = dynamoDb.getItem(request).getItem();

        if (returnedItem != null) {
            return returnedItem;
        }

        throw new ItemNotFoundException(ITEM_NOT_FOUND_MESSAGE);
    }

    private ScanResult executeScanRequest(ScanRequest scanRequest) {
        return dynamoDb.scan(scanRequest);
    }

    private ScanRequest createScanRequestWithTableName() {
        return noSqlRequestFactory.createScanRequest().withTableName(UserTableConstants.TABLE_NAME);
    }
}
