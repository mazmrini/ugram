package com.ulaval.web.ugram.infrastructure.user.dao;

import com.ulaval.web.ugram.infrastructure.user.dto.UserInfraDto;
import java.util.List;

public interface UserDao {
    UserInfraDto fetchUser(String userId);

    UserInfraDto fetchUserByFacebookId(String facebookId);

    List<UserInfraDto> fetchUsers();

    void putUser(UserInfraDto userInfraDto);

    void deleteUser(String userId);

    void createUser(UserInfraDto userInfraDto);
}
