package com.ulaval.web.ugram.infrastructure.user.dao;

public class UserTableConstants {

    public static final String TABLE_NAME = "User";
    public static final String PRIMARY_KEY = "id";
    public static final String AUTH_ID_KEY = "authId";
    public static final String FIRST_NAME_KEY = "firstName";
    public static final String LAST_NAME_KEY = "lastName";
    public static final String EMAIL_KEY = "email";
    public static final String PICTURE_URL_KEY = "pictureUrl";
    public static final String PHONE_NUMBER_KEY = "phoneNumber";
    public static final String REGISTRATION_DATE_KEY = "registrationDate";
    public static final String ID = "id";
}
