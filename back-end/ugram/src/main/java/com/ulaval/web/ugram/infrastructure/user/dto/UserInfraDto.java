package com.ulaval.web.ugram.infrastructure.user.dto;

public class UserInfraDto {

    public String id;
    public String authId;
    public String firstName;
    public String lastName;
    public String email;
    public String pictureUrl;
    public long phoneNumber;
    public long registrationDate;
}
