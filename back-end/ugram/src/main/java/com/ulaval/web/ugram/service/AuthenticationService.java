package com.ulaval.web.ugram.service;

import com.ulaval.web.ugram.domain.user.User;
import org.springframework.http.HttpHeaders;

public interface AuthenticationService<T> {

    T authenticate(HttpHeaders headers);

    User authenticateWithUserId(HttpHeaders headers, String userId);
}
