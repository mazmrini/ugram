package com.ulaval.web.ugram.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ulaval.web.ugram.domain.user.User;
import com.ulaval.web.ugram.domain.user.UserRepository;
import com.ulaval.web.ugram.service.dto.FacebookId;
import com.ulaval.web.ugram.service.dto.FacebookInformation;
import com.ulaval.web.ugram.service.exception.AuthenticationFailedException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

@Service
public class FacebookAuthenticationService implements AuthenticationService<FacebookInformation> {

    private static final String REGEX = "^Bearer (?<userToken>[a-zA-Z\\d]+)$";
    private static final String BASE_URL = "https://graph.facebook.com";

    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private UserRepository userRepository;

    @Override
    public FacebookInformation authenticate(HttpHeaders headers) {
        String userToken = tryGetUserTokenFrom(headers);

        String url = buildFacebookGraphUrl(userToken);

        return tryRetrieveFacebookInformation(url);
    }

    @Override
    public User authenticateWithUserId(HttpHeaders headers, String userId) {
        FacebookInformation facebookInformation = authenticate(headers);

        User user = userRepository.fetchUser(userId);

        user.validateAuthorization(facebookInformation.id);

        return user;
    }

    private String tryGetUserTokenFrom(HttpHeaders headers) {
        if (!headers.containsKey(HttpHeaders.AUTHORIZATION)) {
            throw new AuthenticationFailedException();
        }

        String fullToken = headers.getFirst(HttpHeaders.AUTHORIZATION);

        return tryParseUserTokenFrom(fullToken);
    }

    private String tryParseUserTokenFrom(String fullToken) {
        Pattern pattern = Pattern.compile(REGEX);
        Matcher matcher = pattern.matcher(fullToken);

        if (!matcher.matches()) {
            throw new AuthenticationFailedException();
        }

        return matcher.group("userToken");
    }

    private String buildFacebookGraphUrl(String userToken) {
        return String.format("%s/me?access_token=%s&fields=id", BASE_URL, userToken);
    }

    private FacebookInformation tryRetrieveFacebookInformation(String url) {
        FacebookInformation facebookInformation = new FacebookInformation();

        try {
            FacebookId facebookId = objectMapper.readValue(new URL(url), FacebookId.class);

            facebookInformation.id = facebookId.id;
            facebookInformation.pictureUrl = buildPictureUrl(facebookInformation.id);
        }
        catch (Exception exception) {
            throw new AuthenticationFailedException();
        }

        return facebookInformation;
    }

    private String buildPictureUrl(String authId) {
        return String.format("%s/v2.5/%s/picture", BASE_URL, authId);
    }
}
