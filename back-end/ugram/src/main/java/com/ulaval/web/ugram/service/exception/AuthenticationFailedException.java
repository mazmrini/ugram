package com.ulaval.web.ugram.service.exception;

public class AuthenticationFailedException extends RuntimeException {

    private static final String MESSAGE = "Authentication failed.";

    public AuthenticationFailedException() {
        super(MESSAGE);
    }
}
