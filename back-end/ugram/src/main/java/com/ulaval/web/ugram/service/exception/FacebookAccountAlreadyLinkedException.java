package com.ulaval.web.ugram.service.exception;

public class FacebookAccountAlreadyLinkedException extends RuntimeException {
    private static final String MESSAGE = "Facebook account already linked.";

    public FacebookAccountAlreadyLinkedException() {
        super(MESSAGE);
    }
}
