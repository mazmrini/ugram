package com.ulaval.web.ugram.service.exception;

public class UsernameAlreadyExistException extends RuntimeException {

    private static final String MESSAGE = "Username already in use, choose another one.";

    public UsernameAlreadyExistException() {
        super(MESSAGE);
    }
}
