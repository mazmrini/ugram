package com.ulaval.web.ugram.service.login;

import com.ulaval.web.ugram.domain.user.User;
import com.ulaval.web.ugram.domain.user.UserRepository;
import com.ulaval.web.ugram.service.AuthenticationService;
import com.ulaval.web.ugram.service.dto.FacebookInformation;
import com.ulaval.web.ugram.service.user.assembler.UserApiAssembler;
import com.ulaval.web.ugram.service.user.dto.UserApiDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

@Service
public class LoginService {

    private AuthenticationService<FacebookInformation> authenticationService;

    private UserRepository userRepository;

    private UserApiAssembler userApiAssembler;

    @Autowired
    public LoginService(AuthenticationService<FacebookInformation> authenticationService,
                        UserRepository userRepository, UserApiAssembler userApiAssembler) {
        this.authenticationService = authenticationService;
        this.userRepository = userRepository;
        this.userApiAssembler = userApiAssembler;
    }

    public UserApiDto login(HttpHeaders headers) {
        FacebookInformation facebookInformation = authenticationService.authenticate(headers);

        User user = userRepository.fetchUserByFacebookId(facebookInformation.id);
        return userApiAssembler.toDto(user);
    }

    public void logout(HttpHeaders headers, String userId) {
        authenticationService.authenticateWithUserId(headers, userId);
    }
}
