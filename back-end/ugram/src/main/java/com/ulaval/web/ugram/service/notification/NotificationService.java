package com.ulaval.web.ugram.service.notification;

import com.ulaval.web.ugram.domain.notification.Notification;
import com.ulaval.web.ugram.domain.notification.NotificationEnum;
import com.ulaval.web.ugram.domain.notification.NotificationRepository;
import com.ulaval.web.ugram.infrastructure.notification.dto.NotificationsInfraDto;
import com.ulaval.web.ugram.service.AuthenticationService;
import com.ulaval.web.ugram.service.notification.assembler.NotificationAssembler;
import com.ulaval.web.ugram.service.notification.dto.NotificationApiDto;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

@Service
public class NotificationService {

    private NotificationAssembler notificationAssembler;
    private NotificationRepository notificationRepository;
    private AuthenticationService authenticationService;

    @Autowired
    public NotificationService(NotificationRepository notificationRepository,
                               NotificationAssembler notificationAssembler,
                               AuthenticationService authenticationService) {
        this.notificationRepository = notificationRepository;
        this.notificationAssembler = notificationAssembler;
        this.authenticationService = authenticationService;
    }

    public void notifyAction(String userId, String fromUserId, long pictureId, NotificationEnum notificationEnum) {
        Notification notification = notificationAssembler.assemble(userId, fromUserId, pictureId, notificationEnum);
        notificationRepository.addNotification(notification);
    }

    public List<NotificationApiDto> getNotification(HttpHeaders headers, String userId) {
        authenticationService.authenticateWithUserId(headers, userId);
        NotificationsInfraDto notifications = notificationRepository.getNotifications(userId);
        notificationRepository.deleteNotifications(userId);
        return notificationAssembler.assemble(notifications);
    }
}
