package com.ulaval.web.ugram.service.notification.assembler;

import com.ulaval.web.ugram.domain.notification.Notification;
import com.ulaval.web.ugram.domain.notification.NotificationEnum;
import com.ulaval.web.ugram.infrastructure.notification.dto.NotificationInfraDto;
import com.ulaval.web.ugram.infrastructure.notification.dto.NotificationsInfraDto;
import com.ulaval.web.ugram.service.notification.dto.NotificationApiDto;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class NotificationAssembler {

    public Notification assemble(String userId, String fromUserId, long pictureId, NotificationEnum notificationEnum) {
        return new Notification(userId, fromUserId, pictureId, notificationEnum);
    }

    public List<NotificationApiDto> assemble(NotificationsInfraDto notificationsInfraDto) {
        List<NotificationApiDto> notifications = new ArrayList<>();
        for (NotificationInfraDto notificationInfraDto : notificationsInfraDto.getNotifications()) {
            notifications.add(assemble(notificationInfraDto));
        }
        return notifications;
    }

    private NotificationApiDto assemble(NotificationInfraDto notificationInfraDto) {
        NotificationApiDto notificationApiDto = new NotificationApiDto();
        notificationApiDto.fromUser = notificationInfraDto.getFromUserId();
        notificationApiDto.pictureId = notificationInfraDto.getPictureId();
        notificationApiDto.reaction = notificationInfraDto.getNotificationType();
        return notificationApiDto;
    }
}
