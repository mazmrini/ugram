package com.ulaval.web.ugram.service.notification.dto;

public class NotificationApiDto {

    public long pictureId;
    public String fromUser;
    public String reaction;

}
