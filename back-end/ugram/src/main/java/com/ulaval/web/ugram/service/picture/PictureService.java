package com.ulaval.web.ugram.service.picture;

import static org.springframework.web.util.HtmlUtils.htmlEscape;

import com.ulaval.web.ugram.domain.notification.NotificationEnum;
import com.ulaval.web.ugram.domain.picture.Picture;
import com.ulaval.web.ugram.domain.picture.PictureRepository;
import com.ulaval.web.ugram.domain.picture.tag.Tag;
import com.ulaval.web.ugram.domain.picture.validator.PictureValidator;
import com.ulaval.web.ugram.service.AuthenticationService;
import com.ulaval.web.ugram.service.dto.FacebookInformation;
import com.ulaval.web.ugram.service.notification.NotificationService;
import com.ulaval.web.ugram.service.picture.assembler.PictureAssembler;
import com.ulaval.web.ugram.service.picture.dto.PictureApiDto;
import com.ulaval.web.ugram.service.picture.dto.UpdatePictureApiDto;
import com.ulaval.web.ugram.service.picture.dto.UploadPictureDto;
import com.ulaval.web.ugram.service.picture.dto.UploadPictureResponseDto;
import com.ulaval.web.ugram.service.picture.tag.assembler.TagAssembler;
import com.ulaval.web.ugram.service.picture.tag.dto.TagApiDto;
import com.ulaval.web.ugram.service.user.dto.PutCommentApiDto;
import com.ulaval.web.ugram.service.user.dto.PutLikeApiDto;
import com.ulaval.web.ugram.util.converter.FileConverter;
import java.io.File;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

@Service
public class PictureService {

    private PictureAssembler pictureAssembler;
    private PictureRepository pictureRepository;
    private FileConverter fileConverter;
    private AuthenticationService<FacebookInformation> authenticationService;
    private PictureValidator pictureValidator;
    private TagAssembler tagAssembler;
    private NotificationService notificationService;

    @Autowired
    public PictureService(PictureRepository pictureRepository,
                          PictureAssembler pictureAssembler,
                          FileConverter fileConverter,
                          AuthenticationService<FacebookInformation> authenticationService,
                          PictureValidator pictureValidator,
                          TagAssembler tagAssembler,
                          NotificationService notificationService) {
        this.pictureRepository = pictureRepository;
        this.pictureAssembler = pictureAssembler;
        this.fileConverter = fileConverter;
        this.authenticationService = authenticationService;
        this.pictureValidator = pictureValidator;
        this.tagAssembler = tagAssembler;
        this.notificationService = notificationService;
    }

    public List<PictureApiDto> getPictures() {
        List<Picture> pictures = pictureRepository.getPictures();
        return pictureAssembler.toDto(pictures);
    }

    public List<TagApiDto> getTags() {
        List<Tag> tags = pictureRepository.getTags();
        orderTagsByMostTrending(tags);

        return tagAssembler.toDto(tags);
    }

    public UploadPictureResponseDto postPicture(UploadPictureDto uploadPictureDto, HttpHeaders headers) {
        authenticationService.authenticateWithUserId(headers, uploadPictureDto.userId);

        Picture picture = pictureAssembler.toDomain(uploadPictureDto);
        File file = fileConverter.convert(uploadPictureDto.file);

        pictureRepository.postPicture(picture, file);

        file.delete();

        return pictureAssembler.toDto(String.valueOf(picture.getId()));
    }

    public PictureApiDto getPictureByPictureId(String userId, long pictureId) {
        Picture picture = pictureRepository.getPictureByPictureId(userId, pictureId);
        return pictureAssembler.toDto(picture);
    }

    public List<PictureApiDto> getPicturesByUserId(String userId) {
        List<Picture> pictures = pictureRepository.getPicturesByUserId(userId);
        return pictureAssembler.toDto(pictures);
    }

    public void deletePictureByPictureId(HttpHeaders headers, String userId, long pictureId) {
        authenticationService.authenticateWithUserId(headers, userId);
        pictureRepository.deletePictureByPictureId(pictureId, userId);
    }

    public PictureApiDto putPicture(HttpHeaders headers, String userId, long pictureId,
                                    UpdatePictureApiDto updatePictureApiDto) {
        authenticationService.authenticateWithUserId(headers, userId);
        pictureValidator.validateUpdatePictureApiDto(updatePictureApiDto);
        Picture picture = pictureAssembler.toDomain(updatePictureApiDto, userId, pictureId);
        pictureRepository.putPicture(picture);

        return pictureAssembler.toDto(picture);
    }

    public void putLike(String userId, PutLikeApiDto putLikeApiDto, long pictureId, HttpHeaders headers) {
        pictureValidator.validatePutLikeApiDto(putLikeApiDto);
        escapeInjection(userId, putLikeApiDto);
        authenticationService.authenticateWithUserId(headers, putLikeApiDto.fromUserId);
        pictureRepository.putLike(userId, putLikeApiDto.fromUserId, pictureId);
        notificationService.notifyAction(userId, putLikeApiDto.fromUserId, pictureId, NotificationEnum.LIKE);
    }

    public void putComment(String userId, PutCommentApiDto putCommentApiDto, long pictureId, HttpHeaders headers) {
        pictureValidator.validatePutCommentApiDto(putCommentApiDto);
        escapeInjection(userId, putCommentApiDto);
        authenticationService.authenticateWithUserId(headers, putCommentApiDto.fromUserId);
        pictureRepository.putComment(userId, putCommentApiDto.fromUserId, pictureId, putCommentApiDto.comment);
        notificationService.notifyAction(userId, putCommentApiDto.fromUserId, pictureId, NotificationEnum.COMMENT);
    }

    private void escapeInjection(String userId, PutLikeApiDto putLikeApiDto) {
        htmlEscape(userId);
        htmlEscape(putLikeApiDto.fromUserId);
    }

    private void escapeInjection(String userId, PutCommentApiDto putCommentApiDto) {
        htmlEscape(userId);
        htmlEscape(putCommentApiDto.fromUserId);
        htmlEscape(putCommentApiDto.comment);
    }

    private void orderTagsByMostTrending(List<Tag> tags) {
        Collections.sort(tags, new Comparator<Tag>() {
            @Override
            public int compare(Tag tag, Tag t1) {
                return tag.compareTo(t1);
            }
        });
    }
}
