package com.ulaval.web.ugram.service.picture.assembler;

import static org.springframework.web.util.HtmlUtils.htmlEscape;

import com.ulaval.web.ugram.domain.picture.Picture;
import com.ulaval.web.ugram.service.picture.dto.PictureApiDto;
import com.ulaval.web.ugram.service.picture.dto.UpdatePictureApiDto;
import com.ulaval.web.ugram.service.picture.dto.UploadPictureDto;
import com.ulaval.web.ugram.service.picture.dto.UploadPictureResponseDto;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;
import org.springframework.web.util.HtmlUtils;

@Component
public class PictureAssembler {

    public PictureApiDto toDto(Picture picture) {
        PictureApiDto pictureApiDto = new PictureApiDto();
        pictureApiDto.id = picture.getId();
        pictureApiDto.createdDate = picture.getCreatedDate();
        pictureApiDto.mentions = picture.getMentions();
        pictureApiDto.description = picture.getDescription();
        pictureApiDto.tags = picture.getTags();
        pictureApiDto.url = picture.getUrl();
        pictureApiDto.userId = picture.getUserId();
        pictureApiDto.comments = picture.getComments();
        pictureApiDto.likes = picture.getLikes();

        escapeHtml(pictureApiDto);

        return pictureApiDto;
    }

    public List<PictureApiDto> toDto(List<Picture> pictures) {
        List<PictureApiDto> picturesApiDto = new ArrayList<>();

        for (Picture picture : pictures) {
            PictureApiDto pictureApiDto = toDto(picture);
            picturesApiDto.add(pictureApiDto);
        }

        return picturesApiDto;
    }

    public UploadPictureResponseDto toDto(String id) {
        UploadPictureResponseDto uploadPictureResponseDto = new UploadPictureResponseDto();
        uploadPictureResponseDto.id = id;

        return uploadPictureResponseDto;
    }

    public Picture toDomain(UploadPictureDto uploadPictureDto) {
        escapeHtml(uploadPictureDto);

        return new Picture(uploadPictureDto.description,
            uploadPictureDto.mentions,
            uploadPictureDto.tags,
            uploadPictureDto.userId);
    }

    public Picture toDomain(UpdatePictureApiDto updatePictureApiDto, String userId, long pictureId) {
        escapeHtml(updatePictureApiDto);

        return new Picture(updatePictureApiDto.description,
            updatePictureApiDto.mentions,
            updatePictureApiDto.tags, userId, pictureId);
    }

    private void escapeHtml(PictureApiDto pictureApiDto) {
        pictureApiDto.userId = htmlEscape(pictureApiDto.userId);
        pictureApiDto.description = htmlEscape(pictureApiDto.description);
        pictureApiDto.mentions = escapeHtmlList(pictureApiDto.mentions);
        pictureApiDto.tags = escapeHtmlList(pictureApiDto.tags);
    }

    private void escapeHtml(UploadPictureDto uploadPictureDto) {
        uploadPictureDto.userId = htmlEscape(uploadPictureDto.userId);
        uploadPictureDto.description = htmlEscape(uploadPictureDto.description);
        uploadPictureDto.mentions = escapeHtmlList(uploadPictureDto.mentions);
        uploadPictureDto.tags = escapeHtmlList(uploadPictureDto.tags);
    }

    private void escapeHtml(UpdatePictureApiDto updatePictureApiDto) {
        updatePictureApiDto.description = htmlEscape(updatePictureApiDto.description);
        updatePictureApiDto.mentions = escapeHtmlList(updatePictureApiDto.mentions);
        updatePictureApiDto.tags = escapeHtmlList(updatePictureApiDto.tags);
    }

    private List<String> escapeHtmlList(List<String> list) {
        return list.stream().map(HtmlUtils::htmlEscape).collect(Collectors.toList());
    }
}