package com.ulaval.web.ugram.service.picture.dto;

import java.util.List;
import java.util.Map;

public class PictureApiDto {

    public long id;
    public long createdDate;
    public String description;
    public List<String> mentions;
    public List<String> tags;
    public String url;
    public String userId;
    public List<Map> comments;
    public List<String> likes;
}
