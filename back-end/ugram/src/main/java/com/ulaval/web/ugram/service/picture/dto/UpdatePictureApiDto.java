package com.ulaval.web.ugram.service.picture.dto;

import java.util.List;

public class UpdatePictureApiDto {

    public String description;
    public List<String> mentions;
    public List<String> tags;
}
