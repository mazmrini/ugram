package com.ulaval.web.ugram.service.picture.dto;

import java.util.List;
import org.springframework.web.multipart.MultipartFile;

public class UploadPictureDto {
    public String userId;
    public MultipartFile file;
    public String description;
    public List<String> mentions;
    public List<String> tags;
}
