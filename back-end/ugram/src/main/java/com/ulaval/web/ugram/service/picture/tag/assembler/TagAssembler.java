package com.ulaval.web.ugram.service.picture.tag.assembler;

import com.ulaval.web.ugram.domain.picture.tag.Tag;
import com.ulaval.web.ugram.service.picture.tag.dto.TagApiDto;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class TagAssembler {

    public List<TagApiDto> toDto(List<Tag> tags) {
        List<TagApiDto> tagsApiDto = new ArrayList<>();

        for (Tag tag: tags) {
            TagApiDto tagApiDto = toDto(tag);
            tagsApiDto.add(tagApiDto);
        }

        return tagsApiDto;
    }

    public TagApiDto toDto(Tag tag) {
        TagApiDto tagApiDto = new TagApiDto();
        tagApiDto.tag = tag.getTag();
        tagApiDto.usage = tag.getUsage();

        return tagApiDto;
    }
}
