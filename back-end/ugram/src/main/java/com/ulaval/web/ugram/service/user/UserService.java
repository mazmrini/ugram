package com.ulaval.web.ugram.service.user;

import com.ulaval.web.ugram.domain.user.User;
import com.ulaval.web.ugram.domain.user.UserRepository;
import com.ulaval.web.ugram.domain.user.validator.UserValidator;
import com.ulaval.web.ugram.infrastructure.exception.ItemNotFoundException;
import com.ulaval.web.ugram.service.AuthenticationService;
import com.ulaval.web.ugram.service.dto.FacebookInformation;
import com.ulaval.web.ugram.service.exception.FacebookAccountAlreadyLinkedException;
import com.ulaval.web.ugram.service.exception.UsernameAlreadyExistException;
import com.ulaval.web.ugram.service.user.assembler.UserApiAssembler;
import com.ulaval.web.ugram.service.user.dto.CreateUserApiDto;
import com.ulaval.web.ugram.service.user.dto.UpdateUserApiDto;
import com.ulaval.web.ugram.service.user.dto.UserApiDto;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private UserApiAssembler assembler;
    private UserRepository userRepository;
    private AuthenticationService<FacebookInformation> authenticationService;
    private UserValidator userValidator;

    @Autowired
    public UserService(UserApiAssembler assembler,
                       UserRepository userRepository,
                       AuthenticationService<FacebookInformation> authenticationService,
                       UserValidator userValidator) {
        this.assembler = assembler;
        this.userRepository = userRepository;
        this.authenticationService = authenticationService;
        this.userValidator = userValidator;
    }

    public UserApiDto getUser(String userId) {
        User user = userRepository.fetchUser(userId);

        return assembler.toDto(user);
    }

    public UserApiDto putUser(HttpHeaders headers, String userId, UpdateUserApiDto updateUserApiDto) {
        User user = authenticationService.authenticateWithUserId(headers, userId);

        userValidator.validateUpdateUserApiDto(updateUserApiDto);

        User updatedUser = assembler.toDomain(user, updateUserApiDto);

        userRepository.putUser(updatedUser);

        return assembler.toDto(updatedUser);
    }

    public List<UserApiDto> getUsers() {
        List<User> users = userRepository.fetchUsers();

        return assembler.toDto(users);
    }

    public void deleteUser(HttpHeaders headers, String userId) {
        authenticationService.authenticateWithUserId(headers, userId);

        userRepository.deleteUser(userId);
    }

    public void createUser(HttpHeaders headers, CreateUserApiDto createUserApiDto) {
        FacebookInformation facebookInformation = authenticationService.authenticate(headers);

        userValidator.validateCreateUserApiDto(createUserApiDto);

        validateUsernameIsAvailable(createUserApiDto.id);
        validateFacebookIdIsAvailable(facebookInformation.id);

        User user = assembler.toDomain(createUserApiDto, facebookInformation);

        userRepository.createUser(user);
    }

    private void validateUsernameIsAvailable(String username) {
        try {
            userRepository.fetchUser(username);
            throw new UsernameAlreadyExistException();
        }
        catch (ItemNotFoundException e) {
            // Username is free.
        }
    }

    private void validateFacebookIdIsAvailable(String facebookId) {
        try {
            userRepository.fetchUserByFacebookId(facebookId);
            throw new FacebookAccountAlreadyLinkedException();
        }
        catch (ItemNotFoundException e) {
            // FacebookId is free.
        }
    }
}
