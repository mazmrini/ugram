package com.ulaval.web.ugram.service.user.assembler;

import static org.springframework.web.util.HtmlUtils.htmlEscape;

import com.ulaval.web.ugram.domain.user.User;
import com.ulaval.web.ugram.service.dto.FacebookInformation;
import com.ulaval.web.ugram.service.user.dto.CreateUserApiDto;
import com.ulaval.web.ugram.service.user.dto.UpdateUserApiDto;
import com.ulaval.web.ugram.service.user.dto.UserApiDto;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class UserApiAssembler {

    public User toDomain(User user, UpdateUserApiDto updateDto) {
        escapeHtml(updateDto);

        return new User(user.getId(),
            user.getAuthId(),
            updateDto.firstName,
            updateDto.lastName,
            updateDto.email,
            updateDto.phoneNumber,
            user.getPictureUrl());
    }

    public User toDomain(CreateUserApiDto createUserApiDto, FacebookInformation facebookInformation) {
        escapeHtml(createUserApiDto);

        return new User(createUserApiDto.id,
            facebookInformation.id,
            createUserApiDto.firstName,
            createUserApiDto.lastName,
            createUserApiDto.email,
            createUserApiDto.phoneNumber,
            facebookInformation.pictureUrl);
    }

    public UserApiDto toDto(User user) {
        UserApiDto userApiDto = new UserApiDto();
        userApiDto.id = user.getId();
        userApiDto.email = user.getEmail();
        userApiDto.firstName = user.getFirstName();
        userApiDto.lastName = user.getLastName();
        userApiDto.phoneNumber = user.getPhoneNumber();
        userApiDto.pictureUrl = user.getPictureUrl();
        userApiDto.registrationDate = user.getRegistrationDate();

        escapeHtml(userApiDto);

        return userApiDto;
    }

    public List<UserApiDto> toDto(List<User> users) {
        List<UserApiDto> userApiDtos = new ArrayList<>();
        for (User user : users) {
            UserApiDto userApiDto = toDto(user);
            userApiDtos.add(userApiDto);
        }
        return userApiDtos;
    }

    private void escapeHtml(UserApiDto userApiDto) {
        userApiDto.id = htmlEscape(userApiDto.id);
        userApiDto.firstName = htmlEscape(userApiDto.firstName);
        userApiDto.lastName = htmlEscape(userApiDto.lastName);
        userApiDto.email = htmlEscape(userApiDto.email);
    }

    private void escapeHtml(CreateUserApiDto createUserApiDto) {
        createUserApiDto.id = htmlEscape(createUserApiDto.id);
        createUserApiDto.firstName = htmlEscape(createUserApiDto.firstName);
        createUserApiDto.lastName = htmlEscape(createUserApiDto.lastName);
        createUserApiDto.email = htmlEscape(createUserApiDto.email);
    }

    private void escapeHtml(UpdateUserApiDto updateUserApiDto) {
        updateUserApiDto.firstName = htmlEscape(updateUserApiDto.firstName);
        updateUserApiDto.lastName = htmlEscape(updateUserApiDto.lastName);
        updateUserApiDto.email = htmlEscape(updateUserApiDto.email);
    }
}
