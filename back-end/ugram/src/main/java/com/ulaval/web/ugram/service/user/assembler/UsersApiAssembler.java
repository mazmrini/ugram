package com.ulaval.web.ugram.service.user.assembler;

import com.ulaval.web.ugram.service.user.dto.UserApiDto;
import com.ulaval.web.ugram.service.user.dto.UsersApiDto;
import com.ulaval.web.ugram.util.pagination.PaginationResponse;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class UsersApiAssembler {

    public UsersApiDto assemble(PaginationResponse paginationResponse) {
        UsersApiDto usersApiDto = new UsersApiDto();
        usersApiDto.items = (List<UserApiDto>) paginationResponse.items;
        usersApiDto.totalPage = paginationResponse.totalPage;
        usersApiDto.totalEntries = paginationResponse.totalEntries;

        return usersApiDto;
    }
}
