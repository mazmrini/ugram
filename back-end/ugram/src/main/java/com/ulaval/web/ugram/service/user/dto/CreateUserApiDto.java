package com.ulaval.web.ugram.service.user.dto;

public class CreateUserApiDto {

    public String id;
    public String firstName;
    public String lastName;
    public String email;
    public long phoneNumber;
}
