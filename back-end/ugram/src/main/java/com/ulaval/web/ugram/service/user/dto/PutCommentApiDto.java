package com.ulaval.web.ugram.service.user.dto;

public class PutCommentApiDto {

    public String fromUserId;
    public String comment;
}
