package com.ulaval.web.ugram.service.user.dto;

public class    UpdateUserApiDto {

    public String firstName;
    public String lastName;
    public String email;
    public long phoneNumber;
}
