package com.ulaval.web.ugram.service.user.dto;

public class UserApiDto {

    public String id;
    public String firstName;
    public String lastName;
    public String email;
    public String pictureUrl;
    public long phoneNumber;
    public long registrationDate;
}
