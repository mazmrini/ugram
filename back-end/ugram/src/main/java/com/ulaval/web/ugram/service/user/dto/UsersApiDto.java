package com.ulaval.web.ugram.service.user.dto;

import java.util.List;

public class UsersApiDto {

    public List<UserApiDto> items;
    public int totalPage;
    public int totalEntries;

}
