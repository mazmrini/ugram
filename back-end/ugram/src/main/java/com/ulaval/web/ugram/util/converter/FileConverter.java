package com.ulaval.web.ugram.util.converter;

import com.ulaval.web.ugram.util.converter.exception.InvalidFileException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class FileConverter {

    public File convert(MultipartFile multipartFile) {
        File convFile = new File(multipartFile.getOriginalFilename());
        try {
            convFile.createNewFile();
            FileOutputStream fos = new FileOutputStream(convFile);
            fos.write(multipartFile.getBytes());
            fos.close();
        }
        catch (IOException e) {
            throw new InvalidFileException();
        }

        return convFile;
    }
}
