package com.ulaval.web.ugram.util.converter.exception;

public class InvalidFileException extends RuntimeException {

    private static final String EXCEPTION_MESSAGE = "Error while converting file";

    public InvalidFileException() {
        super(EXCEPTION_MESSAGE);
    }
}
