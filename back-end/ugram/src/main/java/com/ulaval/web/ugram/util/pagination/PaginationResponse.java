package com.ulaval.web.ugram.util.pagination;

import java.util.List;

public class PaginationResponse<T> {

    public List<T> items;
    public int totalPage;
    public int totalEntries;

    public PaginationResponse(List<T> items, int totalPage, int totalEntries) {
        this.items = items;
        this.totalPage = totalPage;
        this.totalEntries = totalEntries;
    }

    public PaginationResponse() {
    }
}
