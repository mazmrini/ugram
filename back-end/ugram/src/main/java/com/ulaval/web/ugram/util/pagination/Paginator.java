package com.ulaval.web.ugram.util.pagination;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class Paginator<T> {

    private static final int OFFSET_ADJUSTMENT = 1;
    private static final int MINIMAL_PAGE = 1;
    private static final int PER_PAGE = 6;


    public PaginationResponse<T> paginate(List<T> objects, int page, int perPage) {
        if (!isValidPerPage(perPage)) {
            perPage = PER_PAGE;
        }
        int totalPages = calculateTotalPages(objects, perPage);
        if (!isValidPage(page, totalPages)) {
            return new PaginationResponse<T>(new ArrayList<>(), totalPages, objects.size());
        }
        int startingOffset = calculateStartingOffset(page, perPage);
        int endingOffset = calculateEndingOffset(objects, startingOffset, perPage);
        return new PaginationResponse<T>(objects.subList(startingOffset, endingOffset),
            totalPages, objects.size());
    }

    private boolean isValidPerPage(int perPage) {
        return perPage > 0;
    }

    private int calculateStartingOffset(int page, int perPage) {
        return (page - OFFSET_ADJUSTMENT) * perPage;
    }

    private boolean isValidPage(int page, int totalPages) {
        return page >= MINIMAL_PAGE && page <= totalPages;
    }

    private int calculateTotalPages(List<?> objects, int perPage) {
        return (int) Math.ceil((double) objects.size() / perPage);
    }

    private int calculateEndingOffset(List<?> objects, int startingOffset, int perPage) {
        if (startingOffset + perPage < objects.size()) {
            return startingOffset + perPage;
        }
        return objects.size();
    }
}
