package com.ulaval.web.ugram.util.pagination.exception;

public class InvalidPageException extends RuntimeException {

    private static final String EXCEPTION_MESSAGE = "Invalid page number";

    public InvalidPageException() {
        super(EXCEPTION_MESSAGE);
    }
}
