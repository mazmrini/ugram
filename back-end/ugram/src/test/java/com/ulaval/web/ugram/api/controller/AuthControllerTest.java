package com.ulaval.web.ugram.api.controller;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.verify;

import com.ulaval.web.ugram.service.login.LoginService;
import com.ulaval.web.ugram.service.user.UserService;
import com.ulaval.web.ugram.service.user.dto.CreateUserApiDto;
import com.ulaval.web.ugram.service.user.dto.UserApiDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

@RunWith(MockitoJUnitRunner.class)
public class AuthControllerTest {

    @Mock
    public CreateUserApiDto createUserApiDto;
    @Mock
    public UserApiDto userApiDto;
    @Mock
    public HttpHeaders headers;

    @Mock
    private LoginService loginService;
    @Mock
    private UserService userService;

    @InjectMocks
    private AuthController authController;

    @Before
    public void setUpService() {
        willReturn(userApiDto).given(loginService).login(headers);
    }

    @Test
    public void givenValidToken_whenLogin_thenReturnsOKStatusCode() {
        HttpStatus status = authController.login(headers).getStatusCode();

        assertThat(status, is(equalTo(HttpStatus.OK)));
    }

    @Test
    public void givenValidToken_whenLogin_thenReturnsExpectedDto() {
        UserApiDto response = authController.login(headers).getBody();

        assertThat(response, is(equalTo(userApiDto)));
    }

    @Test
    public void givenValidToken_whenSignUp_thenReturnsCreatedStatusCode() {
        HttpStatus status = authController.signUp(headers, createUserApiDto).getStatusCode();

        assertThat(status, is(equalTo(HttpStatus.CREATED)));
    }

    @Test
    public void givenValidToken_whenSignUp_thenResponseBodyIsEmpty() {
        Object response = authController.signUp(headers, createUserApiDto).getBody();

        assertThat(response, is(nullValue()));
    }

    @Test
    public void givenValidToken_whenSignUp_thenUserServiceCreateUserIsCalledOnce() {
        authController.signUp(headers, createUserApiDto);

        verify(userService).createUser(headers, createUserApiDto);
    }
}
