package com.ulaval.web.ugram.api.controller;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class HomeControllerTest {

    private HomeController homeController;

    @Before
    public void setUp() {
        homeController = new HomeController();
    }

    @Test
    public void whenCallingHome_expectWelcomeMessage() {
        ResponseEntity<String> response = homeController.getHome();

        String message = response.getBody();

        assertThat(message, is(equalTo(HomeController.WELCOME_MESSAGE)));
    }

    @Test
    public void whenCallingHome_expectOkStatusCode() {
        ResponseEntity<String> response = homeController.getHome();

        HttpStatus statusCode = response.getStatusCode();

        assertThat(statusCode, is(equalTo(HttpStatus.OK)));
    }
}