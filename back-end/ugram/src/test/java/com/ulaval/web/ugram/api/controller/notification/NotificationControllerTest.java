package com.ulaval.web.ugram.api.controller.notification;

import com.ulaval.web.ugram.service.notification.NotificationService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpHeaders;

@RunWith(MockitoJUnitRunner.class)
public class NotificationControllerTest {

    private static final String USER_ID = "mar";
    @Mock
    private NotificationService notificationService;
    @Mock
    private HttpHeaders httpHeaders;

    private NotificationController notificationController;

    @Before
    public void setUp() {
        notificationController = new NotificationController(notificationService);
    }

    @Test
    public void givenNotificationRequest_whenGetNotification_then_getNotificationIsCalled() {
        notificationController.getNotification(USER_ID, httpHeaders);

        Mockito.verify(notificationService).getNotification(httpHeaders, USER_ID);
    }
}