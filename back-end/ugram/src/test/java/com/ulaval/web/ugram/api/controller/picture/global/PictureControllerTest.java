package com.ulaval.web.ugram.api.controller.picture.global;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.ulaval.web.ugram.service.picture.PictureService;
import com.ulaval.web.ugram.service.picture.dto.PictureApiDto;
import com.ulaval.web.ugram.util.pagination.PaginationResponse;
import com.ulaval.web.ugram.util.pagination.Paginator;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@RunWith(MockitoJUnitRunner.class)
public class PictureControllerTest {

    private static final int PER_PAGE = 6;
    private static final int PAGE = 1;
    @Mock
    Paginator<PictureApiDto> paginator;

    @Mock
    PictureService pictureService;

    private PictureController pictureController;
    private List<PictureApiDto> pictureApiDto;


    @Before
    public void setUp() {
        pictureApiDto = new ArrayList<>();
        PaginationResponse paginationResponse = new PaginationResponse();
        when(pictureService.getPictures()).thenReturn(pictureApiDto);
        when(paginator.paginate(pictureApiDto, PER_PAGE, PAGE)).thenReturn(paginationResponse);

        pictureController = new PictureController(pictureService, paginator);
    }

    @Test
    public void pictureController_getPictures_getPicturesFromPictureService() {
        pictureController.getPictures(PAGE, PER_PAGE);

        verify(pictureService).getPictures();
    }

    @Test
    public void pictureController_getPictures_paginateFromPaginator() {
        pictureController.getPictures(PAGE, PER_PAGE);

        verify(paginator).paginate(pictureApiDto, PAGE, PER_PAGE);
    }

    @Test
    public void validRequest_getPictures_returnStatusOk() {
        ResponseEntity response = pictureController.getPictures(PAGE, PER_PAGE);

        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void validRequest_getTrendingTags_returnStatusOk() {
        ResponseEntity response = pictureController.getTrendingTags(10);

        assertEquals(HttpStatus.OK, response.getStatusCode());
    }
}