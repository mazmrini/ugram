package com.ulaval.web.ugram.api.controller.user;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.verify;

import com.ulaval.web.ugram.service.user.UserService;
import com.ulaval.web.ugram.service.user.dto.UpdateUserApiDto;
import com.ulaval.web.ugram.service.user.dto.UserApiDto;
import com.ulaval.web.ugram.util.pagination.PaginationResponse;
import com.ulaval.web.ugram.util.pagination.Paginator;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

@RunWith(MockitoJUnitRunner.class)
public class UsersControllerTest {

    private static final String A_USER_ID = "A_USER_ID";
    private static final int PAGE = 2;
    private static final int PER_PAGE = 12;

    @Mock
    private List<UserApiDto> validUserList;
    @Mock
    private UserApiDto validUserApiDto;
    @Mock
    private UpdateUserApiDto validUpdateUserApiDto;
    @Mock
    private PaginationResponse validPaginationResponse;
    @Mock
    private HttpHeaders httpHeaders;
    @Mock
    private Paginator<UserApiDto> paginator;
    @Mock
    private UserService userService;

    @InjectMocks
    private UsersController usersController;

    @Before
    public void setUp() {
        setUpGetUsers();
        setUpGetUser();
        setUpPutUser();
    }

    @Test
    public void givenValidRequest_whenGetUsers_thenReturnsOKStatusCode() {
        HttpStatus statusCode = usersController.getUsers(PAGE, PER_PAGE).getStatusCode();

        assertThat(statusCode, is(equalTo(HttpStatus.OK)));
    }

    @Test
    public void givenValidRequest_whenGetUsers_thenReturnsExpectedDto() {
        PaginationResponse response = usersController.getUsers(PAGE, PER_PAGE).getBody();

        assertThat(response, is(equalTo(validPaginationResponse)));
    }

    @Test
    public void givenValidRequest_whenGetUser_thenReturnsOKStatusCode() {
        HttpStatus statusCode = usersController.getUser(A_USER_ID).getStatusCode();

        assertThat(statusCode, is(equalTo(HttpStatus.OK)));
    }

    @Test
    public void givenValidRequest_whenGetUser_thenReturnsExpectedDto() {
        UserApiDto response = usersController.getUser(A_USER_ID).getBody();

        assertThat(response, is(equalTo(validUserApiDto)));
    }

    @Test
    public void givenValidRequest_whenPutUser_thenReturnsOKStatusCode() {
        HttpStatus statusCode = usersController.putUser(httpHeaders, A_USER_ID, validUpdateUserApiDto).getStatusCode();

        assertThat(statusCode, is(equalTo(HttpStatus.OK)));
    }

    @Test
    public void givenValidRequest_whenPutUser_thenReturnsExpectedDto() {
        UserApiDto response = usersController.putUser(httpHeaders, A_USER_ID, validUpdateUserApiDto).getBody();

        assertThat(response, is(equalTo(validUserApiDto)));
    }

    @Test
    public void givenValidRequest_whenDeleteUser_thenReturnsNoContentStatusCode() {
        HttpStatus statusCode = usersController.deleteUser(httpHeaders, A_USER_ID).getStatusCode();

        assertThat(statusCode, is(equalTo(HttpStatus.NO_CONTENT)));
    }

    @Test
    public void givenValidRequest_whenDeleteUser_thenResponseBodyIsEmpty() {
        Object body = usersController.deleteUser(httpHeaders, A_USER_ID).getBody();

        assertThat(body, is(nullValue()));
    }

    @Test
    public void givenValidRequest_whenDeleteUser_thenUserServiceDeleteUserIsCalledOnce() {
        usersController.deleteUser(httpHeaders, A_USER_ID);

        verify(userService).deleteUser(httpHeaders, A_USER_ID);
    }

    private void setUpGetUsers() {
        willReturn(validUserList).given(userService).getUsers();
        willReturn(validPaginationResponse).given(paginator).paginate(validUserList, PAGE, PER_PAGE);
    }

    private void setUpGetUser() {
        willReturn(validUserApiDto).given(userService).getUser(A_USER_ID);
    }

    private void setUpPutUser() {
        willReturn(validUserApiDto).given(userService).putUser(httpHeaders, A_USER_ID, validUpdateUserApiDto);
    }
}