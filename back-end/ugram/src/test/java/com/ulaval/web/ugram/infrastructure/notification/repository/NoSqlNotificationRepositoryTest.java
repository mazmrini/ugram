package com.ulaval.web.ugram.infrastructure.notification.repository;

import com.ulaval.web.ugram.domain.notification.Notification;
import com.ulaval.web.ugram.domain.notification.NotificationRepository;
import com.ulaval.web.ugram.infrastructure.notification.assembler.NotificationInfraAssembler;
import com.ulaval.web.ugram.infrastructure.notification.dao.NotificationDao;
import com.ulaval.web.ugram.infrastructure.notification.dto.NotificationsInfraDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class NoSqlNotificationRepositoryTest {

    private static final String USER_ID = "Ma";
    @Mock
    private NotificationInfraAssembler notificationInfraAssembler;

    @Mock
    private NotificationDao notificationDao;

    @Mock
    private Notification notification;

    @Mock
    private NotificationsInfraDto notificationsInfraDto;

    private NotificationRepository notificationRepository;

    @Before
    public void setUp() {
        Mockito.when(notification.getUserId()).thenReturn(USER_ID);
        Mockito.when(notificationDao.getNotifications(USER_ID)).thenReturn(notificationsInfraDto);
        notificationRepository = new NoSqlNotificationRepository(notificationDao, notificationInfraAssembler);
    }

    @Test
    public void givenNotificationToPersist_whenAddNotification_thenAssembleIsCalled() {
        notificationRepository.addNotification(notification);

        Mockito.verify(notificationInfraAssembler).assemble(notification);
    }

    @Test
    public void givenNotificationToPersist_whenAddNotification_thenGetNotificationsIsCalled() {
        notificationRepository.addNotification(notification);

        Mockito.verify(notificationDao).getNotifications(USER_ID);
    }

    @Test
    public void givenNotificationToPersist_whenAddNotification_thenAddNotificationIsCalled() {
        notificationRepository.addNotification(notification);

        Mockito.verify(notificationDao).addNotification(Mockito.any(NotificationsInfraDto.class));
    }

    @Test
    public void givenGetNotificationRequest_whenGetNotifications_thenGetNotificationsIsCalled() {
        notificationRepository.getNotifications(USER_ID);

        Mockito.verify(notificationDao).getNotifications(USER_ID);
    }

    @Test
    public void givenDeleteNotificationRequest_whendeleteNotifications_thendeleteNotificationsIsCalled() {
        notificationRepository.deleteNotifications(USER_ID);

        Mockito.verify(notificationDao).deleteNotifications(USER_ID);
    }
}