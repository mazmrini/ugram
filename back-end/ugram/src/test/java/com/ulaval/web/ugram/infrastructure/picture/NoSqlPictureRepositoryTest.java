package com.ulaval.web.ugram.infrastructure.picture;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.ulaval.web.ugram.domain.picture.Picture;
import com.ulaval.web.ugram.infrastructure.picture.assembler.PictureInfraAssembler;
import com.ulaval.web.ugram.infrastructure.picture.dao.PictureDao;
import com.ulaval.web.ugram.infrastructure.picture.dao.upload.PictureFileDao;
import com.ulaval.web.ugram.infrastructure.picture.dto.PictureDtoInfra;
import com.ulaval.web.ugram.infrastructure.picture.tag.TagDao;
import com.ulaval.web.ugram.infrastructure.picture.tag.assembler.TagInfraAssembler;
import com.ulaval.web.ugram.infrastructure.picture.tag.dto.TagDtoInfra;
import com.ulaval.web.ugram.infrastructure.user.dao.UserDao;
import java.io.File;
import org.junit.Before;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class NoSqlPictureRepositoryTest {

    private static final String USER_ID = "USER_ID";
    private static final long PICTURE_ID = 100;
    private static final String FROM_USER_ID = "JEAN";
    private static final String COMMENT = "a comment";

    @Mock
    private Picture picture;
    @Mock
    private File file;
    @Mock
    private PictureDtoInfra pictureDtoInfra;

    @Mock
    private PictureDao pictureDao;
    @Mock
    private UserDao userDao;
    @Mock
    private PictureInfraAssembler pictureAssembler;
    @Mock
    private PictureFileDao pictureFileDao;
    @Mock
    private TagDao tagDao;
    @Mock
    private TagInfraAssembler tagInfraAssembler;

    @InjectMocks
    NoSqlPictureRepository noSqlPictureRepository;

    @Before
    public void setUp() {
        when(pictureAssembler.toDto(any(Picture.class))).thenReturn(pictureDtoInfra);
    }

    @Test
    public void getPictures_callsPictureDaoGetPictures() {
        noSqlPictureRepository.getPictures();

        verify(pictureDao).getPictures();
    }

    @Test
    public void getPictures_callsPictureAssemblerAssemble() {
        noSqlPictureRepository.getPictures();

        verify(pictureAssembler).assemble(anyListOf(PictureDtoInfra.class));
    }

    @Test
    public void pictureFile_postPictures_callsPictureFileDaoUploadPicture() {
        noSqlPictureRepository.postPicture(picture, file);

        verify(pictureFileDao).uploadPicture(file);
    }

    @Test
    public void pictureFile_postPictures_callsPictureAssemblerToDto() {
        noSqlPictureRepository.postPicture(picture, file);

        verify(pictureAssembler).toDto(picture);
    }

    @Test
    public void pictureFile_postPictures_callsPictureDaoCreatePicture() {
        noSqlPictureRepository.postPicture(picture, file);

        verify(pictureDao).createPicture(any(PictureDtoInfra.class));
    }

    @Test
    public void pictureFile_postPictures_callsTagDaoPutTags() {
        noSqlPictureRepository.postPicture(picture, file);

        verify(tagDao).putTags(anyListOf(String.class));
    }

    @Test
    public void userId_getPicturesByUserId_callsUserDaoFetchUser() {
        noSqlPictureRepository.getPicturesByUserId(USER_ID);

        verify(userDao).fetchUser(USER_ID);
    }

    @Test
    public void userId_getPicturesByUserId_callsPictureDaoGetPicturesByUserId() {
        noSqlPictureRepository.getPicturesByUserId(USER_ID);

        verify(pictureDao).getPicturesByUserId(USER_ID);
    }

    @Test
    public void userId_getPicturesByUserId_callsPictureAssemblerAssemble() {
        noSqlPictureRepository.getPicturesByUserId(USER_ID);

        verify(pictureAssembler).assemble(anyListOf(PictureDtoInfra.class));
    }

    @Test
    public void pictureAndUserIds_deletePictureByPictureId_callsPictureDaoDeletePictureByPictureId() {
        noSqlPictureRepository.deletePictureByPictureId(PICTURE_ID, USER_ID);

        verify(pictureDao).deletePictureByPictureId(PICTURE_ID, USER_ID);
    }

    @Test
    public void pictureAndUserIds_getPictureByPictureId_callsPictureDaoGetPictureByPictureId() {
        noSqlPictureRepository.getPictureByPictureId(USER_ID, PICTURE_ID);

        verify(pictureDao).getPictureByPictureId(USER_ID, PICTURE_ID);
    }

    @Test
    public void pictureAndUserIds_getPictureByPictureId_callsPictureAssemblerAssemble() {
        noSqlPictureRepository.getPictureByPictureId(USER_ID, PICTURE_ID);

        verify(pictureAssembler).assemble(any(PictureDtoInfra.class));
    }

    @Test
    public void picture_putPicture_callsPictureAssemblerToDto() {
        noSqlPictureRepository.putPicture(picture);

        verify(pictureAssembler).toDto(picture);
    }

    @Test
    public void picture_putPicture_callsPictureDaoPutPicture() {
        noSqlPictureRepository.putPicture(picture);

        verify(pictureDao).putPicture(any(PictureDtoInfra.class));
    }

    @Test
    public void getTags_callsTagDaoGetTags() {
        noSqlPictureRepository.getTags();

        verify(tagDao).getTags();
    }

    @Test
    public void getPictures_callsTagAssemblerToDomain() {
        noSqlPictureRepository.getTags();

        verify(tagInfraAssembler).toDomain(anyListOf(TagDtoInfra.class));
    }

    @Test
    public void putLikeRequest_putLike_getPictureByPictureIdIsCalled() {
        when(pictureDao.getPictureByPictureId(USER_ID, PICTURE_ID)).thenReturn(createPictureDtoInfra());
        noSqlPictureRepository.putLike(USER_ID, FROM_USER_ID, PICTURE_ID);
        verify(pictureDao).getPictureByPictureId(USER_ID, PICTURE_ID);
    }

    @Test
    public void putCommentRequest_putComment_getPictureByPictureIdIsCalled() {
        when(pictureDao.getPictureByPictureId(USER_ID, PICTURE_ID)).thenReturn(createPictureDtoInfra());
        noSqlPictureRepository.putComment(USER_ID, FROM_USER_ID, PICTURE_ID, COMMENT);
        verify(pictureDao).getPictureByPictureId(USER_ID, PICTURE_ID);
    }

    private PictureDtoInfra createPictureDtoInfra() {
        List<String> likes = new ArrayList<>();
        List<Map> comments = new ArrayList<>();
        PictureDtoInfra pictureDtoInfra = new PictureDtoInfra();
        pictureDtoInfra.likes = likes;
        pictureDtoInfra.comments = comments;
        return pictureDtoInfra;
    }
}