package com.ulaval.web.ugram.infrastructure.user;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import com.ulaval.web.ugram.domain.user.User;
import com.ulaval.web.ugram.infrastructure.user.assembler.UserInfraAssembler;
import com.ulaval.web.ugram.infrastructure.user.dao.UserDao;
import com.ulaval.web.ugram.infrastructure.user.dto.UserInfraDto;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class NoSqlUserRepositoryTest {

    private static final String A_USER_ID = "A_USER_ID";
    private static final String A_FACEBOOK_ID = "A_FACEBOOK_ID";

    @Mock
    UserInfraDto userInfraDto;
    @Mock
    User expectedUser;

    @Mock
    private UserDao userDao;
    @Mock
    private UserInfraAssembler userInfraAssembler;

    @InjectMocks
    private NoSqlUserRepository userRepository;

    @Test
    public void whenFetchingUser_thenReturnsExpectedDto() {
        List<UserInfraDto> userInfraDtoList = mock(List.class);
        List<User> expectedUsers = mock(List.class);

        willReturn(userInfraDtoList).given(userDao).fetchUsers();
        willReturn(expectedUsers).given(userInfraAssembler).toDomain(userInfraDtoList);

        List<User> users = userRepository.fetchUsers();

        assertThat(users, is(equalTo(expectedUsers)));
    }

    @Test
    public void whenFetchUser_thenReturnsExpectedDto() {
        willReturn(userInfraDto).given(userDao).fetchUser(A_USER_ID);
        willReturn(expectedUser).given(userInfraAssembler).toDomain(userInfraDto);

        User user = userRepository.fetchUser(A_USER_ID);

        assertThat(user, is(equalTo(expectedUser)));
    }

    @Test
    public void whenFetchUserByFacebookId_thenReturnsExpectedDto() {
        willReturn(userInfraDto).given(userDao).fetchUserByFacebookId(A_FACEBOOK_ID);
        willReturn(expectedUser).given(userInfraAssembler).toDomain(userInfraDto);

        User user = userRepository.fetchUserByFacebookId(A_FACEBOOK_ID);

        assertThat(user, is(equalTo(expectedUser)));
    }

    @Test
    public void givenValidUser_whenPuttingUser_thenDaoPutUserIsCalledOnce() {
        User user = expectedUser;
        willReturn(userInfraDto).given(userInfraAssembler).toInfra(user);

        userRepository.putUser(user);

        verify(userDao).putUser(userInfraDto);
    }

    @Test
    public void whenDeletingUser_thenDaoDeletesTheUser() {
        userRepository.deleteUser(A_USER_ID);

        verify(userDao).deleteUser(A_USER_ID);
    }

    @Test
    public void whenCreatingUser_thenDaoCreatesTheUser() {
        User user = expectedUser;
        willReturn(userInfraDto).given(userInfraAssembler).toInfra(user);

        userRepository.createUser(user);

        verify(userDao).createUser(userInfraDto);
    }
}