package com.ulaval.web.ugram.infrastructure.user.dao;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.DeleteItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.dynamodbv2.model.Condition;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import com.amazonaws.services.dynamodbv2.model.GetItemResult;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.ulaval.web.ugram.infrastructure.NoSqlRequestFactory;
import com.ulaval.web.ugram.infrastructure.exception.ItemNotFoundException;
import com.ulaval.web.ugram.infrastructure.user.assembler.UserInfraAssembler;
import com.ulaval.web.ugram.infrastructure.user.dto.UserInfraDto;
import java.util.List;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class NoSqlUserDaoTest {

    private static final String A_USER_ID = "A_USER_ID";
    private static final String A_FACEBOOK_ID = "A_FACEBOOK_ID";
    private static final String AN_EMAIL = "AN_EMAIL";
    private static final String A_FIRST_NAME = "A_FIRST_NAME";
    private static final String A_LAST_NAME = "A_LAST_NAME";
    private static final String A_PICTURE_URL = "A_PICTURE_URL";
    private static final long A_PHONE_NUMBER = 123;
    private static final long A_REGISTRATION_DATE = 91023901;

    private UserInfraDto userInfraDtoRequest;

    @Mock
    private UserInfraDto expectedUserInfraDto;
    @Mock
    private GetItemRequest getItemRequest;
    @Mock
    private GetItemResult getItemResult;
    @Mock
    private Map<String, AttributeValue> getItemContent;
    @Mock
    private ScanRequest scanRequest;
    @Mock
    private ScanResult scanResult;
    @Mock
    private List scanResultItems;
    @Mock
    private Map<String, Condition> scanFilter;
    @Mock
    private Condition facebookCondition;
    @Mock
    private UpdateItemSpec updateItemSpec;
    @Mock
    private DeleteItemSpec deleteItemSpec;

    @Mock
    private Table table;

    @Mock
    private NoSqlRequestFactory noSqlRequestFactory;
    @Mock
    private UserInfraAssembler userInfraAssembler;
    @Mock
    private AmazonDynamoDB amazonDynamoDB;
    @Mock
    private DynamoDB dynamoDB;

    private NoSqlUserDao noSqlUserDao;

    @Before
    public void setUp() {
        userInfraDtoRequest = new UserInfraDto();
        userInfraDtoRequest.id = A_USER_ID;
        userInfraDtoRequest.authId = A_FACEBOOK_ID;
        userInfraDtoRequest.email = AN_EMAIL;
        userInfraDtoRequest.firstName = A_FIRST_NAME;
        userInfraDtoRequest.lastName = A_LAST_NAME;
        userInfraDtoRequest.phoneNumber = A_PHONE_NUMBER;
        userInfraDtoRequest.registrationDate = A_REGISTRATION_DATE;
        userInfraDtoRequest.pictureUrl = A_PICTURE_URL;

        willReturn(table).given(dynamoDB).getTable(UserTableConstants.TABLE_NAME);

        setUpNoSqlRequestFactory();

        noSqlUserDao = new NoSqlUserDao(userInfraAssembler, amazonDynamoDB, dynamoDB, noSqlRequestFactory);
    }

    @Test
    public void givenAUserId_whenFetchingUser_thenReturnsExpectedDto() {
        willReturn(getItemContent).given(getItemResult).getItem();
        willReturn(expectedUserInfraDto).given(userInfraAssembler).assemble(getItemContent);

        UserInfraDto user = noSqlUserDao.fetchUser(A_USER_ID);

        assertThat(user, is(equalTo(expectedUserInfraDto)));
    }

    @Test(expected = ItemNotFoundException.class)
    public void givenInexistentUserId_whenFetchingUser_thenItemNotFoundExceptionIsThrown() {
        willReturn(null).given(getItemResult).getItem();

        noSqlUserDao.fetchUser(A_USER_ID);
    }

    @Test
    public void givenValidFacebookId_whenFetchingUserByFacebookId_thenScanFilterContainsTheCondition() {
        noSqlUserDao.fetchUserByFacebookId(A_FACEBOOK_ID);

        verify(scanFilter).put(UserTableConstants.AUTH_ID_KEY, facebookCondition);
    }

    @Test
    public void givenValidFacebookId_whenFetchingUserByFacebookId_thenScanRequestContainsScanFilter() {
        noSqlUserDao.fetchUserByFacebookId(A_FACEBOOK_ID);

        verify(scanRequest).setScanFilter(scanFilter);
    }

    @Test
    public void givenValidFacebookId_whenFetchingUserByFacebookId_thenReturnsExpectedDto() {
        willReturn(expectedUserInfraDto).given(userInfraAssembler).assemble(getItemContent);

        UserInfraDto userInfraDto = noSqlUserDao.fetchUserByFacebookId(A_FACEBOOK_ID);

        assertThat(userInfraDto, is(equalTo(expectedUserInfraDto)));
    }

    @Test(expected = ItemNotFoundException.class)
    public void givenInvalidFacebookId_whenFetchingUserByFacebookId_thenItemNotFoundExceptionIsThrown() {
        willReturn(true).given(scanResultItems).isEmpty();

        noSqlUserDao.fetchUserByFacebookId(A_FACEBOOK_ID);
    }

    @Test
    public void whenFetchingAllUsers_thenReturnsExpectedDto() {
        List<UserInfraDto> expectedUserInfraDto = mock(List.class);
        willReturn(expectedUserInfraDto).given(userInfraAssembler).assemble(scanResult);

        List<UserInfraDto> userInfraDto = noSqlUserDao.fetchUsers();

        assertThat(userInfraDto, is(equalTo(expectedUserInfraDto)));
    }

    @Test
    public void givenValidUser_whenPuttingUser_thenTableUpdateItemIsCalledOnce() {
        noSqlUserDao.putUser(userInfraDtoRequest);

        verify(table).updateItem(updateItemSpec);
    }

    @Test(expected = ItemNotFoundException.class)
    public void givenInvalidUser_whenPuttingUser_thenItemNotFoundExceptionIsThrown() {
        willThrow(Exception.class).given(table).updateItem(any(UpdateItemSpec.class));

        noSqlUserDao.putUser(userInfraDtoRequest);
    }

    @Test
    public void givenValidUser_whenDeletingUser_thenTableDeleteItemIsCalledOnce() {
        noSqlUserDao.deleteUser(A_USER_ID);

        verify(table).deleteItem(deleteItemSpec);
    }

    @Test(expected = ItemNotFoundException.class)
    public void givenInvalidUser_whenDeletingUser_thenItemNotFoundExceptionIsThrown() {
        willThrow(Exception.class).given(table).deleteItem(any(DeleteItemSpec.class));

        noSqlUserDao.deleteUser(A_USER_ID);
    }

    @Test
    public void givenValidUser_whenCreatingUser_thenTablePutUserIsCalledOnce() {
        Item itemToCreate = createItemToCreate();

        noSqlUserDao.createUser(userInfraDtoRequest);

        verify(table).putItem(itemToCreate);
    }

    private void setUpNoSqlRequestFactory() {
        setUpGetItemRequest();
        setUpScanRequest();
        setUpScanResult();
        setUpScanFilter();
        setUpUpdateItemSpec();
        setUpDeleteItemSpec();

        willReturn(getItemRequest).given(noSqlRequestFactory).createGetItemRequest();
    }

    private Item createItemToCreate() {
        Item itemToCreate = mock(Item.class);

        willReturn(itemToCreate).given(itemToCreate).withPrimaryKey(UserTableConstants.PRIMARY_KEY, A_USER_ID);
        willReturn(itemToCreate).given(itemToCreate).withString(UserTableConstants.FIRST_NAME_KEY, A_FIRST_NAME);
        willReturn(itemToCreate).given(itemToCreate).withString(UserTableConstants.LAST_NAME_KEY, A_LAST_NAME);
        willReturn(itemToCreate).given(itemToCreate).withString(UserTableConstants.EMAIL_KEY, AN_EMAIL);
        willReturn(itemToCreate).given(itemToCreate).withString(UserTableConstants.PICTURE_URL_KEY, A_PICTURE_URL);
        willReturn(itemToCreate).given(itemToCreate).withString(UserTableConstants.AUTH_ID_KEY, A_FACEBOOK_ID);
        willReturn(itemToCreate).given(itemToCreate).withNumber(UserTableConstants.PHONE_NUMBER_KEY, A_PHONE_NUMBER);
        willReturn(itemToCreate).given(itemToCreate).withNumber(UserTableConstants.REGISTRATION_DATE_KEY, A_REGISTRATION_DATE);
        willReturn(itemToCreate).given(noSqlRequestFactory).createItem();

        return itemToCreate;
    }

    private void setUpUpdateItemSpec() {
        willReturn(updateItemSpec).given(updateItemSpec).withPrimaryKey(UserTableConstants.PRIMARY_KEY, A_USER_ID);
        willReturn(updateItemSpec).given(updateItemSpec).withUpdateExpression(any(String.class));
        willReturn(updateItemSpec).given(updateItemSpec).withValueMap(any(ValueMap.class));

        willReturn(updateItemSpec).given(noSqlRequestFactory).createUpdateItemSpec();
    }

    private void setUpDeleteItemSpec() {
        willReturn(deleteItemSpec).given(deleteItemSpec).withPrimaryKey(UserTableConstants.PRIMARY_KEY, A_USER_ID);

        willReturn(deleteItemSpec).given(noSqlRequestFactory).createDeleteItemSpec();
    }

    private void setUpScanResult() {
        willReturn(scanResult).given(amazonDynamoDB).scan(scanRequest);
        willReturn(scanResultItems).given(scanResult).getItems();
        willReturn(false).given(scanResultItems).isEmpty();
        willReturn(getItemContent).given(scanResultItems).get(0);
    }

    private void setUpScanFilter() {
        willReturn(scanFilter).given(noSqlRequestFactory).createScanFilter();

        willReturn(facebookCondition).given(noSqlRequestFactory).createCondition();
        willReturn(facebookCondition).given(facebookCondition).withAttributeValueList(any(AttributeValue.class));
        willReturn(facebookCondition).given(facebookCondition).withComparisonOperator(ComparisonOperator.EQ);
    }

    private void setUpScanRequest() {
        willReturn(scanRequest).given(noSqlRequestFactory).createScanRequest();
        willReturn(scanRequest).given(scanRequest).withTableName(UserTableConstants.TABLE_NAME);
    }

    private void setUpGetItemRequest() {
        willReturn(getItemRequest).given(getItemRequest).withKey(any(Map.class));
        willReturn(getItemRequest).given(getItemRequest).withTableName(UserTableConstants.TABLE_NAME);

        willReturn(getItemResult).given(amazonDynamoDB).getItem(getItemRequest);
    }
}