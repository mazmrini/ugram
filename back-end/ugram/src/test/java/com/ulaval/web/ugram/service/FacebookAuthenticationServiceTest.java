package com.ulaval.web.ugram.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.Assert.fail;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ulaval.web.ugram.domain.user.User;
import com.ulaval.web.ugram.domain.user.UserRepository;
import com.ulaval.web.ugram.service.dto.FacebookId;
import com.ulaval.web.ugram.service.dto.FacebookInformation;
import com.ulaval.web.ugram.service.exception.AuthenticationFailedException;
import java.net.URL;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpHeaders;

@RunWith(MockitoJUnitRunner.class)
public class FacebookAuthenticationServiceTest {

    private static final String VALID_TOKEN = "Bearer validToken10293012";
    private static final String INVALID_TOKEN = "invalid_token0192";
    private static final String VALID_PICTURE_URL = "https://graph.facebook.com/v2.5/" + VALID_TOKEN + "/picture";
    private static final String A_USER_ID = "A_USER_ID";
    private static final String A_FACEBOOK_ID = "A_FACEBOOK_ID";

    private FacebookId facebookId;
    private FacebookInformation facebookInformation;

    @Mock
    private User user;
    @Mock
    private HttpHeaders httpHeaders;

    @Mock
    private ObjectMapper objectMapper;
    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private FacebookAuthenticationService facebookAuthenticationService;

    @Before
    public void setUp() throws Exception {
        facebookId = new FacebookId();
        facebookId.id = A_FACEBOOK_ID;

        facebookInformation = new FacebookInformation();
        facebookInformation.id = A_FACEBOOK_ID;
        facebookInformation.pictureUrl = VALID_PICTURE_URL;

        willReturn(facebookId).given(objectMapper).readValue(any(URL.class), eq(FacebookId.class));

        willReturn(user).given(userRepository).fetchUser(A_USER_ID);
    }

    @Test
    public void givenHttpHeadersWithValidToken_whenAuthenticating_thenReturnsFacebookInformation() {
        givenHttpHeadersWithValidToken();

        FacebookInformation result = facebookAuthenticationService.authenticate(httpHeaders);

        assertThat(result, samePropertyValuesAs(facebookInformation));
    }

    @Test(expected = AuthenticationFailedException.class)
    public void givenHttpHeadersWithoutToken_whenAuthenticating_thenAuthenticationFailedExceptionIsThrown() {
        givenHttpHeadersWithoutToken();

        facebookAuthenticationService.authenticate(httpHeaders);
    }

    @Test(expected = AuthenticationFailedException.class)
    public void givenHttpHeadersWithInvalidToken_whenAuthenticating_thenAuthenticationFailedExceptionIsThrown()
        throws Exception {
        givenHttpHeadersWithInvalidToken();

        facebookAuthenticationService.authenticate(httpHeaders);
    }

    @Test(expected = AuthenticationFailedException.class)
    public void givenFacebookAuthFails_whenAuthenticatingWithValidToken_thenAuthenticationFailedExceptionIsThrown()
        throws Exception {
        givenFacebookAuthFails();
        givenHttpHeadersWithValidToken();

        facebookAuthenticationService.authenticate(httpHeaders);
    }

    @Test
    public void givenValidFacebookAuth_whenAuthenticatingWithUser_thenUserValidationIsCalledOnce() {
        givenHttpHeadersWithValidToken();

        facebookAuthenticationService.authenticateWithUserId(httpHeaders, A_USER_ID);

        verify(user).validateAuthorization(A_FACEBOOK_ID);
    }

    @Test
    public void givenValidFacebookAuth_whenAuthenticatingWithUser_thenReturnsExpectedUser() {
        givenHttpHeadersWithValidToken();

        User result = facebookAuthenticationService.authenticateWithUserId(httpHeaders, A_USER_ID);

        assertThat(result, is(equalTo(user)));
    }

    @Test
    public void givenValidFacebookAuth_whenAuthenticatingWithUser_thenUserRepositoryFetchUserByFacebookIdIsNeverCalled() {
        givenHttpHeadersWithValidToken();

        facebookAuthenticationService.authenticateWithUserId(httpHeaders, A_USER_ID);

        verify(userRepository, never()).fetchUserByFacebookId(any(String.class));
    }

    @Test
    public void givenInvalidFacebookAuth_whenAuthenticatingWithUser_thenNothingElseIsCalled() throws Exception {
        givenHttpHeadersWithInvalidToken();

        try {
            facebookAuthenticationService.authenticateWithUserId(httpHeaders, A_USER_ID);
            fail("Should throw an exception.");
        }
        catch (Exception exception) {
            verify(userRepository, never()).fetchUser(any(String.class));
            verify(user, never()).validateAuthorization(any(String.class));
        }
    }

    @Test(expected = AuthenticationFailedException.class)
    public void givenInvalidFacebookAuth_whenAuthenticatingWithUser_thenAuthenticationFailedExceptionIsThrown()
        throws Exception {
        givenHttpHeadersWithInvalidToken();

        facebookAuthenticationService.authenticateWithUserId(httpHeaders, A_USER_ID);
    }

    private void givenHttpHeadersWithoutToken() {
        willReturn(false).given(httpHeaders).containsKey(HttpHeaders.AUTHORIZATION);
    }

    private void givenHttpHeadersWithValidToken() {
        willReturn(true).given(httpHeaders).containsKey(HttpHeaders.AUTHORIZATION);
        willReturn(VALID_TOKEN).given(httpHeaders).getFirst(HttpHeaders.AUTHORIZATION);
    }

    private void givenHttpHeadersWithInvalidToken() throws Exception {
        willReturn(true).given(httpHeaders).containsKey(HttpHeaders.AUTHORIZATION);
        willReturn(INVALID_TOKEN).given(httpHeaders).getFirst(HttpHeaders.AUTHORIZATION);
    }

    private void givenFacebookAuthFails() throws Exception {
        willThrow(Exception.class).given(objectMapper).readValue(any(URL.class), eq(FacebookId.class));
    }
}