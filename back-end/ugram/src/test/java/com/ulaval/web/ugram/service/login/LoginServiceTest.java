package com.ulaval.web.ugram.service.login;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.fail;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import com.ulaval.web.ugram.domain.user.User;
import com.ulaval.web.ugram.domain.user.UserRepository;
import com.ulaval.web.ugram.service.AuthenticationService;
import com.ulaval.web.ugram.service.dto.FacebookInformation;
import com.ulaval.web.ugram.service.user.assembler.UserApiAssembler;
import com.ulaval.web.ugram.service.user.dto.UserApiDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpHeaders;

@RunWith(MockitoJUnitRunner.class)
public class LoginServiceTest {

    private static final String A_FACEBOOK_ID = "A_FACEBOOK_ID";
    private static final String A_PICTURE_URL = "A_PICTURE_URL";

    @Mock
    private UserApiDto validUserApiDto;
    @Mock
    private User validUser;
    @Mock
    private HttpHeaders httpHeaders;

    @Mock
    private UserApiAssembler userApiAssembler;
    @Mock
    private UserRepository userRepository;

    @Mock
    private AuthenticationService<FacebookInformation> authenticationService;

    @InjectMocks
    private LoginService loginService;

    @Before
    public void setUpFacebookAuthService() {
        FacebookInformation facebookInformation = new FacebookInformation();
        facebookInformation.id = A_FACEBOOK_ID;
        facebookInformation.pictureUrl = A_PICTURE_URL;

        willReturn(facebookInformation).given(authenticationService).authenticate(httpHeaders);
    }

    @Test
    public void givenValidLogin_whenLogin_thenReturnsUserApiDto() {
        givenValidLogin();

        UserApiDto userApiDto = loginService.login(httpHeaders);

        assertThat(userApiDto, is(equalTo(validUserApiDto)));
    }

    @Test
    public void givenAnyLogin_whenLogin_thenAuthenticationServiceIsCalledOnce() {
        givenValidLogin();

        loginService.login(httpHeaders);

        verify(authenticationService).authenticate(httpHeaders);
    }

    @Test
    public void givenFacebookAuthFails_whenLogin_thenNothingElseIsCalled() {
        willThrow(Exception.class).given(authenticationService).authenticate(any(HttpHeaders.class));

        try {
            loginService.login(httpHeaders);
            fail("Should throw exception.");
        }
        catch (Exception exception) {
            verify(userRepository, never()).fetchUserByFacebookId(any(String.class));
            verify(userApiAssembler, never()).toDto(any(User.class));
        }
    }

    @Test(expected = Exception.class)
    public void givenFacebookAuthFails_whenLogin_thenExceptionIsThrown() {
        willThrow(Exception.class).given(authenticationService).authenticate(any(HttpHeaders.class));

        loginService.login(httpHeaders);
    }

    private void givenValidLogin() {
        willReturn(validUser).given(userRepository).fetchUserByFacebookId(A_FACEBOOK_ID);
        willReturn(validUserApiDto).given(userApiAssembler).toDto(validUser);
    }
}