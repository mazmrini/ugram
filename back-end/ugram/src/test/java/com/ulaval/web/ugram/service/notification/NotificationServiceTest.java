package com.ulaval.web.ugram.service.notification;

import com.ulaval.web.ugram.domain.notification.Notification;
import com.ulaval.web.ugram.domain.notification.NotificationEnum;
import com.ulaval.web.ugram.domain.notification.NotificationRepository;
import com.ulaval.web.ugram.infrastructure.notification.dto.NotificationsInfraDto;
import com.ulaval.web.ugram.service.AuthenticationService;
import com.ulaval.web.ugram.service.notification.assembler.NotificationAssembler;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpHeaders;

@RunWith(MockitoJUnitRunner.class)
public class NotificationServiceTest {

    private static final String USER_ID = "MA";
    private static final String FROM_USER_ID = "JA";
    private static final int PICTURE_ID = 1245;
    private static final NotificationEnum NOTIFICATION_ENUM = NotificationEnum.LIKE;
    @Mock
    private NotificationAssembler notificationAssembler;

    @Mock
    private NotificationRepository notificationRepository;

    @Mock
    private AuthenticationService authenticationService;

    @Mock
    private HttpHeaders httpHeaders;

    private NotificationService notificationService;

    @Before
    public void setUp() {
        notificationService = new NotificationService(notificationRepository, notificationAssembler,
            authenticationService);
    }

    @Test
    public void givenNotifyRequest_whenNotifyAction_thenAssembleIsCalled() {
        notificationService.notifyAction(USER_ID, FROM_USER_ID, PICTURE_ID, NOTIFICATION_ENUM);
        Mockito.verify(notificationAssembler).assemble(USER_ID, FROM_USER_ID, PICTURE_ID, NOTIFICATION_ENUM);
    }

    @Test
    public void givenNotifyRequest_whenNotifyAction_thenAddNotificationIsCalled() {
        notificationService.notifyAction(USER_ID, FROM_USER_ID, PICTURE_ID, NOTIFICATION_ENUM);
        Mockito.verify(notificationRepository).addNotification(Mockito.any(Notification.class));
    }

    @Test
    public void givenGetNotificationRequest_whenGetNotification_thenAuthenticateWithUserIdIsCalled() {
        notificationService.getNotification(httpHeaders, USER_ID);
        Mockito.verify(authenticationService).authenticateWithUserId(httpHeaders, USER_ID);
    }

    @Test
    public void givenGetNotificationRequest_whenGetNotification_thenGetNotificationsIsCalled() {
        notificationService.getNotification(httpHeaders, USER_ID);
        Mockito.verify(notificationRepository).getNotifications(USER_ID);
    }

    @Test
    public void givenGetNotificationRequest_whenGetNotification_thenDeleteNotificationsIsCalled() {
        notificationService.getNotification(httpHeaders, USER_ID);
        Mockito.verify(notificationRepository).deleteNotifications(USER_ID);
    }

    @Test
    public void givenGetNotificationRequest_whenGetNotification_thenAssembleIsCalled() {
        notificationService.getNotification(httpHeaders, USER_ID);
        Mockito.verify(notificationAssembler).assemble(Mockito.any(NotificationsInfraDto.class));
    }
}