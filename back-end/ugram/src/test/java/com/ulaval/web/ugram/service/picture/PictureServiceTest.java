package com.ulaval.web.ugram.service.picture;

import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.ulaval.web.ugram.domain.notification.NotificationEnum;
import com.ulaval.web.ugram.domain.picture.Picture;
import com.ulaval.web.ugram.domain.picture.PictureRepository;
import com.ulaval.web.ugram.domain.picture.tag.Tag;
import com.ulaval.web.ugram.domain.picture.validator.PictureValidator;
import com.ulaval.web.ugram.service.AuthenticationService;
import com.ulaval.web.ugram.service.dto.FacebookInformation;
import com.ulaval.web.ugram.service.notification.NotificationService;
import com.ulaval.web.ugram.service.picture.assembler.PictureAssembler;
import com.ulaval.web.ugram.service.picture.dto.UpdatePictureApiDto;
import com.ulaval.web.ugram.service.picture.dto.UploadPictureDto;
import com.ulaval.web.ugram.service.picture.tag.assembler.TagAssembler;
import com.ulaval.web.ugram.service.user.dto.PutCommentApiDto;
import com.ulaval.web.ugram.service.user.dto.PutLikeApiDto;
import com.ulaval.web.ugram.util.converter.FileConverter;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpHeaders;

@RunWith(MockitoJUnitRunner.class)
public class PictureServiceTest {

    private static final String USER_ID = "Alx";
    private static final int PICTURE_ID = 212;
    private static final String FROM_USER_ID = "Jean";
    private static final String COMMENT = "Comment";

    @Mock
    private PictureAssembler pictureAssembler;
    @Mock
    private PictureRepository pictureRepository;
    @Mock
    private FileConverter fileConverter;
    @Mock
    private AuthenticationService<FacebookInformation> authenticationService;
    @Mock
    private PictureValidator pictureValidator;
    @Mock
    private TagAssembler tagAssembler;
    @Mock
    private NotificationService notificationService;

    private HttpHeaders httpHeaders;
    private UploadPictureDto uploadPictureDto;
    private UpdatePictureApiDto updatePictureApiDto;
    private PutLikeApiDto putLikeApiDto;
    private PutCommentApiDto putCommentApiDto;
    private PictureService pictureService;

    @Before
    public void setUp() {
        httpHeaders = Mockito.mock(HttpHeaders.class);
        updatePictureApiDto = Mockito.mock(UpdatePictureApiDto.class);
        putLikeApiDto = new PutLikeApiDto();
        putCommentApiDto = new PutCommentApiDto();
        pictureService = new PictureService(
            pictureRepository,
            pictureAssembler,
            fileConverter,
            authenticationService,
            pictureValidator,
            tagAssembler,
            notificationService);
    }

    @Test
    public void getPicturesRequest_getPictures_getPicturesFromPictureRepositoryIsCalled() {
        pictureService.getPictures();
        verify(pictureRepository).getPictures();
    }

    @Test
    public void getPicturesRequest_getPictures_toDtoFromPictureAssemblerIsCalled() {
        List<Picture> pictures = new ArrayList<>();
        when(pictureRepository.getPictures()).thenReturn(pictures);
        pictureService.getPictures();
        verify(pictureAssembler).toDto(pictures);
    }

    @Test
    public void postPictureRequest_postPictures_authenticateWithUserIdIsCalled() {
        mockPostPictureRequest();
        pictureService.postPicture(uploadPictureDto, httpHeaders);
        verify(authenticationService).authenticateWithUserId(Mockito.any(), Mockito.any());
    }

    @Test
    public void getTagsRequest_getTags_getTagsFromPictureRepositoryIsCalled() {
        pictureService.getTags();
        verify(pictureRepository).getTags();
    }

    @Test
    public void getTagsRequest_getTags_ToDtoFromTagAssemblerIsCalled() {
        pictureService.getTags();
        verify(tagAssembler).toDto(anyListOf(Tag.class));
    }

    @Test
    public void postPictureRequest_postPictures_toDomainFromPictureAssemblerIsCalled() {
        mockPostPictureRequest();
        pictureService.postPicture(uploadPictureDto, httpHeaders);
        verify(pictureAssembler).toDomain(Mockito.any());
    }

    @Test
    public void postPictureRequest_postPictures_convertIsCalled() {
        mockPostPictureRequest();
        pictureService.postPicture(uploadPictureDto, httpHeaders);

        verify(fileConverter).convert(Mockito.any());
    }

    @Test
    public void postPictureRequest_postPictures_postPictureFromPictureRepositoryCalled() {
        mockPostPictureRequest();
        pictureService.postPicture(uploadPictureDto, httpHeaders);
        verify(pictureRepository).postPicture(Mockito.any(), Mockito.any());
    }

    @Test
    public void postPictureRequest_postPictures_toDtoFromPictureAssemblerIsCalled() {
        mockPostPictureRequest();
        pictureService.postPicture(uploadPictureDto, httpHeaders);
        verify(pictureAssembler).toDto(Mockito.anyString());
    }

    @Test
    public void getPictureByPictureIdRequest_getPictureByPictureId_getPictureByPictureIdFromPictureRepoIsCalled() {
        when(pictureRepository.getPictureByPictureId(USER_ID, PICTURE_ID))
            .thenReturn(Mockito.mock(Picture.class));
        pictureService.getPictureByPictureId(USER_ID, PICTURE_ID);
        verify(pictureRepository).getPictureByPictureId(USER_ID, PICTURE_ID);
    }

    @Test
    public void getPictureByPictureIdRequest_getPictureByPictureId_toDtoIsCalled() {
        when(pictureRepository.getPictureByPictureId(USER_ID, PICTURE_ID))
            .thenReturn(Mockito.mock(Picture.class));
        pictureService.getPictureByPictureId(USER_ID, PICTURE_ID);
        verify(pictureAssembler).toDto(Mockito.any(Picture.class));
    }

    @Test
    public void getPicturesByUserIdRequest_getPicturesByUserId_getPicturesByUserIdFromPictureRepoIsCalled() {
        List<Picture> pictures = new ArrayList<>();
        when(pictureRepository.getPicturesByUserId(USER_ID))
            .thenReturn(pictures);
        pictureService.getPicturesByUserId(USER_ID);
        verify(pictureRepository).getPicturesByUserId(USER_ID);
    }

    @Test
    public void getPicturesByUserIdRequest_getPicturesByUserId_toDtoIsCalled() {
        List<Picture> pictures = new ArrayList<>();
        when(pictureRepository.getPicturesByUserId(USER_ID))
            .thenReturn(pictures);
        pictureService.getPicturesByUserId(USER_ID);
        verify(pictureAssembler).toDto(pictures);
    }

    @Test
    public void deletePictureByPictureIdRequest_deletePictureByPictureId_authenticateWithUserIdIsCalled() {
        pictureService.deletePictureByPictureId(httpHeaders, USER_ID, PICTURE_ID);
        verify(authenticationService).authenticateWithUserId(httpHeaders, USER_ID);
    }

    @Test
    public void deletePictureByPictureIdRequest_deletePictureByPictureId_deletePictureByPictureIdIsCalled() {
        pictureService.deletePictureByPictureId(httpHeaders, USER_ID, PICTURE_ID);
        verify(pictureRepository).deletePictureByPictureId(PICTURE_ID, USER_ID);
    }

    @Test
    public void putPictureRequest_putPicture_authenticateWithUserIdIsCalled() {
        pictureService.putPicture(httpHeaders, USER_ID, PICTURE_ID, updatePictureApiDto);
        verify(authenticationService).authenticateWithUserId(httpHeaders, USER_ID);
    }

    @Test
    public void putPictureRequest_putPicture_validateUpdatePictureApiDtoIsCalled() {
        pictureService.putPicture(httpHeaders, USER_ID, PICTURE_ID, updatePictureApiDto);
        verify(pictureValidator).validateUpdatePictureApiDto(updatePictureApiDto);
    }

    @Test
    public void putPictureRequest_putPicture_toDomainIsCalled() {
        pictureService.putPicture(httpHeaders, USER_ID, PICTURE_ID, updatePictureApiDto);
        verify(pictureAssembler).toDomain(updatePictureApiDto, USER_ID, PICTURE_ID);
    }

    @Test
    public void putPictureRequest_putPicture_putPictureIsCalled() {
        pictureService.putPicture(httpHeaders, USER_ID, PICTURE_ID, updatePictureApiDto);
        verify(pictureRepository).putPicture(Mockito.any());
    }

    @Test
    public void putPictureRequest_putPicture_toDtoIsCalled() {
        pictureService.putPicture(httpHeaders, USER_ID, PICTURE_ID, updatePictureApiDto);
        verify(pictureAssembler).toDto(Mockito.any(Picture.class));
    }

    @Test
    public void putLikeRequest_putLike_validatePutLikeApiDtoIsCalled() {
        pictureService.putLike(USER_ID, putLikeApiDto, PICTURE_ID, httpHeaders);
        Mockito.verify(pictureValidator).validatePutLikeApiDto(putLikeApiDto);

    }

    @Test
    public void putLikeRequest_putLike_authenticateWithUserIdCalled() {
        putLikeApiDto.fromUserId = FROM_USER_ID;
        pictureService.putLike(USER_ID, putLikeApiDto, PICTURE_ID, httpHeaders);
        Mockito.verify(authenticationService).authenticateWithUserId(httpHeaders, FROM_USER_ID);

    }

    @Test
    public void putLikeRequest_putLike_putLikeFromPictureRepositoryIsCalled() {
        putLikeApiDto.fromUserId = FROM_USER_ID;
        pictureService.putLike(USER_ID, putLikeApiDto, PICTURE_ID, httpHeaders);
        Mockito.verify(pictureRepository).putLike(USER_ID, FROM_USER_ID, PICTURE_ID);

    }

    @Test
    public void putLikeRequest_putLike_notifyActionIsCalled() {
        putLikeApiDto.fromUserId = FROM_USER_ID;
        pictureService.putLike(USER_ID, putLikeApiDto, PICTURE_ID, httpHeaders);

        verify(notificationService).notifyAction(USER_ID, FROM_USER_ID, PICTURE_ID, NotificationEnum.LIKE);
    }

    @Test
    public void putCommentRequest_putComment_validatePutCommentApiDtoIsCalled() {
        pictureService.putComment(USER_ID, putCommentApiDto, PICTURE_ID, httpHeaders);

        verify(pictureValidator).validatePutCommentApiDto(putCommentApiDto);
    }

    @Test
    public void putCommentRequest_putComment_authenticateWithUserIdCalled() {
        putCommentApiDto.fromUserId = FROM_USER_ID;
        pictureService.putComment(USER_ID, putCommentApiDto, PICTURE_ID, httpHeaders);

        verify(authenticationService).authenticateWithUserId(httpHeaders, FROM_USER_ID);

    }

    @Test
    public void putCommentRequest_putComment_putCommentFromPictureRepositoryIsCalled() {
        putCommentApiDto.fromUserId = FROM_USER_ID;
        putCommentApiDto.comment = COMMENT;
        pictureService.putComment(USER_ID, putCommentApiDto, PICTURE_ID, httpHeaders);

        verify(pictureRepository).putComment(USER_ID, FROM_USER_ID, PICTURE_ID, COMMENT);
    }

    @Test
    public void putCommentRequest_putComment_notifyActionIsCalled() {
        putCommentApiDto.fromUserId = FROM_USER_ID;
        putCommentApiDto.comment = COMMENT;
        pictureService.putComment(USER_ID, putCommentApiDto, PICTURE_ID, httpHeaders);

        verify(notificationService).notifyAction(USER_ID, FROM_USER_ID, PICTURE_ID, NotificationEnum.COMMENT);
    }

    private void mockPostPictureRequest() {
        Picture picture = Mockito.mock(Picture.class);
        File file = Mockito.mock(File.class);
        uploadPictureDto = Mockito.mock(UploadPictureDto.class);
        when(pictureAssembler.toDomain(Mockito.any())).thenReturn(picture);
        when(fileConverter.convert(Mockito.any())).thenReturn(file);
    }
}