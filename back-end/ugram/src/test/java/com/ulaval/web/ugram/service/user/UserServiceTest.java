package com.ulaval.web.ugram.service.user;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.fail;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import com.ulaval.web.ugram.domain.user.User;
import com.ulaval.web.ugram.domain.user.UserRepository;
import com.ulaval.web.ugram.domain.user.validator.UserValidator;
import com.ulaval.web.ugram.infrastructure.exception.ItemNotFoundException;
import com.ulaval.web.ugram.service.AuthenticationService;
import com.ulaval.web.ugram.service.dto.FacebookInformation;
import com.ulaval.web.ugram.service.exception.FacebookAccountAlreadyLinkedException;
import com.ulaval.web.ugram.service.exception.UsernameAlreadyExistException;
import com.ulaval.web.ugram.service.user.assembler.UserApiAssembler;
import com.ulaval.web.ugram.service.user.dto.CreateUserApiDto;
import com.ulaval.web.ugram.service.user.dto.UpdateUserApiDto;
import com.ulaval.web.ugram.service.user.dto.UserApiDto;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpHeaders;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    private static final String A_USER_ID = "A_USER_ID";
    private static final String A_FACEBOOK_ID = "A_FACEBOOK_ID";
    private static final String A_PROFILE_PIC_URL = "A_PROFILE_PIC_URL";

    private FacebookInformation facebookInformation;
    private CreateUserApiDto createUserApiDto;

    @Mock
    private User user;
    @Mock
    private User updatedUser;
    @Mock
    private User createdUser;
    @Mock
    private UserApiDto userApiDto;
    @Mock
    private UpdateUserApiDto updateUserApiDto;
    @Mock
    private HttpHeaders httpHeaders;

    @Mock
    private UserApiAssembler userApiAssembler;
    @Mock
    private UserRepository userRepository;
    @Mock
    private AuthenticationService<FacebookInformation> authenticationService;
    @Mock
    private UserValidator userValidator;

    @InjectMocks
    private UserService userService;

    @Before
    public void setUp() {
        facebookInformation = new FacebookInformation();
        facebookInformation.id = A_FACEBOOK_ID;
        facebookInformation.pictureUrl = A_PROFILE_PIC_URL;

        createUserApiDto = new CreateUserApiDto();
        createUserApiDto.id = A_USER_ID;

        willReturn(user).given(authenticationService).authenticateWithUserId(httpHeaders, A_USER_ID);

        setUpPutUser();
        setUpCreateUser();
    }

    @Test
    public void givenValidUserId_whenGetUser_thenReturnsExpectedUserDto() {
        willReturn(user).given(userRepository).fetchUser(A_USER_ID);
        willReturn(userApiDto).given(userApiAssembler).toDto(user);

        UserApiDto result = userService.getUser(A_USER_ID);

        assertThat(result, is(equalTo(userApiDto)));
    }

    @Test
    public void givenAnyUserId_whenGetUser_thenNoAuthenticationIsDone() {
        willReturn(user).given(userRepository).fetchUser(A_USER_ID);
        willReturn(userApiDto).given(userApiAssembler).toDto(user);

        userService.getUser(A_USER_ID);

        assertAuthenticationIsNeverDone();
    }

    @Test
    public void givenValidUser_whenPutUser_thenReturnsExpectedDto() {
        UserApiDto updatedDto = userService.putUser(httpHeaders, A_USER_ID, updateUserApiDto);

        assertThat(updatedDto, is(equalTo(userApiDto)));
    }

    @Test
    public void givenUser_whenPutUser_thenAuthenticationServiceAuthenticateWithUserIdIsCalledOnce() {
        userService.putUser(httpHeaders, A_USER_ID, updateUserApiDto);

        verify(authenticationService).authenticateWithUserId(httpHeaders, A_USER_ID);
    }

    @Test
    public void givenUser_whenPutUser_thenUserValidatorValidateIsCalledOnce() {
        userService.putUser(httpHeaders, A_USER_ID, updateUserApiDto);

        verify(userValidator).validateUpdateUserApiDto(updateUserApiDto);
    }

    @Test
    public void givenUser_whenPutUser_thenUserRepositoryPutUserIsCalledOnce() {
        userService.putUser(httpHeaders, A_USER_ID, updateUserApiDto);

        verify(userRepository).putUser(updatedUser);
    }

    @Test(expected = Exception.class)
    public void givenFacebookAuthFails_whenPutUser_thenExceptionIsThrown() {
        givenFacebookAuthFails();

        userService.putUser(httpHeaders, A_USER_ID, updateUserApiDto);
    }

    @Test
    public void givenFacebookAuthFails_whenPutUser_thenUserRepositoryPutUserIsNotCalled() {
        givenFacebookAuthFails();

        try {
            userService.putUser(httpHeaders, A_USER_ID, updateUserApiDto);
            fail("Should throw exception.");
        }
        catch (Exception exception) {
            verify(userRepository, never()).putUser(any(User.class));
        }
    }

    @Test(expected = Exception.class)
    public void givenUserValidationFails_whenPutUser_thenExceptionIsThrown() {
        givenUserValidationFails();

        userService.putUser(httpHeaders, A_USER_ID, updateUserApiDto);
    }

    @Test
    public void givenUserValidationFails_whenPutUser_thenUserRepositoryPutUserIsNotCalled() {
        givenUserValidationFails();

        try {
            userService.putUser(httpHeaders, A_USER_ID, updateUserApiDto);
            fail("Should throw exception.");
        }
        catch (Exception exception) {
            verify(userRepository, never()).putUser(any(User.class));
        }
    }

    @Test
    public void givenValidRequest_whenGetUsers_thenReturnsExpectedDto() {
        List<User> users = mock(List.class);
        List<UserApiDto> expectedDto = mock(List.class);
        willReturn(users).given(userRepository).fetchUsers();
        willReturn(expectedDto).given(userApiAssembler).toDto(users);

        List<UserApiDto> result = userService.getUsers();

        assertThat(result, is(equalTo(expectedDto)));
    }

    @Test
    public void givenValidRequest_whenGetUsers_thenNoAuthenticationIsDone() {
        userService.getUsers();

        assertAuthenticationIsNeverDone();
    }

    @Test
    public void givenValidUserId_whenDeletingUser_thenAuthenticationIsCalledOnce() {
        userService.deleteUser(httpHeaders, A_USER_ID);

        verify(authenticationService).authenticateWithUserId(httpHeaders, A_USER_ID);
    }

    @Test
    public void givenValidUserId_whenDeletingUser_thenUserRepositoryDeleteUserIsCalledOnce() {
        userService.deleteUser(httpHeaders, A_USER_ID);

        verify(userRepository).deleteUser(A_USER_ID);
    }

    @Test(expected = Exception.class)
    public void givenFacebookAuthFails_whenDeletingUser_thenExceptionIsThrown() {
        givenFacebookAuthFails();

        userService.deleteUser(httpHeaders, A_USER_ID);
    }

    @Test
    public void givenInvalidFacebookAuth_whenDeletingUser_thenUserRepositoryDeleteUserIsNeverCalled() {
        givenFacebookAuthFails();

        try {
            userService.deleteUser(httpHeaders, A_USER_ID);
            fail("Should throw exception.");

        }
        catch (Exception exception) {
            verify(userRepository, never()).deleteUser(any(String.class));
        }
    }

    @Test
    public void givenValidRequest_whenCreatingUser_thenUserRepositoryCreateUserIsCalledOnce() {
        userService.createUser(httpHeaders, createUserApiDto);

        verify(userRepository).createUser(createdUser);
    }

    @Test
    public void givenValidRequest_whenCreatingUser_thenAuthenticationServiceAuthenticateIsCalledOnce() {
        userService.createUser(httpHeaders, createUserApiDto);

        verify(authenticationService).authenticate(httpHeaders);
    }

    @Test
    public void givenValidRequest_whenCreatingUser_thenUserValidatorValidateIsCalledOnce() {
        userService.createUser(httpHeaders, createUserApiDto);

        verify(userValidator).validateCreateUserApiDto(createUserApiDto);
    }

    @Test
    public void givenValidRequest_whenCreatingUser_thenUserRepositoryFetchUserIsCalledOnce() {
        userService.createUser(httpHeaders, createUserApiDto);

        verify(userRepository).fetchUser(A_USER_ID);
    }

    @Test
    public void givenValidRequest_whenCreatingUser_thenUserRepositoryFetchUserByFacebookIdIsCalledOnce() {
        userService.createUser(httpHeaders, createUserApiDto);

        verify(userRepository).fetchUserByFacebookId(A_FACEBOOK_ID);
    }

    @Test
    public void givenFacebookAuthFails_whenCreatingUser_thenUserRepositoryAndValidatorAreNeverCalled() {
        givenFacebookAuthFails();

        try {
            userService.createUser(httpHeaders, createUserApiDto);
            fail("Should throw an exception");
        }
        catch (Exception exception) {
            verify(userValidator, never()).validateCreateUserApiDto(any(CreateUserApiDto.class));
            verify(userRepository, never()).fetchUser(any(String.class));
            verify(userRepository, never()).fetchUserByFacebookId(any(String.class));
        }
    }

    @Test(expected = Exception.class)
    public void givenFacebookAuthFails_whenCreatingUser_thenExceptionIsThrown() {
        givenFacebookAuthFails();

        userService.createUser(httpHeaders, createUserApiDto);
    }

    @Test(expected = Exception.class)
    public void givenUserValidationFails_whenCreatingUser_thenExceptionIsThrown() {
        givenUserValidationFails();

        userService.createUser(httpHeaders, createUserApiDto);
    }

    @Test
    public void givenUserValidationFails_whenCreatingUser_thenUserRepositoryAreNeverCalledButAuthServiceIsCalledOnce() {
        givenUserValidationFails();

        try {
            userService.createUser(httpHeaders, createUserApiDto);
            fail("Should throw an exception");
        }
        catch (Exception exception) {
            verify(authenticationService).authenticate(httpHeaders);
            verify(userRepository, never()).fetchUser(any(String.class));
            verify(userRepository, never()).fetchUserByFacebookId(any(String.class));
        }
    }

    @Test(expected = UsernameAlreadyExistException.class)
    public void givenUsernameAlreadyExists_whenCreatingUser_thenUsernameAlreadyExistsExceptionIsThrown() {
        willReturn(user).given(userRepository).fetchUser(A_USER_ID);

        userService.createUser(httpHeaders, createUserApiDto);
    }

    @Test
    public void givenUsernameAlreadyExists_whenCreatingUser_thenUserRepositoryCreateUserIsNeverCalled() {
        willReturn(user).given(userRepository).fetchUser(A_USER_ID);

        try {
            userService.createUser(httpHeaders, createUserApiDto);
            fail("Should throw an exception");
        }
        catch (UsernameAlreadyExistException exception) {
            verify(userRepository, never()).createUser(any(User.class));
        }
    }

    @Test(expected = FacebookAccountAlreadyLinkedException.class)
    public void givenFacebookAccountAlreadyLinked_whenCreatingUser_thenFacebookAccountAlreadyLinkedExceptionIsThrown() {
        willReturn(user).given(userRepository).fetchUserByFacebookId(A_FACEBOOK_ID);

        userService.createUser(httpHeaders, createUserApiDto);
    }

    @Test
    public void givenFacebookAccountAlreadyLinked_whenCreatingUser_thenUserRepositoryCreateUserIsNeverCalled() {
        willReturn(user).given(userRepository).fetchUserByFacebookId(A_FACEBOOK_ID);

        try {
            userService.createUser(httpHeaders, createUserApiDto);
            fail("Should throw an exception");
        }
        catch (FacebookAccountAlreadyLinkedException exception) {
            verify(userRepository, never()).createUser(any(User.class));
        }
    }

    private void assertAuthenticationIsNeverDone() {
        verify(authenticationService, never()).authenticate(any(HttpHeaders.class));
        verify(authenticationService, never()).authenticateWithUserId(any(HttpHeaders.class), any(String.class));
    }

    private void givenFacebookAuthFails() {
        willThrow(Exception.class).given(authenticationService).authenticateWithUserId(any(HttpHeaders.class),
            any(String.class));
        willThrow(Exception.class).given(authenticationService).authenticate(any(HttpHeaders.class));
    }

    private void givenUserValidationFails() {
        willThrow(Exception.class).given(userValidator).validateCreateUserApiDto(any(CreateUserApiDto.class));
        willThrow(Exception.class).given(userValidator).validateUpdateUserApiDto(any(UpdateUserApiDto.class));
    }

    private void setUpPutUser() {
        willReturn(updatedUser).given(userApiAssembler).toDomain(user, updateUserApiDto);
        willReturn(userApiDto).given(userApiAssembler).toDto(updatedUser);
    }

    private void setUpCreateUser() {
        willReturn(facebookInformation).given(authenticationService).authenticate(httpHeaders);
        willReturn(createdUser).given(userApiAssembler).toDomain(createUserApiDto, facebookInformation);

        willThrow(ItemNotFoundException.class).given(userRepository).fetchUser(A_USER_ID);
        willThrow(ItemNotFoundException.class).given(userRepository).fetchUserByFacebookId(A_FACEBOOK_ID);
    }
}