import axios from "axios";
import { ADD_ACCESS_TOKEN,
         LOGIN_SUCCESSFUL,
         LOGOUT_SUCCESSFUL,
         SIGN_UP_NEEDED } from "../constants/actionTypes";
import { buildLoginUrl } from "../urlConfig";

export interface LoggedUser {
    username: string;
    profilePicUrl: string;
}

export function loginToServer(accessToken: string): any {
    return (dispatch) => {
        dispatch({ type: ADD_ACCESS_TOKEN, payload: accessToken });

        const promise = axios.post(buildLoginUrl(), "")
            .then((response) => {
                dispatch({
                    type: LOGIN_SUCCESSFUL,
                    payload: mapToLoggedUser(response.data)
                });
            });

        return promise;
    };
}

export function needSignUp(): any {
    return (dispatch) => {
        dispatch({ type: SIGN_UP_NEEDED });
    };
}

export function logout(history: any): any {
    return (dispatch) => {
        dispatch({ type: LOGOUT_SUCCESSFUL });

        history.push(buildLoginUrl());
    };
}

function mapToLoggedUser(response: any): LoggedUser {
    return {
        username: response.id,
        profilePicUrl: response.pictureUrl
    };
}
