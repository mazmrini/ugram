import axios from "axios";
import { buildNotificationsUrl } from "../urlConfig";
import { RECEIVED_NOTIFICATIONS, DELETE_NOTIFICATIONS } from "../constants/actionTypes";

export function fetchNotifications(userId: string): any {
    return (dispatch) => {
        const getUrl = buildNotificationsUrl(userId);

        const promise = axios.get(getUrl).then((response) => {
            const notifications = [];
            response.data.forEach((notification) => {
                const userName = notification.fromUser;
                const reaction = notification.reaction === "LIKE" ? "liked" : "commented";
                notifications.push( userName + " " + reaction + " your picture." );
            });

            dispatch({type: RECEIVED_NOTIFICATIONS, payload: notifications});
        });

        return promise;
    };
}
export function deleteNotifications(): any {
    return (dispatch) => {
        dispatch({type: DELETE_NOTIFICATIONS});
    };
}
