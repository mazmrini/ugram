import axios from "axios";
import { fetchUserPictures } from "./picturesActions";
import { fetchUserInfo } from "./userActions";
import { RECEIVED_PICTURE,
         DELETED_PICTURE,
         ADDED_PICTURE,
         EDITED_PICTURE } from "../constants/actionTypes";
import { buildGetPictureUrl,
         buildPostPictureUrl,
         buildPutPictureUrl,
         buildDeletePictureUrl,
         buildPutPictureLikeUrl,
         buildPutPictureCommentUrl } from "../urlConfig";

export interface PostPictureMetadata {
    userId: string;
    file: File;
    description: string;
    mentions: string[];
    tags: string[];
}

export interface EditPictureMetadata {
    description: string;
    username: string;
    pictureId: number;
    mentions: string[];
    tags: string[];
}

export interface UserComment {
    username: string;
    comment: string;
}

export interface Picture {
    id: number;
    profilePicUrl: string;
    pictureUrl: string;
    description: string;
    userName: string;
    tags: string[];
    mentions: string[];
    likes: string[];
    comments: UserComment[];
}

export function fetchPicture(userId: string, pictureId: number): any {
    return (dispatch) => {
        const getPictureUrl = buildGetPictureUrl(userId, pictureId);
        let profilePictureUrl;

        const userInfoPromise = fetchUserInfo(userId).then((response) => {
            profilePictureUrl = response.data.pictureUrl;
        });

        const picturePromise = userInfoPromise.then(() => {
            axios.get(getPictureUrl)
                .then((response) => {
                    const data = mapPicture(response.data, profilePictureUrl);
                    dispatch({type: RECEIVED_PICTURE, payload: data});
                });
        });

        return picturePromise;
    };
}

export function postPicture(metadata: PostPictureMetadata): any {
    return (dispatch, getState) => {
        const url = buildPostPictureUrl(metadata.userId);
        const headers = createPostPictureHeaders(getState().login.token);

        const body = createForm({
            file: metadata.file,
            description: metadata.description,
            mentions: metadata.mentions,
            tags: metadata.tags
        });

        const promise = axios.post(url, body, headers)
                            .then(() => {
                                dispatch({type: ADDED_PICTURE});
                            })
                            .then(() => {
                                dispatch(fetchUserPictures(metadata.userId));
                            });

        return promise;
    };
}

export function putPicture(picture: EditPictureMetadata): any {
    return (dispatch, getState) => {
        const url = buildPutPictureUrl(picture.username, picture.pictureId);

        const body = JSON.stringify({
            description: picture.description,
            mentions: picture.mentions,
            tags: picture.tags,
        });

        const promise = axios.put(url, body)
                            .then(() => {
                                dispatch({type: EDITED_PICTURE});
                            });

        return promise;
    };
}

export function deletePicture(userId: string, pictureId: number): any {
    return (dispatch, getState) => {
        const url = buildDeletePictureUrl(userId, pictureId);

        const promise = axios.delete(url)
                            .then((response) => {
                                dispatch({type: DELETED_PICTURE, payload: pictureId});
                            });

        return promise;
    };
}

export function likePicture(pictureUserId: string, pictureId: number): any {
    return (dispatch, getState) => {
        const url = buildPutPictureLikeUrl(pictureUserId, pictureId);

        const body = { fromUserId: getState().login.loggedUser.username };

        return axios.put(url, JSON.stringify(body));
    };
}

export function putComment(pictureUserId: string, pictureId: number, userComment: string): any {
    return (dispatch, getState) => {
        const url = buildPutPictureCommentUrl(pictureUserId, pictureId);

        const body = {
            fromUserId: getState().login.loggedUser.username,
            comment: userComment
        };

        return axios.put(url, JSON.stringify(body));
    };
}

export function mapPicture(picture: any, userPictureUrl: string = ""): Picture {
    const mappedComments = picture.comments.map((comment) => mapUserComments(comment));

    return {
        id: picture.id,
        profilePicUrl: userPictureUrl,
        pictureUrl: picture.url,
        description: picture.description,
        userName: picture.userId,
        tags: picture.tags,
        mentions: picture.mentions,
        likes: picture.likes,
        comments: mappedComments
    };
}

function mapUserComments(userComment: any): UserComment {
    return {
        username: userComment.fromUserId,
        comment: userComment.comment
    };
}

function createPostPictureHeaders(token: string): object {
    return {
        headers: {
            "Content-Type": "multipart/form-data",
            "Authorization": "Bearer " + token
        }
    };
}

interface FormMetadata {
    file: File;
    description: string;
    mentions: string[];
    tags: string[];
}

function createForm(metadata: FormMetadata): FormData {
    const form = new FormData();

    form.append("pictureModel", "description");
    form.append("pictureModel", "mentions");
    form.append("pictureModel", "tags");

    form.append("description", metadata.description);

    for (const mention of metadata.mentions)
        form.append("mentions", mention);

    for (const tag of metadata.tags)
        form.append("tags", tag);

    form.append("file", metadata.file);

    return form;
}
