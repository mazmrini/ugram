import axios from "axios";
import { USER_PICTURES_RECEIVED, RECEIVED_ALL_PICTURES, TAGS_RECEIVED } from "../constants/actionTypes";
import { UserProfilePic } from "./usersActions";
import { mapPicture, Picture } from "./pictureActions";
import { buildGetUsersUrl,
         buildGetPicturesUrl,
         buildGetUserPicturesUrl,
         buildGetPicturesTrendingTagsUrl } from "../urlConfig";

export function fetchAllPictures(): any {
    return (dispatch) => {
        const getUsersUrl = buildGetUsersUrl();
        const getPicturesUrl = buildGetPicturesUrl();

        let usersToProfilePicturesUrl = {};

        const promise = axios.get(getUsersUrl)
                            .then((response) => {
                                usersToProfilePicturesUrl = mapToUserToProfilePictureUrl(response.data.items);
                            })
                            .then(() => {
                                axios.get(getPicturesUrl)
                                    .then((response) => {
                                        const pictures = mapPictures(response.data.items, usersToProfilePicturesUrl);

                                        dispatch({type: RECEIVED_ALL_PICTURES, payload: pictures});
                                    });
                            });

        return promise;
    };
}

export function fetchTrendingTags(): any {
    return (dispatch) => {
        const getUrl = buildGetPicturesTrendingTagsUrl();

        const promise = axios.get(getUrl)
                            .then((response) => {
                                const tags = [];
                                response.data.forEach((tag) => {
                                    tags.push(tag.tag);
                                });
                                dispatch({type: TAGS_RECEIVED, payload: tags});
                            });

        return promise;
    };
}

export function fetchUserPictures(userId: string): any {
    return (dispatch) => {
        const url = buildGetUserPicturesUrl(userId);

        const promise = axios.get(url)
            .then((response) => {
                const pictures = response.data.items;

                pictures.sort((a, b) => {
                    return b.createdDate - a.createdDate;
                });

                const data = response.data.items.map((value) => mapPicture(value));

                dispatch({type: USER_PICTURES_RECEIVED, payload: data});
            });

        return promise;
    };
}

interface UserToProfilePictureUrl {
    [userId: string]: string;
}

function mapToUserToProfilePictureUrl(users: UserProfilePic[]): UserToProfilePictureUrl {
    const usersProfilePicturesUrl = {};

    users.forEach((user) => {
        usersProfilePicturesUrl[user.id] = user.pictureUrl;
    });

    return usersProfilePicturesUrl;
}

function mapPictures(picturesDto: any[], usersProfilePicturesUrl: UserToProfilePictureUrl): Picture[] {
    picturesDto.sort((a, b) => {
        return b.createdDate - a.createdDate;
    });

    const pictures = picturesDto.map((picture) => {
        const userPictureUrl = usersProfilePicturesUrl[picture.userId];

        return mapPicture(picture, userPictureUrl);
    });

    return pictures;
}
