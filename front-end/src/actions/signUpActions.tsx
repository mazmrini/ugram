import axios from "axios";
import { SIGN_UP_SUCCESSFUL } from "../constants/actionTypes";
import { UserMetadata } from "./userActions";
import { buildSignUpUrl } from "../urlConfig";

interface PostUser {
    id: string;
    firstName: string;
    lastName: string;
    email: string;
    phoneNumber: number;
}

export function signUp(user: UserMetadata): any {
    return (dispatch, getState) => {
        const postUserData = mapToPostUserData(user);

        const promise = axios.post(buildSignUpUrl(), postUserData)
            .then((response) => {
                dispatch({ type: SIGN_UP_SUCCESSFUL });
            });

        return promise;
    };
}

function mapToPostUserData(user: UserMetadata): PostUser {
    return {
        id: user.username,
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
        phoneNumber: user.phoneNumber
    };
}
