import axios from "axios";
import { USER_FETCHED, USER_EDITED, USER_DELETED } from "../constants/actionTypes";
import { InputFormatter } from "../utils/inputFormatter";
import { buildGetUserUrl, buildPutUserUrl, buildDeleteUserUrl } from "../urlConfig";

export function fetchUser(userId: string): any {
    return (dispatch) => {
        const promise = fetchUserInfo(userId)
            .then((response) => {
                dispatch({type: USER_FETCHED, payload: formatReceivedData(response.data)});
            });

        return promise;
    };
}

export function fetchUserInfo(userId: string): Promise<any> {
    const url = buildGetUserUrl(userId);

    return axios.get(url);
}

export interface UserMetadata {
    username: string;
    firstName: string;
    lastName: string;
    email: string;
    phoneNumber: number;
}

export function putUser(metadata: UserMetadata): any {
    return (dispatch, getState) => {
        const url = buildPutUserUrl(metadata.username);

        const body = JSON.stringify({
            firstName: metadata.firstName,
            lastName: metadata.lastName,
            phoneNumber: metadata.phoneNumber,
            email: metadata.email
        });

        const promise = axios.put(url, body)
                            .then((response) => {
                                const fetchedUser = formatReceivedData(response.data);
                                dispatch({type: USER_EDITED, payload: fetchedUser});
                            });

        return promise;
    };
}

export function deleteUser(userId: string): any {
    return (dispatch, getState) => {
        const url = buildDeleteUserUrl(userId);

        const promise = axios.delete(url)
            .then((response) => dispatch({ type: USER_DELETED }));

        return promise;
    };
}

function formatReceivedData(data: any): object {
    data.pictureUrl = InputFormatter.formatUrlPictureToGetLargePicture(data.pictureUrl);
    data.registrationDate = InputFormatter.formatDateToMMDDYYYY(data.registrationDate);

    return data;
}
