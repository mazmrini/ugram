import axios from "axios";
import { RECEIVED_ALL_USERS } from "../constants/actionTypes";
import { buildGetUsersUrl } from "../urlConfig";

export interface UserProfilePic {
    id: string;
    pictureUrl: string;
}

function receivedAllUsers(users: any): any {
    const userList: UserProfilePic[] = [];
    const userMap = {};

    users.forEach((user: any) => {
        userList.push(mapUser(user));
        userMap[user.id] = user.pictureUrl;
    });

    return {
        type: RECEIVED_ALL_USERS,
        payload: {
            users: userList,
            userToProfilePicture: userMap
        }
    };
}

export function fetchAllUsers(): any {
    return (dispatch) => {
        const url = buildGetUsersUrl();

        const promise = axios.get(url)
                             .then((response) => {
                                 dispatch(receivedAllUsers(response.data.items));
                             });

        return promise;
    };
}

function mapUser(user: any): UserProfilePic {
    if (!user.pictureUrl)
        user.pictureUrl = "https://graph.facebook.com/v2.5/101795610643340/picture";

    return {
        id: user.id,
        pictureUrl: user.pictureUrl
    };
}
