import * as React from "react";
import { connect } from "react-redux";
import appHistory from "./history";
import { Router, Switch, Route, Redirect } from "react-router-dom";
import { ACCESS_TOKEN_KEY } from "./constants/constants";
import { loginToServer } from "./actions/loginActions";
import { NavBar } from "./components/navbar/navBar";
import { LoginPage } from "./components/loginPage";
import { SignUpPage } from "./components/signUpPage";
import { HomePage } from "./components/homePage";
import { UserListPage } from "./components/userListPage";
import { PicturePage } from "./components/picturePage";
import { User } from "./components/user/user";
import { EditUser } from "./components/editUser";
import { EditPicture } from "./components/editPicture";
import { SearchResultPage } from "./components/search/searchResultPage";

interface DispatchProps {
    tryLogin: (token: string) => any;
}

interface StateProps {
    isLogged: boolean;
    isSignUpNeeded: boolean;
}

interface AppState {
    shouldRender: boolean;
}

type AppProps = StateProps & DispatchProps;

class AppComponent extends React.Component<AppProps, AppState> {
    public constructor(props: any) {
        super(props);

        this.state = {
            shouldRender: false
         };
    }

    public componentDidMount(): void {
        const token = localStorage.getItem(ACCESS_TOKEN_KEY);

        if (!!token)
            this.props.tryLogin(token)
                .then(() => this.activateShouldRender())
                .catch(() => this.activateShouldRender());
        else
            this.activateShouldRender();
    }

    public render(): JSX.Element {
        if (!this.state.shouldRender)
            return <div />;

        return (
            <Router history={appHistory}>
                <div className="full-height">
                    <NavBar history={appHistory} />
                    {this.addRoutes()}
                </div>
            </Router>
        );
    }

    private activateShouldRender(): void {
        this.setState({ shouldRender: true });
    }

    private addRoutes(): JSX.Element {
        return (
            <Switch>
                {this.getHomeRoute()}
                {this.addLoginRouteIfNeeded()}
                {this.addSignUpRouteIfNeeded()}
                {this.addPrivateRoute("/users", UserListPage)}
                {this.addPrivateRoute("/users/:userId", User)}
                {this.addPrivateRoute("/users/:userId/edit", EditUser)}
                {this.addPrivateRoute("/users/:userId/pictures/:pictureId", PicturePage)}
                {this.addPrivateRoute("/users/:userId/pictures/:pictureId/edit", EditPicture)}
                {this.addPrivateRoute("/search", SearchResultPage)}
                <Redirect to="/" />
            </Switch>
        );
    }

    private addLoginRouteIfNeeded(): JSX.Element {
        if (this.props.isLogged)
            return this.getHomeRoute();

        return <Route exact path="/login" component={LoginPage} />;
    }

    private addSignUpRouteIfNeeded(): JSX.Element {
        if (!this.props.isSignUpNeeded)
            return this.getHomeRoute();

        return <Route exact path="/sign-up" component={SignUpPage} />;
    }

    private addPrivateRoute(routePath: string, component: React.ComponentClass): JSX.Element {
        if (!this.props.isLogged)
            return this.getHomeRoute();

        return <Route exact path={routePath} component={component} />;
    }

    private getHomeRoute(): JSX.Element {
        return <Route exact path="/" component={HomePage} />;
    }
}

function mapDispatchToProps(dispatch: any): DispatchProps {
    return {
        tryLogin: (token: string) => dispatch(loginToServer(token))
    };
}

function mapStateToProps(state: any, ownProps: any): StateProps {
    return {
        isLogged: state.login.isLogged,
        isSignUpNeeded: state.signUp.isSignUpNeeded
    };
}

export const App = connect<StateProps, DispatchProps>(mapStateToProps, mapDispatchToProps) (AppComponent);

export function buildHomeUrl(): string {
    return "/";
}

export function buildLoginPageUrl(): string {
    return "/login";
}

export function buildSignUpPageUrl(): string {
    return "/sign-up";
}

export function buildAllUsersUrl(): string {
    return "/users";
}

export function buildUserProfileUrl(userId: string): string {
    return "/users/" + userId;
}

export function buildEditUserUrl(userId: string): string {
    return buildUserProfileUrl(userId) + "/edit";
}

export function buildPictureUrl(userId: string, pictureId: number): string {
    return "/users/" + userId + "/pictures/" + pictureId;
}

export function buildEditPictureUrl(userId: string, pictureId: number): string {
    return buildPictureUrl(userId, pictureId) + "/edit";
}

export function buildSearchPageUrl(): string {
    return "/search";
}
