import * as React from "react";
import { connect } from "react-redux";
import { EditPictureMetadata, putPicture, fetchPicture } from "../actions/pictureActions";
import { toast, ToastContainer } from "react-toastify";
import Chips from "react-chips";
import { mentionAndTagPattern } from "../constants/regexPatterns";
import { buildUserProfileUrl, buildPictureUrl, buildHomeUrl } from "../app";

interface StateProps {
    history: any;
    loggedUsername: string;
    id: number;
    pictureUrl: string;
    description: string;
    userName: string;
    tags: string[];
    mentions: string[];
}

interface DispatchProps {
    fetchPicture: (userId: string, pictureId: number) => any;
    editPicture: (picture: EditPictureMetadata) => any;
}

type EditPictureProps = StateProps & DispatchProps;

class EditPictureComponent extends React.Component<EditPictureProps, EditPictureProps> {
    private readonly TOAST_MESSAGE: string = "Failed to update, try later!";

    constructor(props: any) {
        super(props);
        this.state = this.props;
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    public componentDidMount(): void {
        if (this.shouldRender())
            this.props.fetchPicture(this.props.userName, this.props.id)
                .catch(() => this.redirectToHome());
        else
            this.redirectUser();
    }

    public render(): JSX.Element {
        if (!this.shouldRender())
            return <div />;

        return (
            <div className="row">
                <form className="col s10 offset-s1" onSubmit={this.handleSubmit}>
                    <div className="col s6 offset-s3">
                        <h4 className="center-align blue-grey-text edit-user title">Edit photo</h4>
                    </div>
                    <div className="row">
                        {this.renderDescriptionInput()}
                        {this.renderMentionsInput()}
                        {this.renderTagsInput()}
                        {this.renderSubmitButton()}
                    </div>
                </form>
                <div>
                    <ToastContainer className="bottom-center" autoClose={5000} hideProgressBar={true}/>
                </div>
            </div>
        );
    }

    private shouldRender(): boolean {
        return this.props.userName === this.props.loggedUsername;
    }

    private handleSubmit(event: any): void {
        event.preventDefault();
        this.editPicture();
    }

    private editPicture(): void {
        this.state.editPicture({
            username: this.state.userName,
            pictureId: this.state.id,
            description: this.state.description,
            mentions: this.state.mentions,
            tags: this.state.tags
        })
        .then(() => {
            this.state.history.push(buildUserProfileUrl(this.state.userName));
        })
        .catch(() => {
            this.notifyOnFail();
        });
    }

    private handleChange(event: any): void {
        this.setState({[event.target.name]: event.target.value});
    }

    private onChangeMentions = (mentions: any) => {
        this.setState({mentions});
    }

    private onChangeTags = (tags: any) => {
        this.setState({tags});
    }

    private notifyOnFail(): void {
        toast(this.TOAST_MESSAGE, {
            position: toast.POSITION.BOTTOM_CENTER,
            className: "edit-user toaster",
        });
    }

    private renderDescriptionInput(): JSX.Element {
        return this.renderInput(this.state.description, "description", "Description", "text",
            "Please enter a valid description");
    }

    private renderMentionsInput(): JSX.Element {
        return this.renderChipsInput(this.state.mentions, "mentions", "Mentions", "text",
            "Please enter a valid mention", this.onChangeMentions);
    }

    private renderTagsInput(): JSX.Element {
        return this.renderChipsInput(this.state.tags, "tags", "Tags", "text",
            "Please enter a valid tag", this.onChangeTags);
    }

    private renderInput(defaultValue: string, name: string,
                        label: string, type: string, dataError: string): JSX.Element {
        return (
            <div className="input-field col s6 offset-s3 ">
                <input type={type} className="validate" defaultValue={defaultValue}
                       name={name}
                       onChange={this.handleChange}/>
                <label className="active">{label}</label>
            </div>
        );
    }

    private renderChipsInput(defaultValue: string[], name: string,
                             label: string, type: string, dataError: string, onChangeHandle: any): JSX.Element {
        return (
            <div className="input-field col s6 offset-s3 ">
                <Chips type={type} className="validate" value={defaultValue}
                       name={name} onChange={onChangeHandle} pattern={mentionAndTagPattern}/>
                <label className="active" data-error={dataError}>{label}</label>
            </div>
        );
    }

    private renderSubmitButton(): JSX.Element {
        return (
            <div className="col s2 offset-s3 edit-user submit-btn">
                <button className="submit-btn btn waves-effect waves-light blue-grey">Submit
                    <i className="material-icons right">send</i>
                </button>
            </div>
        );
    }

    private redirectUser(): void {
        const url = buildPictureUrl(this.props.userName, this.props.id);

        this.props.history.push(url);
    }

    private redirectToHome(): void {
        this.props.history.push(buildHomeUrl());
    }
}

function mapDispatchToProps(dispatch: any): DispatchProps {
    return {
        fetchPicture: (userId: string, pictureId: number) => dispatch(fetchPicture(userId, pictureId)),
        editPicture: (picture: EditPictureMetadata) => dispatch(putPicture(picture))
    };
}

function mapStateToProps(state: any, ownProps: any): StateProps {
    const data = state.picture.picture;

    return {
        history: ownProps.history,
        loggedUsername: state.login.loggedUser.username,
        id: ownProps.match.params.pictureId,
        pictureUrl: data.pictureUrl,
        description: data.description,
        userName: ownProps.match.params.userId,
        tags: data.tags,
        mentions: data.mentions
    };
}

export const EditPicture =
    connect<StateProps, DispatchProps>(mapStateToProps, mapDispatchToProps)(EditPictureComponent);
