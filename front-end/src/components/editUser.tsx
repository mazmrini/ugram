import * as React from "react";
import { buildUserProfileUrl, buildHomeUrl } from "../app";
import { putUser, fetchUser, UserMetadata } from "../actions/userActions";
import { connect } from "react-redux";
import { toast, ToastContainer } from "react-toastify";
import { UserForm } from "./user/userForm";

interface DispatchProps {
    fetchUser: (username: string) => any;
    editUser: (metadata: UserMetadata) => any;
}

type StateProps = UserMetadata & {
    loggedUsername: string;
    history: any;
};

type EditUserProps = StateProps & DispatchProps;

class EditUserComponent extends React.Component<EditUserProps, EditUserProps> {
    private readonly TOAST_MESSAGE: string = "Failed to save, try later!";

    constructor(props: any) {
        super(props);
        this.state = this.props;
    }

    public componentDidMount(): void {
        if (this.shouldRender())
            this.props.fetchUser(this.props.username).catch(() => this.redirectToHome());
        else
            this.redirectUser();
    }

    public render(): JSX.Element {
        if (!this.shouldRender())
            return <div />;

        return (
            <div>
                <UserForm
                    title="Edit user"
                    buttonText="Save"
                    username={this.props.username}
                    shouldEditUsername={false}
                    firstName={this.props.firstName}
                    lastName={this.props.lastName}
                    phoneNumber={this.props.phoneNumber.toString()}
                    email={this.props.email}
                    onSubmit={(metadata) => this.editUser(metadata)} />
                <div>
                    <ToastContainer className="bottom-center" autoClose={5000} hideProgressBar={true}/>
                </div>
            </div>
        );
    }

    private shouldRender(): boolean {
        return this.props.username === this.props.loggedUsername;
    }

    private redirectUser(): void {
        const url = buildUserProfileUrl(this.props.loggedUsername);

        this.props.history.push(url);
    }

    private editUser(userMetadata: UserMetadata): void {
        this.props.editUser(userMetadata)
            .then(() => this.state.history.push(buildUserProfileUrl(this.state.username)))
            .catch(() => this.notifyOnFail());
    }

    private notifyOnFail(): void {
        toast(this.TOAST_MESSAGE, {
            position: toast.POSITION.BOTTOM_CENTER,
            className: "edit-user toaster",
        });
    }

    private redirectToHome(): void {
        this.props.history.push(buildHomeUrl());
    }
}

function mapDispatchToProps(dispatch: any): DispatchProps {
    return {
        fetchUser: (username: string) => dispatch(fetchUser(username)),
        editUser: (userMetadata: UserMetadata) => dispatch(putUser(userMetadata))
    };
}

function mapStateToProps(state: any, ownProps: any): StateProps {
    const data = state.user.user;

    return {
        loggedUsername: state.login.loggedUser.username,
        history: ownProps.history,
        username: data.id,
        firstName: data.firstName,
        lastName: data.lastName,
        email: data.email,
        phoneNumber: data.phoneNumber,
    };
}

export const EditUser = connect<StateProps, DispatchProps>(mapStateToProps, mapDispatchToProps)(EditUserComponent);
