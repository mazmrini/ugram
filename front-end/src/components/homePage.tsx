import * as React from "react";
import { connect } from "react-redux";
import { fetchAllPictures } from "../actions/picturesActions";
import { Picture } from "../actions/pictureActions";
import { PictureCard } from "./picture/pictureCard";
import { buildLoginPageUrl } from "../app";

interface StateProps {
    isLogged: boolean;
    pictures: Picture[];
    history: any;
}

interface DispatchProps {
    fetchAllPictures: () => void;
}

type HomePageProps = StateProps & DispatchProps;

class HomePageComponent extends React.Component<HomePageProps> {

    public componentDidMount(): any {
        if (this.props.isLogged)
            this.props.fetchAllPictures();
        else
            this.props.history.push(buildLoginPageUrl());
    }

    public render(): JSX.Element {
        if (!this.props.isLogged)
            return <div />;

        return (
            <div className="container">
                {this.renderPictures()}
            </div>
        );
    }

    private renderPictures(): JSX.Element[] {
        return this.props.pictures.map((value) => this.mapPictureToJSX(value));
    }

    private mapPictureToJSX(pic: Picture): JSX.Element {
        return (
            <PictureCard classe="col m6 offset-m3 s10 offset-s1"
                key={pic.id}
                id={pic.id}
                pictureUrl={pic.pictureUrl}
                description={pic.description}
                userName={pic.userName}
                profilePicUrl={pic.profilePicUrl}
                tags={pic.tags}
                mentions={pic.mentions}
                likes={pic.likes}
                comments={pic.comments}
                shouldDisplayEditButtons={false}
                history={this.props.history} />
        );
    }
}

function mapDispatchToProps(dispatch: any): DispatchProps {
    return {
        fetchAllPictures: () => dispatch(fetchAllPictures())
    };
}

function mapStateToProps(state: any, ownProps: any): StateProps {
    return {
        isLogged: state.login.isLogged,
        pictures: state.pictures.pictures,
        history: ownProps.history
    };
}

export const HomePage = connect<StateProps, DispatchProps>(mapStateToProps, mapDispatchToProps)(HomePageComponent);
