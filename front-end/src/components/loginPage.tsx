import * as React from "react";
import { connect } from "react-redux";
import FacebookLogin from "react-facebook-login";
import { loginToServer, needSignUp } from "../actions/loginActions";
import { buildHomeUrl, buildSignUpPageUrl } from "../app";
import { APP_ID } from "../constants/constants";

interface DispatchProps {
    login: (token: string) => any;
    needSignUp: () => any;
}

interface StateProps {
    isLogged: boolean;
    history: any;
    logoUrl: string;
    homeLogoUrl: string;
}

type LoginPageProps = StateProps & DispatchProps;

class LoginPageComponent extends React.Component<LoginPageProps> {
    public componentDidMount(): void {
        if (this.props.isLogged)
            this.props.history.push(buildHomeUrl());
    }

    public render(): JSX.Element {
        if (this.props.isLogged)
            return <div />;

        return (
            <div className="container full-height valign-wrapper center-align">
                {this.renderLoginCard()}
            </div>
        );
    }

    private renderLoginCard(): JSX.Element {
        return(
            <div className="card margin-center" id="login-card">
                <div className="row">
                    <span className="card-title">
                        <img src={this.props.logoUrl} />
                    </span>
                </div>
                <img id="home-logo" src={this.props.homeLogoUrl} />
                <div className="card-action">
                    {this.renderFbLoginButton()}
                </div>
            </div>
        );
    }

    private renderFbLoginButton(): JSX.Element {
        return (
            <div>
                <FacebookLogin
                    appId={APP_ID}
                    autoload={true}
                    fields="name,email,picture"
                    icon="small fa-facebook"
                    callback={(response) => this.loginCallback(response)}
                />
            </div>
        );
    }

    private loginCallback(response: any): void {
        if (response.accessToken)
            this.loginToServer(response.accessToken);
        else
            this.loginFailed();
    }

    private loginToServer(token: string): void {
        this.props.login(token)
            .then(() => this.redirectTo(buildHomeUrl()))
            .catch((error) => {
                const status = error.response.status;

                if (status === 400)
                    this.goToSignUp();
                else
                    this.loginFailed();
            });
    }

    private goToSignUp(): void {
        this.props.needSignUp();

        this.redirectTo(buildSignUpPageUrl());
    }

    private redirectTo(url: string): void {
        this.props.history.push(url);
    }

    private loginFailed(): void {
        (window as any).Materialize.toast("Login failed", 5000);
    }
}

function mapDispatchToProps(dispatch: any): DispatchProps {
    return {
        login: (token: string) => dispatch(loginToServer(token)),
        needSignUp: () => dispatch(needSignUp())
    };
}

function mapStateToProps(state: any, ownProps: any): StateProps {
    return {
        isLogged: state.login.isLogged,
        history: ownProps.history,
        logoUrl: state.login.logoUrl,
        homeLogoUrl: state.login.homeLogoUrl
    };
}

export const LoginPage = connect<StateProps, DispatchProps>(mapStateToProps, mapDispatchToProps) (LoginPageComponent);
