import * as React from "react";
import { createPortal } from "react-dom";

const modalRootElement = document.getElementById("modal-root");

export class Modal extends React.Component {

    public el: HTMLDivElement;

    constructor(props: any) {
        super(props);
        this.el = document.createElement("div");
    }

    public componentDidMount(): void {
        modalRootElement.appendChild(this.el);
    }

    public componentWillUnmount(): void {
        modalRootElement.removeChild(this.el);
    }

    public render(): JSX.Element {
        return createPortal(
            this.props.children,
            this.el
        );
    }
}
