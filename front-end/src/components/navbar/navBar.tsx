import * as React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { ProfileChip } from "../profileChip";
import { buildAllUsersUrl, buildSearchPageUrl } from "../../app";
import { LoggedUser, logout } from "../../actions/loginActions";
import { fetchNotifications, deleteNotifications } from "../../actions/notificationsAction";

interface OwnProps {
    history: any;
}

interface DispatchProps {
    logout: (history: any) => any;
    fetchNotifications: (userId: string) => any;
    deleteNotifications: () => any;
}

interface StateProps {
    pathname: string;
    isLogged: boolean;
    logoUrl: string;
    loggedUser: LoggedUser;
    notifications: string[];
}

type NavBarProps = DispatchProps & StateProps & OwnProps;

class NavBarComponent extends React.Component<NavBarProps> {
    public componentWillReceiveProps(nextProps: any): void {
        if (this.props.pathname !== nextProps.pathname && nextProps.isLogged)
            this.props.fetchNotifications(this.props.loggedUser.username);
    }

    public render(): JSX.Element {
        return (
            <div className="navbar-fixed">
                <nav>
                    <div className="nav-wrapper grey lighten-4">
                        {this.renderNavContent()}
                    </div>
                </nav>
            </div>
        );
    }

    private renderNavContent(): JSX.Element {
        return (
            <div className="container full-height">
                {this.renderLogo()}
                {this.renderNavOptions()}
            </div>
        );
    }

    private renderLogo(): JSX.Element {
        return (
            <Link to="/" className="brand-logo full-height ugram-logo">
                <img id="nav-bar-logo" src={this.props.logoUrl}/>
            </Link>
        );
    }

    private renderNavOptions(): JSX.Element {
        if (!this.props.isLogged)
            return;

        return (
            <div className="right full-height valign-wrapper">
                {this.renderNotificationsButton()}
                {this.renderSearchButton()}
                {this.renderListPeopleIcon()}
                <ProfileChip
                    profilePicUrl={this.props.loggedUser.profilePicUrl}
                    userName={this.props.loggedUser.username} />
                {this.renderDisconnect()}
            </div>
        );
    }

    private renderSearchButton(): JSX.Element {
        return (
            <Link to={buildSearchPageUrl()}>
                <i className="large material-icons">search</i>
            </Link>
        );
    }

    private renderNotificationsButton(): JSX.Element {
        return (
            <div className="dropdown-notifications">
                <i className="large material-icons"  onMouseOut={() => this.handleOnMouseOut()}>
                    {this.props.notifications.length > 0 ? "notifications_active" : "notifications"}
                </i>
                {this.renderAllNotifications()}
            </div>
        );
    }

    private renderAllNotifications(): JSX.Element {
        if (this.props.notifications.length === 0)
            return <div />;

        return (
            <ul className="collection dropdown-content">
                {this.renderNotifiactions()}
            </ul>
        );
    }

    private handleOnMouseOut(): void {
        if (this.props.notifications.length > 0)
            this.props.deleteNotifications();
    }

    private renderNotifiactions(): JSX.Element[] {
        return this.props.notifications.map((value, index) => this.mapNotificationToJSX(value, index));
    }

    private mapNotificationToJSX(notification: string, index: number): JSX.Element {
        return (
            <li key={index} className="collection-item">{notification}</li>
        );
    }

    private renderDisconnect(): JSX.Element {
        return (
            <i className="material-icons dropdown-button" onClick={() => this.onDisconnectClick()}>
                power_settings_new
            </i>
        );
    }

    private onDisconnectClick(): void {
        this.props.logout(this.props.history);
    }

    private renderListPeopleIcon(): JSX.Element {
        return (
            <Link to={buildAllUsersUrl()}>
                <i className="large material-icons">group</i>
            </Link>
        );
    }
}

function mapDispatchToProps(dispatch: any): DispatchProps {
    return {
        logout: (history: any) => dispatch(logout(history)),
        fetchNotifications: (userId: string) => dispatch(fetchNotifications(userId)),
        deleteNotifications: () => dispatch(deleteNotifications())

    };
}

function mapStateToProps(state: any, ownProps: OwnProps): StateProps {
    return {
        pathname: ownProps.history.location.pathname,
        isLogged: state.login.isLogged,
        logoUrl: state.login.logoUrl,
        loggedUser: state.login.loggedUser,
        notifications: state.notifications.notifications
    };
}

export const NavBar =
    connect<StateProps, DispatchProps, OwnProps>(mapStateToProps, mapDispatchToProps)(NavBarComponent);
