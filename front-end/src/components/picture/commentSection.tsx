import * as React from "react";
import { connect } from "react-redux";
import { UserComment, putComment } from "../../actions/pictureActions";

interface OwnProps {
    pictureUserId: string;
    pictureId: number;
    comments: UserComment[];
}

interface DispatchProps {
    addComment: (pictureUserId: string, pictureId: number, userComment: string) => any;
}

interface StateProps {
    loggedUsername: string;
}

interface CommentSectionState {
    comments: UserComment[];
    currentComment: string;
}

type CommentSectionProps = OwnProps & DispatchProps & StateProps;

class CommentSectionComponent extends React.Component<CommentSectionProps, CommentSectionState> {
    public constructor(props: any) {
        super(props);

        this.state = {
            comments: this.props.comments,
            currentComment: ""
        };

        this.handleCommentChange = this.handleCommentChange.bind(this);
        this.onCommentKeyPress = this.onCommentKeyPress.bind(this);
    }

    public render(): JSX.Element {
        return (
            <div className="picture-comment">
                <ul className="collection">
                    {this.renderComments()}
                    {this.renderAddCommentSection()}
                </ul>
            </div>
        );
    }

    private renderAddCommentSection(): JSX.Element {
        return (
            <li key="comment-section" className="collection-item">
                <input
                    className="validate comment-input"
                    placeholder="Commentaire"
                    type="text"
                    onChange={this.handleCommentChange}
                    onKeyPress={this.onCommentKeyPress}
                    value={this.state.currentComment} />
            </li>
        );
    }

    private handleCommentChange(event: any): void {
        this.setState({ currentComment: event.target.value });
    }

    private onCommentKeyPress(event: any): void {
        if (event.key === "Enter") {
            event.preventDefault();
            this.tryAddComment();
        }
    }

    private tryAddComment(): void {
        if (this.hasNewComment())
            this.addComment();
    }

    private hasNewComment(): boolean {
        return !!this.state.currentComment;
    }

    private addComment(): void {
        this.props.addComment(this.props.pictureUserId, this.props.pictureId, this.state.currentComment)
                .then(() => {
                    this.setState({
                        currentComment: "",
                        comments: this.state.comments.concat({
                            comment: this.state.currentComment,
                            username: this.props.loggedUsername
                        })
                    });
                })
                .catch(() => (window as any).Materialize.toast("Try again later!", 3000));
    }

    private renderComments(): JSX.Element[] {
        return this.state.comments.map((comment, index) => this.mapCommentToJsx(comment, index));
    }

    private mapCommentToJsx(userComment: UserComment, key: number): JSX.Element {
        let css = "collection-item";
        css += (key % 2 === 0) ? " even-element" : "";

        return (
            <li key={userComment.username + key} className={css}>
                <span className="username">{userComment.username}</span>
                {userComment.comment}
            </li>
        );
    }
}

function mapDispatchToProps(dispatch: any): DispatchProps {
    return {
        addComment: (pictureUserId: string, pictureId: number, userComment: string) =>
            dispatch(putComment(pictureUserId, pictureId, userComment))
    };
}

function mapStateToProps(state: any): StateProps {
    return {
        loggedUsername: state.login.loggedUser.username
    };
}

export const CommentSection =
    connect<StateProps, DispatchProps, OwnProps>(mapStateToProps, mapDispatchToProps) (CommentSectionComponent);
