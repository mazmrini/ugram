import * as React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { buildUserProfileUrl, buildEditPictureUrl } from "../../app";
import { ProfileChip } from "../profileChip";
import { PictureTag } from "./pictureTag";
import { Picture, likePicture, deletePicture } from "../../actions/pictureActions";
import { toast, ToastContainer } from "react-toastify";
import { CommentSection } from "./commentSection";

interface DispatchProps {
    likePicture: (pictureUser: string, pictureId: number) => any;
    deletePicture: (userId: string, pictureId: number) => any;
}

interface StateProps {
    loggedUsername: string;
}

type OwnProps = Picture & {
    key: number;
    classe?: string;
    shouldDisplayEditButtons: boolean;
    history: any;
};

interface PictureCardState {
    loggedUserLikesIt: boolean;
    likesCount: number;
}

type PictureCardProps = DispatchProps & StateProps & OwnProps;

export class PictureCardComponent extends React.Component<PictureCardProps, PictureCardState> {
    public constructor(props: any) {
        super(props);

        this.state = {
            loggedUserLikesIt: this.props.likes.indexOf(this.props.loggedUsername) > -1,
            likesCount: this.props.likes.length
        };

        this.handleLikeClick = this.handleLikeClick.bind(this);
        this.adjustLikeStateAfterClick = this.adjustLikeStateAfterClick.bind(this);
    }

    public render(): JSX.Element {
        return (
            <div className="picture-card row">
                <div className={this.props.classe}>
                    <div className="card">
                        {this.renderCardImage()}
                        {this.renderCardContent()}
                    </div>
                </div>
                <ToastContainer className="bottom-center" autoClose={5000} hideProgressBar={true}/>
            </div>
        );
    }

    private renderCardContent(): JSX.Element {
        return (
            <div className="card-content">
                {this.renderLikeIcon()}
                <ProfileChip profilePicUrl={this.props.profilePicUrl} userName={this.props.userName} />
                <div>
                    {this.props.description}
                    {this.renderButtons()}
                </div>
                <div className="row picture-tags">
                    {this.renderTags()}
                </div>
                <CommentSection
                    comments={this.props.comments}
                    pictureUserId={this.props.userName}
                    pictureId={this.props.id} />
            </div>
        );
    }

    private renderLikeIcon(): JSX.Element {
        const icon = this.state.loggedUserLikesIt ? "favorite" : "favorite_border";
        const iconColor = this.state.loggedUserLikesIt ? "red-text" : "black-text";
        const css = "col clickable material-icons small disable-selection " + iconColor;

        return (
            <div className="row valign-wrapper">
                <i className={css} onClick={this.handleLikeClick}>{icon}</i>
                {this.renderLikeCount()}
            </div>
        );
    }

    private renderLikeCount(): JSX.Element {
        const likeOrLikes = this.state.likesCount > 1 ? "likes" : "like";

        return (
            <p>{this.state.likesCount} {likeOrLikes}</p>
        );
    }

    private handleLikeClick(): void {
        this.props.likePicture(this.props.userName, this.props.id)
            .then(() => this.adjustLikeStateAfterClick())
            .catch(() => this.notifyOnFail("Failed to like, try again!"));
    }

    private adjustLikeStateAfterClick(): void {
        const likesCountAdjustment = this.state.loggedUserLikesIt ? -1 : 1;

        this.setState({
            loggedUserLikesIt: !this.state.loggedUserLikesIt,
            likesCount: this.state.likesCount + likesCountAdjustment
        });
    }

    private renderCardImage(): JSX.Element {
        return (
            <div className="card-image">
                <img src={this.props.pictureUrl} />
            </div>
        );
    }

    private renderButtons(): JSX.Element {
        if (!(this.isUserPicture() && this.props.shouldDisplayEditButtons))
            return;

        return (
            <span>
                <i className="material-icons right clickable" onClick={() => this.deleteOnClick()}>delete</i>
                <Link to={buildEditPictureUrl(this.props.userName, this.props.id)}>
                    <i className="material-icons right">edit</i>
                </Link>
            </span>);
    }

    private isUserPicture(): boolean {
        return this.props.loggedUsername === this.props.userName;
    }

    private renderTags(): JSX.Element[] {
        return this.props.tags.map((value, index) => this.mapTagToJSX(value, index));
    }

    private mapTagToJSX(tag: string, index: number): JSX.Element {
        return (
            <PictureTag key={index.toString()} tag={tag} />
        );
    }

    private deleteOnClick(): void {
        this.props.deletePicture(this.props.userName, this.props.id)
            .then(() => this.props.history.push(buildUserProfileUrl(this.props.userName)))
            .catch(() => this.notifyOnFail("Failed deleting, try again!"));
    }

    private notifyOnFail(message: string): void {
        toast(message, {
            position: toast.POSITION.BOTTOM_CENTER,
            className: "edit-user toaster",
        });
    }
}

function mapDispatchToProps(dispatch: any): DispatchProps {
    return {
        likePicture: (pictureUser: string, pictureId: number) => dispatch(likePicture(pictureUser, pictureId)),
        deletePicture: (pictureUser: string, pictureId: number) => dispatch(deletePicture(pictureUser, pictureId))
    };
}

function mapStateToProps(state: any): StateProps {
    return {
        loggedUsername: state.login.loggedUser.username
    };
}

export const PictureCard =
    connect<StateProps, DispatchProps, OwnProps>(mapStateToProps, mapDispatchToProps) (PictureCardComponent);
