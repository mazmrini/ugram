import * as React from "react";

interface PictureTagProps {
    tag: string;
    onTagClick?: any;
}

export class PictureTag extends React.Component<PictureTagProps> {

    constructor(props: any) {
        super(props);

        this.handleOnClickFunction = this.handleOnClickFunction.bind(this);
    }

    public render(): JSX.Element {
        return (
            <div className="col">
                <span className="tag-chip grey lighten-4" onClick={this.handleOnClickFunction}>{this.props.tag}</span>
            </div>
        );
    }

    private handleOnClickFunction(event: any): void {
        if (this.props.onTagClick)
            this.props.onTagClick(this.props.tag);
    }
}
