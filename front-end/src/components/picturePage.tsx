import * as React from "react";
import { connect } from "react-redux";
import { buildHomeUrl } from "../app";
import { PictureCard } from "./picture/pictureCard";
import { Picture, fetchPicture } from "../actions/pictureActions";

interface StateProps {
    history: any;
    userIdInUrl: string;
    pictureIdInUrl: number;
    picture: Picture;
    userPic: string;
}

interface DispatchProps {
    fetchPicture: (userId: string, pictureId: number) => any;
}

type PicturePageProps = StateProps & DispatchProps;

class PicturePageComponent extends React.Component<PicturePageProps> {
    public componentDidMount(): void {
        this.props.fetchPicture(this.props.userIdInUrl, this.props.pictureIdInUrl)
            .catch(() => this.redirectToHome());
    }

    public render(): JSX.Element {
        return (
            <PictureCard
                classe="col m4 offset-m4 s10 offset-s1"
                key={this.props.picture.id}
                id={this.props.picture.id}
                pictureUrl={this.props.picture.pictureUrl}
                description={this.props.picture.description}
                userName={this.props.picture.userName}
                profilePicUrl={this.props.picture.profilePicUrl}
                tags={this.props.picture.tags}
                mentions={this.props.picture.mentions}
                likes={this.props.picture.likes}
                comments={this.props.picture.comments}
                shouldDisplayEditButtons={true}
                history={this.props.history}
            />
        );
    }

    private redirectToHome(): void {
        this.props.history.push(buildHomeUrl());
    }
}

function mapDispatchToProps(dispatch: any): DispatchProps {
    return {
        fetchPicture: (userId: string, pictureId: number) => dispatch(fetchPicture(userId, pictureId)),
    };
}

function mapStateToProps(state: any, ownProps: any): StateProps {
    return {
        history: ownProps.history,
        userIdInUrl: ownProps.match.params.userId,
        pictureIdInUrl: ownProps.match.params.pictureId,
        picture: state.picture.picture,
        userPic: state.user.user.pictureUrl,
    };
}

export const PicturePage =
    connect<StateProps, DispatchProps>(mapStateToProps, mapDispatchToProps)(PicturePageComponent);
