import * as React from "react";
import { Link } from "react-router-dom";
import { buildUserProfileUrl } from "../app";

interface ProfileChipProps {
    profilePicUrl: string;
    userName: string;
    className?: string;
}

export class ProfileChip extends React.Component<ProfileChipProps> {
    public render(): JSX.Element {
        const userProfileUrl = buildUserProfileUrl(this.props.userName);

        return (
            <Link to={userProfileUrl} className="profile chip">
                <img src={this.props.profilePicUrl} />
                {this.props.userName}
            </Link>
        );
    }
}
