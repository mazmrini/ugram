import * as React from "react";

interface ProfilePictureChipProps {
    profilePicUrl: string;
    className?: string;
}

export class ProfilePictureChip extends React.Component<ProfilePictureChipProps> {
    public render(): JSX.Element {
        return (
            <img src={this.props.profilePicUrl}
                 className={"circle " + this.props.className}/>
        );
    }
}
