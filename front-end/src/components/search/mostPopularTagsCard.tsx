import * as React from "react";
import { PictureTag } from "../picture/pictureTag";

interface TagsProps {
    tags: string[];
    onClick: any;
}

export class MostPopularTagsCard extends React.Component<TagsProps> {

    public render(): JSX.Element {

        return (
            <div>
                {this.renderBadges()}
           </div>
        );
    }

    private renderBadges(): JSX.Element[] {
        return this.props.tags.map((value, index) => this.mapTagToJSX(value, index));
    }

    private mapTagToJSX(tag: string, index: number): JSX.Element {
        return (
            <PictureTag key={index.toString()} tag={tag} onTagClick={this.props.onClick}/>
        );
    }
}
