import * as React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import * as Autosuggest from "react-autosuggest";
import { fetchAllUsers, UserProfilePic } from "../../actions/usersActions";
import { Picture } from "../../actions/pictureActions";
import { ProfileChip } from "../profileChip";
import { fetchAllPictures, fetchTrendingTags } from "../../actions/picturesActions";
import { buildPictureUrl } from "../../app";
import { MostPopularTagsCard } from "./mostPopularTagsCard";

interface StateProps {
    users: UserProfilePic[];
    pictures: Picture[];
    tags: string[];
    history: any;
}

interface SearchResultProps {
    foundUsers: UserProfilePic[];
    foundPictures: Picture[];
    searchString: string;
    inDescriptions: boolean;
    inHashtags: boolean;
    inUsers: boolean;
}

interface DispatchProps {
    fetchAllUsers: () => any;
    fetchAllPictures: () => any;
    fetchTrendingTags: () => any;
}

type SearchPageProps = DispatchProps & StateProps;

type SearchPageState = StateProps & SearchResultProps & { suggestions: string[] };

const renderSuggestion = (suggestion) => (
    <div>
      {suggestion}
    </div>
  );

class SearchPageComponent extends React.Component<SearchPageProps, SearchPageState> {

    constructor(props: any) {
        super(props);
        this.state = {
            users: this.props.users,
            pictures: this.props.pictures,
            foundPictures: [],
            foundUsers: [],
            searchString: "",
            inDescriptions: true,
            inHashtags: true,
            inUsers: true,
            tags: [],
            suggestions: [],
            history: this.props.history,
        };

        this.handleChangeSearch = this.handleChangeSearch.bind(this);
        this.handleChangeDescriptionsCheckbox = this.handleChangeDescriptionsCheckbox.bind(this);
        this.handleChangeUsersCheckbox = this.handleChangeUsersCheckbox.bind(this);
        this.handleChangeHashtagsCheckbox = this.handleChangeHashtagsCheckbox.bind(this);
        this.handleOnClickTags = this.handleOnClickTags.bind(this);
    }

    public componentDidMount(): void {
        this.props.fetchAllPictures();
        this.props.fetchAllUsers();
        this.props.fetchTrendingTags();
    }

    public componentWillReceiveProps(nextProps: SearchPageProps): void {
        if (this.props.pictures !== nextProps.pictures)
            this.setState({ pictures: nextProps.pictures});
        if (this.props.users !== nextProps.users)
            this.setState({ users: nextProps.users});
    }

    public render(): JSX.Element {

        return (
            <div className="container search-page">
                <div className="row">
                    Top 10 tags:
                    <MostPopularTagsCard tags={this.props.tags} onClick={this.handleOnClickTags}/>
                </div>
                <div className="row">
                    {this.renderSearchInput()}
                </div>

                <div className="row">
                    <p className="center-align">Search in: </p>
                </div>

                <div className="row">
                    <span className="col m4">
                        <input type="checkbox" id="checkbox_users" checked={this.state.inUsers}
                        onChange={this.handleChangeUsersCheckbox}/>
                        <label htmlFor="checkbox_users">Users</label>
                    </span>
                    <span className="col m4">
                        <input type="checkbox" id="checkbox_descriptions" checked={this.state.inDescriptions}
                        onChange={this.handleChangeDescriptionsCheckbox} />
                        <label htmlFor="checkbox_descriptions">Pictures descriptions</label>
                    </span>
                    <span className="col m4">
                        <input type="checkbox" id="checkbox_hashtags" checked={this.state.inHashtags}
                        onChange={this.handleChangeHashtagsCheckbox} />
                        <label htmlFor="checkbox_hashtags">Hashtags</label>
                    </span>
                </div>
                {this.renderUsers()}
                {this.renderPictures()}
           </div>
        );
    }

    private onSuggestionsFetchRequested = ({value}: any) => {
        this.setState({
            suggestions: this.getSuggestions(value)
        });
    }

    private getSuggestions = (value: string) => {
        const sug = [];
        const inputValue = value.trim().toLowerCase();
        const inputLength = inputValue.length;
        this.state.pictures.forEach((pic) => {
            if (pic.description.toLowerCase().slice(0, inputLength) === inputValue)
                sug.push(pic.description);
            pic.tags.forEach((tag) => {
                if (tag.toLowerCase().slice(0, inputLength) === inputValue)
                    sug.push(tag);
            });
        });

        return sug;
    }

    private onSuggestionsClearRequested = () => {
        this.setState({
            suggestions: []
        });
    }

    private getSuggestionValue = (suggestion: string) => suggestion;

    private renderSearchInput(): JSX.Element {
        const value = this.state.searchString;

        const inputProps = {
            placeholder: "Search",
            value,
            onChange: this.handleChangeSearch
        };

        return <Autosuggest suggestions={this.state.suggestions}
                            onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                            onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                            getSuggestionValue={this.getSuggestionValue}
                            renderSuggestion={renderSuggestion}
                            inputProps={inputProps}/>;
    }

    private handleChangeSearch(event: any, { newValue, method }: any): void {
        this.search(newValue);
    }

    private handleOnClickTags(tag: string): void {
        this.search(tag);
   }

    private search(search: string): void {
        let usersResult = [];
        let picturesResult = [];
        if (this.state.inUsers)
            usersResult = this.SearchResultUser(search);
        if (this.state.inDescriptions || this.state.inHashtags)
            picturesResult = this.SearchResultPicture(search, this.state.inDescriptions, this.state.inHashtags);
        this.setState({searchString: search,
                        foundUsers: usersResult,
                        foundPictures: picturesResult});
    }

    private handleChangeUsersCheckbox(event: any): void {
        if (event.target.checked)
            this.setState({inUsers: event.target.checked,
                foundUsers: this.SearchResultUser(this.state.searchString)});

        else this.setState({inUsers: event.target.checked,
            foundUsers: []});
    }

    private handleChangeDescriptionsCheckbox(event: any): void {
        if (event.target.checked)
        this.setState({inDescriptions: event.target.checked,
                foundPictures: this.SearchResultPicture
                (this.state.searchString, event.target.checked, this.state.inHashtags)});
        else
            this.setState({inDescriptions: event.target.checked,
                foundPictures: []});
    }

    private handleChangeHashtagsCheckbox(event: any): void {
        if (event.target.checked)
            this.setState({inHashtags: event.target.checked,
                foundPictures: this.SearchResultPicture
                (this.state.searchString, this.state.inDescriptions, event.target.checked)});
        else
            this.setState({inHashtags: event.target.checked,
                foundPictures: []});
    }

    private SearchResultUser(search: string): UserProfilePic[] {
        const allUsers = this.state.users;
        const result = [];
        if (search.length === 0) return result;
        allUsers.forEach((user) => {
            if (user.id.indexOf(search) !== -1)
                result.push(user);
            }
        );

        return result;
    }

    private SearchResultPicture(search: string, inDescriptions: boolean, inHashtags: boolean): Picture[] {
        const allPictures = this.state.pictures;
        const result = [];

        if (search.length === 0)
            return result;

        allPictures.forEach((pic) => {

            const matchesDescription = inDescriptions && pic.description &&
                    (pic.description.indexOf(search) !== -1);
            const matchesHachtag = inHashtags && (pic.tags.indexOf(search) !== -1);

            if (matchesDescription || matchesHachtag)
                result.push(pic);
        });

        return result;
    }

    private renderUsers(): JSX.Element {
        if (!this.state.inUsers)
            return <div/>;

        return (
            <div>
                <i className="material-icons">people_outline</i>
                <ul className="collection">
                    {this.renderFoundUsers()}
                </ul>
            </div>
        );
    }

    private renderFoundUsers(): JSX.Element[] {
        return this.state.foundUsers.map((value) => this.mapUserToJSX(value));
    }

    private mapUserToJSX(user: UserProfilePic): JSX.Element {
        return (
            <li key={user.id} className="collection-item">
                <ProfileChip profilePicUrl={user.pictureUrl} userName={user.id}/>
            </li>
        );
    }

    private renderPictures(): JSX.Element {
        if (!(this.state.inDescriptions || this.state.inHashtags))
            return <div/>;

        return (
            <div>
                <i className="material-icons">photo_library</i>
                <div className="row">
                    {this.renderFoundPictures()}
                </div>
            </div>
        );
    }

    private renderFoundPictures(): JSX.Element[] {
        return this.state.foundPictures.map((value) => this.mapPictureToJSX(value));
    }

    private mapPictureToJSX(pic: Picture): JSX.Element {
        return (
            <Link to={buildPictureUrl(pic.userName, pic.id)} key={pic.id}>
                <img className="user-picture col m4 offset-m4" src={pic.pictureUrl} />
            </Link>
        );
    }
}

function mapDispatchToProps(dispatch: any): DispatchProps {
    return {
        fetchAllUsers: () => dispatch(fetchAllUsers()),
        fetchAllPictures: () => dispatch(fetchAllPictures()),
        fetchTrendingTags: () => dispatch(fetchTrendingTags())

    };
}

function mapStateToProps(state: any, ownProps: any): StateProps {

    return {
        history: ownProps.history,
        pictures: state.pictures.pictures,
        users: state.users.userList,
        tags: state.tags.tags
    };
}

export const SearchResultPage =
    connect<StateProps, DispatchProps>(mapStateToProps, mapDispatchToProps) (SearchPageComponent);
