import * as React from "react";
import { connect } from "react-redux";
import { UserForm } from "../components/user/userForm";
import { buildHomeUrl } from "../app";
import { UserMetadata } from "../actions/userActions";
import { signUp } from "../actions/signUpActions";

interface DispatchProps {
    signUp: (userMetadata: UserMetadata) => any;
}

interface StateProps {
    history: any;
    isSignUpNeeded: boolean;
}

type SignUpPageProps = StateProps & DispatchProps;

class SignUpPageComponent extends React.Component<SignUpPageProps> {

    public componentDidMount(): void {
        if (!this.props.isSignUpNeeded)
            this.props.history.push(buildHomeUrl());
    }

    public render(): JSX.Element {
        if (!this.props.isSignUpNeeded)
            return <div />;

        return (
            <div className="container full-height valign-wrapper center-align">
                {this.renderSignUpCard()}
            </div>
        );
    }

    private renderSignUpCard(): JSX.Element {
        return (
            <UserForm
                title="Sign up"
                buttonText="Sign up"
                shouldEditUsername={true}
                username="maz"
                firstName="maz"
                lastName="maz"
                phoneNumber="1234567890"
                email="ok@hot.com"
                onSubmit={(metadata) => this.handleSignUpClick(metadata)} />
        );
    }

    private handleSignUpClick(metadata: UserMetadata): void {
        this.props.signUp(metadata)
            .then(() => (window as any).Materialize.toast("Account created!", 3000))
            .then(() => this.props.history.push(buildHomeUrl()))
            .catch((error) => {
                const status = error.response.status;
                let message = "Try again later.";

                if (status === 400)
                    message = "Username already exists.";

                (window as any).Materialize.toast(message, 5000);
            });
    }
}

function mapDispatchToProps(dispatch: any): DispatchProps {
    return {
        signUp: (userMetadata: UserMetadata) => dispatch(signUp(userMetadata))
    };
}

function mapStateToProps(state: any, ownProps: any): StateProps {
    return {
        history: ownProps.history,
        isSignUpNeeded: state.signUp.isSignUpNeeded
    };
}

export const SignUpPage =
    connect<StateProps, DispatchProps>(mapStateToProps, mapDispatchToProps) (SignUpPageComponent);
