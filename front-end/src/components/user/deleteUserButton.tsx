import * as React from "react";
import { connect } from "react-redux";
import { Modal } from "../modal/Modal";
import { deleteUser } from "../../actions/userActions";
import { buildLoginPageUrl } from "../../app";

interface OwnProps {
    history: any;
    userId: string;
}

interface DispatchProps {
    deleteUser: (userId: string) => any;
}

type DeleteUserButtonProps = OwnProps & DispatchProps & {
    shouldShowModal: boolean;
};

type DeleteUserButtonState = DeleteUserButtonProps;

class DeleteUserButtonComponent extends React.Component<DeleteUserButtonProps, DeleteUserButtonState> {
    constructor(props: any) {
        super(props);
        this.state = this.props;

        this.showModal = this.showModal.bind(this);
        this.hideModal = this.hideModal.bind(this);
    }

    public showModal(): void {
        this.setState({shouldShowModal: true});
    }

    public hideModal(): void {
        this.setState({shouldShowModal: false});
    }

    public render(): JSX.Element {
        const modal = this.state.shouldShowModal ? (
            <Modal>
                {this.renderModalContent()}
            </Modal>
        ) : undefined;

        return (
            <span>
                <i className="material-icons user parameter-icon clickable"
                    onClick={this.showModal}>
                    delete_forever
                </i>
                {modal}
            </span>
        );
    }

    private renderModalContent(): JSX.Element {
        return (
            <div className="row">
                <div className="col l4 offset-l4 m6 offset-m3 s8 offset-s2 modal show-modal">
                    <div className="modal-content">
                        <h5>You fo' real doug ?</h5>
                    </div>
                    <div className="modal-footer">
                        <button className="modal-action modal-close waves-effect waves-red btn-flat"
                            onClick={() => this.deleteUser()}>
                            Yes
                        </button>
                        <button className="modal-action modal-close waves-effect waves-green btn-flat"
                            onClick={this.hideModal}>
                            No
                        </button>
                    </div>
                </div>
            </div>
        );
    }

    private deleteUser(): void {
        this.state.deleteUser(this.props.userId)
            .then(() => this.hideModal)
            .then(() => this.props.history.push(buildLoginPageUrl()));
    }
}

function mapDispatchToProps(dispatch: any): DispatchProps {
    return {
        deleteUser: (userId: string) => dispatch(deleteUser(userId))
    };
}

export const DeleteUserButton =
    connect<void, DispatchProps, OwnProps>(undefined, mapDispatchToProps)(DeleteUserButtonComponent);
