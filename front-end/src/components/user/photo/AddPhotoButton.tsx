import * as React from "react";
import { connect } from "react-redux";
import { Modal } from "../../modal/Modal";
import Chips from "react-chips";
import { postPicture, PostPictureMetadata } from "../../../actions/pictureActions";
import { mentionAndTagPattern } from "../../../constants/regexPatterns";
import { WebcamCapture } from "../../webCam";

interface StateProps {
    userId: string;
    shouldShowModal: boolean;
    file: File;
    imagePreviewUrl: any;
    description: string;
    mentions: string[];
    tags: string[];
    showCamera: boolean;
    sizeChecked: boolean;
}

interface DispatchProps {
    addPicture: (picture: PostPictureMetadata) => any;
}

type AddPhotoButtonProps = StateProps & DispatchProps;

type AddPhotoButtonState = AddPhotoButtonProps;

class AddPhotoButtonComponent extends React.Component<AddPhotoButtonProps, AddPhotoButtonState> {
    constructor(props: any) {
        super(props);
        this.state = this.props;

        this.showModal = this.showModal.bind(this);
        this.checkMe = this.checkMe.bind(this);
        this.resetAndCloseModal = this.resetAndCloseModal.bind(this);
        this.mapStateToPictureMetadata = this.mapStateToPictureMetadata.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleFileChange = this.handleFileChange.bind(this);
        this.toggleWebcam = this.toggleWebcam.bind(this);
        this.handleOnCapture = this.handleOnCapture.bind(this);
    }

    public showModal(): void {
        this.setState({shouldShowModal: true});
    }

    public handleChange(event: any): void {
        this.setState({[event.target.name]: event.target.value});
    }

    public handleFileChange(e: any): void {
        e.preventDefault();
        const reader = new FileReader();
        const pfile = e.target.files[0];

        reader.onloadend = () => {
            this.setState({
                file: pfile,
                imagePreviewUrl: reader.result
            });
        };
        reader.readAsDataURL(pfile);
    }

    public onChangeMentions = (mentions: any) => {
        this.setState({mentions});
    }

    public onChangeTags = (tags: any) => {
        this.setState({tags});
    }

    public handleSubmit(event: any): void {
        event.preventDefault();

        if (!this.state.file)
            (window as any).Materialize.toast("Please select a picture.", 2000);

        if (!this.state.sizeChecked)
            (window as any).Materialize.toast("Please select a picture size.", 2000);

        if (!!this.state.file && this.state.sizeChecked) {
            (window as any).Materialize.toast("Upload starting...", 2500);
            this.addPicture();
            this.resetAndCloseModal();
        }
    }

    public render(): JSX.Element {
        const modal = this.state.shouldShowModal ? (
            <Modal>
                {this.renderModalContent()}
            </Modal>
        ) : undefined;

        return (
            <div className="add-photo-btn">
                <div className="btn" onClick={this.showModal}>
                    <i className="large material-icons">add_a_photo</i>
                </div>
                {modal}
            </div>
        );
    }

    private addPicture(): void {
        const pictureMetadata = this.mapStateToPictureMetadata();

        this.state.addPicture(pictureMetadata)
            .then(() => (window as any).Materialize.toast("Picture uploaded.", 2500))
            .catch(() => (window as any).Materialize.toast("Upload failed, try again.", 2500));
    }

    private mapStateToPictureMetadata(): PostPictureMetadata {
        return {
            userId: this.state.userId,
            description: this.state.description,
            file: this.state.file,
            mentions: this.state.mentions,
            tags: this.state.tags,
        };
    }

    private renderModalContent(): JSX.Element {
        return (
            <div className="modal show-modal">
                <form onSubmit={this.handleSubmit}>
                    <div className="modal-content">
                        <h4>Share a new photo</h4>
                        {this.renderPhotoUpload()}
                        {this.renderDivider()}
                        {this.renderPicture()}
                        {this.renderSizeOptions()}
                        {this.renderDescription()}
                        {this.renderMentions()}
                        {this.renderTags()}
                    </div>
                    <div className="modal-footer">
                        <button
                            className="modal-action modal-close waves-effect waves-red btn-flat"
                            onClick={this.resetAndCloseModal}>
                           Cancel
                        </button>
                        <button className="modal-action modal-close waves-effect waves-green btn-flat">
                            <input type="submit" value="Submit"/>
                        </button>
                    </div>
                </form>
            </div>
        );
    }

    private renderSizeOptions(): JSX.Element {
        return (
            <div className="row">
                <p className="col s2">Size: </p>
                <p className="col s3" onClick={() => this.checkMe("radio3")}>
                    <input name="size-choice" type="radio" id="radio3"/>
                    <label html-for="radio3">300x300</label>
                </p>
                <p className="col s3" onClick={() => this.checkMe("radio5")}>
                    <input name="size-choice" type="radio" id="radio5"/>
                    <label html-for="radio5">500x500</label>
                </p>
                <p className="col s3" onClick={() => this.checkMe("radio7")}>
                    <input name="size-choice" type="radio" id="radio7"/>
                    <label html-for="radio7">700x700</label>
                </p>
            </div>
        );
    }

    private checkMe(radioId: string): void {
        this.setState({sizeChecked: true});
        this.uncheckAllRadio();
        (document.getElementById(radioId) as any).defaultChecked = true;
    }

    private uncheckAllRadio(): void {
        (document.getElementById("radio3") as any).defaultChecked = false;
        (document.getElementById("radio5") as any).defaultChecked = false;
        (document.getElementById("radio7") as any).defaultChecked = false;
    }

    private resetAndCloseModal(): void {
        this.setState({
            file: undefined,
            description: "",
            mentions: [],
            tags: [],
            showCamera: false,
            shouldShowModal: false
         });
    }

    private renderPhotoUpload(): JSX.Element {
        return (
            <div>
                <label htmlFor="upload-file-input">Upload your photo</label> <br/>
                <div className="file-field input-field">
                    <div className="btn">
                        <i className="large material-icons">insert_photo</i>
                        <input id="upload-file-input" type="file" name="file" accept="image/*"
                               onChange={this.handleFileChange} />
                    </div>
                    <div className="file-path-wrapper">
                        <input className="file-path validate" type="text"/>
                    </div>
                </div>
                <div className="btn blue" onClick={this.toggleWebcam}>
                     <i className="material-icons">camera_alt</i>
                </div>
                {this.renderWebcam()}
            </div>
        );
    }

    private renderDivider(): JSX.Element {
        return (
            <div>
                <div className="row" />
                <div className="divider" />
                <div className="row" />
            </div>
        );
    }

    private renderDescription(): JSX.Element {
        return (
            <div>
                <label htmlFor="description-input">Description</label>
                <input id="description-input" type="text" name="description" value={this.state.description}
                       onChange={this.handleChange}/>
            </div>
        );
    }

    private renderMentions(): JSX.Element {
        return (
            <div>
                <label htmlFor="mentions-input" data-error={"Please enter a valid mentions"}>Mentions</label>
                <Chips id="mentions-input" type="text" name="mentions"
                       value={this.state.mentions} onChange={this.onChangeMentions} pattern={mentionAndTagPattern}/>
            </div>
        );
    }

    private renderTags(): JSX.Element {
        return (
            <div>
                <label htmlFor="tags-input" data-error={"Please enter a valid tag"}>Tags</label>
                <Chips id="tags-input" type="text" name="tags"
                       value={this.state.tags} onChange={this.onChangeTags} pattern={mentionAndTagPattern}/>
            </div>
        );
    }

    private renderWebcam(): JSX.Element {
        return this.state.showCamera ? <WebcamCapture onCapture={this.handleOnCapture}/> : <div />;
    }

    private renderPicture(): JSX.Element {
        if (!this.state.file)
            return <div />;

        const imageId = "add-picture-preview";
        const reader = new FileReader();

        reader.onloadend = () => {
            const image = document.getElementById(imageId);

            image.setAttribute("src", reader.result);
        };

        reader.readAsDataURL(this.state.file);

        return (
            <div className="row center-align">
                <div className="s12">Preview</div>
                <img height={300} id={imageId} />
            </div>
        );
    }

    private toggleWebcam(): void {
        this.setState({showCamera: !this.state.showCamera});
    }

    private handleOnCapture(imageSource: any): void {
        this.setState({file: imageSource, imagePreviewUrl: "", showCamera: false});
    }
}

function mapDispatchToProps(dispatch: any): DispatchProps {
    return {
        addPicture: (picture: PostPictureMetadata) => dispatch(postPicture(picture))
    };
}

function mapStateToProps(state: any): StateProps {
    return {
        userId: state.login.loggedUser.username,
        shouldShowModal: false,
        file: undefined,
        imagePreviewUrl: "",
        description: "",
        mentions: [],
        tags: [],
        showCamera: false,
        sizeChecked: false
    };
}

export const AddPhotoButton =
    connect<StateProps, DispatchProps>(mapStateToProps, mapDispatchToProps)(AddPhotoButtonComponent);
