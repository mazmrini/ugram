import * as React from "react";
import { Link } from "react-router-dom";
import { Picture } from "../../../actions/pictureActions";
import { buildPictureUrl } from "../../../app";

interface UserPicturesProps {
    pictures: Picture[];
}

export class UserPictures extends React.Component<UserPicturesProps> {

    public render(): JSX.Element {
        return (
            <div className="row">
                <div className="col m8 offset-m2">
                    <div className="flexbin">
                        {this.renderPictures()}
                    </div>
                </div>
            </div>
        );
    }

    private renderPictures(): JSX.Element[] {
        return this.props.pictures.map((value) => this.mapPictureToJSX(value));
    }

    private mapPictureToJSX(pic: Picture): JSX.Element {
        return (
            <Link to={buildPictureUrl(pic.userName, pic.id)} key={pic.id}>
                <img className="user-picture" src={pic.pictureUrl} />
            </Link>
        );
    }
}
