import * as React from "react";
import { buildEditUserUrl, buildHomeUrl } from "../../app";
import { connect } from "react-redux";
import { fetchUserPictures } from "../../actions/picturesActions";
import { Picture } from "../../actions/pictureActions";
import { UserPictures } from "./photo/userPictures";
import { Link } from "react-router-dom";
import { AddPhotoButton } from "./photo/AddPhotoButton";
import { DeleteUserButton } from "./deleteUserButton";
import { fetchUser } from "../../actions/userActions";

interface StateProps {
    history: any;
    userIdInUrl: string;
    username: string;
    firstName: string;
    lastName: string;
    email: string;
    phoneNumber: number;
    pictureUrl: string;
    registrationDate: string;
    loggedUsername: string;
    pictures: Picture[];
}

interface DispatchProps {
    fetchUser: (userId: string) => any;
    fetchUserPictures: (userId: string) => any;
}

type UserProps = StateProps & DispatchProps;

class UserComponent extends React.Component<UserProps> {
    public componentDidMount(): any {
        this.fetchUserPage(this.props.userIdInUrl);
    }

    public componentWillReceiveProps(nextProps: UserProps): any {
        if (this.props.userIdInUrl !== nextProps.userIdInUrl)
            this.fetchUserPage(nextProps.userIdInUrl);
    }

    public render(): JSX.Element {
        return (
            <div>
                <div className="row">
                    <div className="col s10 offset-s1">
                        <div className="row">
                            {this.renderUserPicture()}
                            {this.renderUserInfo()}
                        </div>
                    </div>
                </div>
                <div className="divider"/>
                <UserPictures pictures={this.props.pictures}/>
            </div>
        );
    }

    private fetchUserPage(userId: string): void {
        this.props.fetchUser(userId).catch(() => this.redirectToHome());
        this.props.fetchUserPictures(userId).catch(() => this.redirectToHome());
    }

    private renderUserPicture(): JSX.Element {
        return (
            <div className="col s4 offset-s5 m3 offset-m3 center-align">
                <img src={this.props.pictureUrl} className="profile-image responsive-img circle"/>
            </div>
        );
    }

    private renderUserInfo(): JSX.Element {
        return (
            <div className="col s12 m6 offset-s1 info-margin">
                {this.renderEditButton()}
                <h6 className="user name">{this.props.firstName} {this.props.lastName}</h6>
                <h6 className="user info">{this.props.phoneNumber}</h6>
                <h6 className="user info">{this.props.email}</h6>
                <h6 className="user info">Registration date : {this.props.registrationDate}</h6>
                {this.renderAddPhotoButton()}
            </div>
        );
    }

    private renderAddPhotoButton(): JSX.Element {
        if (this.isRequestUserLogged())
            return <AddPhotoButton/>;
    }

    private renderEditButton(): JSX.Element {
        let banner: JSX.Element = <h6 className="user username">{this.props.username}</h6>;

        if (this.isRequestUserLogged())
            banner = (
                <h6 className="user username-edit username">{this.props.username}
                    <Link id="" to={buildEditUserUrl(this.props.username)}>
                        <i className="material-icons user parameter-icon">edit</i>
                    </Link>
                    <DeleteUserButton userId={this.props.username} history={this.props.history} />
                </h6>
            );

        return banner;
    }

    private isRequestUserLogged(): boolean {
        return this.props.username === this.props.loggedUsername;
    }

    private redirectToHome(): void {
        this.props.history.push(buildHomeUrl());
    }
}

function mapDispatchToProps(dispatch: any): DispatchProps {
    return {
        fetchUser: (userId: string) => dispatch(fetchUser(userId)),
        fetchUserPictures: (userId: string) => dispatch(fetchUserPictures(userId))
    };
}

function mapStateToProps(state: any, ownProps: any): StateProps {
    const user = state.user.user;

    return {
        history: ownProps.history,
        userIdInUrl: ownProps.match.params.userId,
        username: user.id,
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
        phoneNumber: user.phoneNumber,
        pictureUrl: user.pictureUrl,
        registrationDate: user.registrationDate,
        loggedUsername: state.login.loggedUser.username,
        pictures: state.user.userPictures
    };
}

export const User = connect<StateProps, DispatchProps>(mapStateToProps, mapDispatchToProps)(UserComponent);
