import * as React from "react";
import { UserMetadata } from "../../actions/userActions";
import { emailPattern, namePattern, phonePattern, usernamePattern } from "../../constants/regexPatterns";

interface UserFormProps {
    title: string;
    buttonText: string;
    username: string;
    shouldEditUsername: boolean;
    firstName: string;
    lastName: string;
    email: string;
    phoneNumber: string;
    onSubmit: (metadata: UserMetadata) => any;
}

type UserFormState = UserFormProps;

export class UserForm extends React.Component<UserFormProps, UserFormState> {
    constructor(props: any) {
        super(props);
        this.state = this.props;
        this.handleChange = this.handleChange.bind(this);
    }

    public render(): JSX.Element {
        return (
            <div className="row">
                <div className="col s6 offset-s3">
                    <h4 className="center-align blue-grey-text edit-user title">{this.props.title}</h4>
                </div>
                <div className="row">
                    {this.renderUsernameInput()}
                    {this.renderFirstNameInput()}
                    {this.renderLastNameInput()}
                    {this.renderPhoneNumberInput()}
                    {this.renderEmailInput()}
                </div>
                <div className="row">
                    {this.renderSubmitButton()}
                </div>
            </div>
        );
    }

    private handleChange(event: any): void {
        this.setState({[event.target.name]: event.target.value});
    }

    private renderUsernameInput(): JSX.Element {
        if (!this.state.shouldEditUsername)
            return;

        return this.renderInput("account_circle", this.state.username, "username", "Username", "text",
            "icon_prefix", "Please enter a valid username", usernamePattern);
    }

    private renderFirstNameInput(): JSX.Element {
        return this.renderInput("account_circle", this.state.firstName, "firstName", "First name", "text",
            "icon_prefix", "Please enter a valid first name", namePattern);
    }

    private renderLastNameInput(): JSX.Element {
        return this.renderInput("account_circle", this.state.lastName, "lastName", "Last name", "text",
            "icon_prefix", "Please enter a valid last name", namePattern);
    }

    private renderPhoneNumberInput(): JSX.Element {
        return this.renderInput("phone", this.state.phoneNumber.toString(), "phoneNumber", "Phone", "text",
            "icon_telephone", "Please enter a valid phone number (xxxxxxxxxx)", phonePattern);
    }

    private renderEmailInput(): JSX.Element {
        return this.renderInput("email", this.state.email, "email", "Email", "text", "icon_telephone",
            "Please enter a valid email address", emailPattern);
    }

    private renderInput(icon: string, defaultValue: string, name: string,
                        label: string, type: string, id: string, dataError: string, pattern: string): JSX.Element {
        return (
            <div className="input-field col s6 offset-s3 ">
                <i className="material-icons blue-grey-text prefix">{icon}</i>
                <input id={id} type={type} className="validate" defaultValue={defaultValue}
                       name={name} pattern={pattern}
                       onChange={this.handleChange}/>
                <label className="active" data-error={dataError}>{label}</label>
            </div>
        );
    }

    private renderSubmitButton(): JSX.Element {
        return (
            <div className="col s6 offset-s3">
                <button className="right btn waves-effect waves-light blue-grey" onClick={() => this.onSubmit()}>
                    {this.props.buttonText}
                </button>
            </div>
        );
    }

    private onSubmit(): void {
        this.state.onSubmit({
            username: this.state.username,
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            email: this.state.email,
            phoneNumber: Number(this.state.phoneNumber)
        });
    }
}
