import * as React from "react";
import { connect } from "react-redux";
import { UserProfilePic, fetchAllUsers } from "../actions/usersActions";
import { ProfileChip } from "./profileChip";

interface StateProps {
    history: any;
    users: UserProfilePic[];
}

interface DispatchProps {
    fetchUsers: () => void;
}

type UserListProps = StateProps & DispatchProps;

class UserListComponent extends React.Component<UserListProps> {
    public componentDidMount(): any {
        this.props.fetchUsers();
    }

    public render(): JSX.Element {
        return (
            <div className="container user-list">
                <ul className="collection with-header">
                    {this.renderHeader()}
                    {this.renderUsers()}
                </ul>
            </div>
        );
    }

    private renderHeader(): JSX.Element {
        return (
            <li className="collection-header">
                <h2>Users</h2>
            </li>
        );
    }

    private renderUsers(): JSX.Element[] {
        return this.props.users.map((value) => this.mapUserToJSX(value));
    }

    private mapUserToJSX(user: UserProfilePic): JSX.Element {
        return (
            <li key={user.id} className="collection-item">
                <ProfileChip profilePicUrl={user.pictureUrl} userName={user.id}/>
            </li>
        );
    }
}

function mapDispatchToProps(dispatch: any): DispatchProps {
    return {
        fetchUsers: () => dispatch(fetchAllUsers())
    };
}

function mapStateToProps(state: any, ownProps: any): StateProps {
    return {
        history: ownProps.history,
        users: state.users.userList
    };
}

export const UserListPage =
    connect<StateProps, DispatchProps>(mapStateToProps, mapDispatchToProps) (UserListComponent);
