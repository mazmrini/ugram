import * as React from "react";
import * as Webcam from "react-webcam";

interface WebcamCaptureProps {
    onCapture: any;
}

export class WebcamCapture extends React.Component<WebcamCaptureProps> {
    private webcam: Webcam;

    public render(): JSX.Element {
        const webcam = !!(window as any).chrome ? this.renderChromeWarning() : this.renderWebcam();
        const button = !!(window as any).chrome ? <span /> : this.renderCaptureButton();

        return (
            <div className="row">
                <div className="center-align">
                {webcam}
                </div>
                {button}
            </div>
        );
    }

    private renderWebcam(): JSX.Element {
        return (
            <Webcam
                audio={false}
                ref={this.setRef}
                screenshotFormat="image/jpeg"
                width={450}
                height={400}
            />
        );
    }

    private renderChromeWarning(): JSX.Element {
        return (
            <img src="https://s3.amazonaws.com/ugramteam10/Screenshot+at+2018-04-18+17%3A33%3A49.png" />
        );
    }

    private renderCaptureButton(): JSX.Element {
        return (
            <div className="center-align">
                <div className="btn blue center-align" onClick={this.capture}>Capture photo</div>
            </div>
        );
    }

    private setRef = (webcam: any) => {
        this.webcam = webcam;
    }

    private capture = () => {
        const imageSrc = this.webcam.getScreenshot();
        const blob = dataURItoBlob(imageSrc);
        this.props.onCapture(blob);
        (window as any).Materialize.toast("Photo captured!", 5000);
    }
}

function dataURItoBlob(dataURI: any): any {
    const byteString = atob(dataURI.split(",")[1]);
    const ab = new ArrayBuffer(byteString.length);
    const ia = new Uint8Array(ab);

    for (let i = 0; i < byteString.length; i++)
        ia[i] = byteString.charCodeAt(i);

    return new Blob([ab], { type: "image/jpeg"});
}
