export const RECEIVED_ALL_USERS = "RECEIVED_ALL_USERS";

export const RECEIVED_ALL_PICTURES = "RECEIVED_ALL_PICTURES";

export const RECEIVED_PICTURE = "RECEIVED_PICTURE";
export const DELETED_PICTURE = "DELETED_PICTURE";
export const ADDED_PICTURE = "ADDED_PICTURE";
export const EDITED_PICTURE = "EDITED_PICTURE";

export const USER_FETCHED = "USER_FETCHED";
export const USER_EDITED = "USER_EDITED";
export const USER_DELETED = "USER_DELETED";
export const USER_PICTURES_RECEIVED = "USER_PICTURES_RECEIVED";

export const TAGS_RECEIVED = "TAGS_RECEIVED";

export const ADD_ACCESS_TOKEN = "ADD_ACCESS_TOKEN";
export const LOGIN_SUCCESSFUL = "LOGIN_SUCCESSFUL";
export const LOGOUT_SUCCESSFUL = "LOGOUT";

export const SIGN_UP_NEEDED = "SIGN_UP_NEEDED";
export const SIGN_UP_SUCCESSFUL = "SIGN_UP_SUCCESSFUL";

export const RECEIVED_NOTIFICATIONS = "RECEIVED_NOTIFICATIONS";
export const DELETE_NOTIFICATIONS = "DELETE_NOTIFICATIONS";
