export const usernamePattern = "^\\w+$";
export const namePattern = "^([ \u00c0-\u01ffa-zA-Z'\\-])+$";
export const phonePattern = "^\\d{10}$";
export const emailPattern = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:" +
    "[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
export const mentionAndTagPattern = "^\\w+$";
