import * as React from "react";
import * as ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { App } from "./app";
import { setAxiosContentType } from "./utils/axiosConfig";
import appStore from "./store";

import "../lib/flexbin-css";
import "../lib/materialize-css";
import "../scss/app.scss";

setAxiosContentType("application/json");

ReactDOM.render(
    <Provider store={appStore}>
        <App/>
    </Provider>,
    document.getElementById("app")
);
