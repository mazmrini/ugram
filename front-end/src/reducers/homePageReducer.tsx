import { RECEIVED_ALL_PICTURES, LOGOUT_SUCCESSFUL } from "../constants/actionTypes";
import { Picture } from "../actions/pictureActions";

interface HomePageReducerState {
    pictures: Picture[];
}

const defaultState = {
    pictures: []
};

export default function reducer(state: HomePageReducerState = defaultState, action: any): HomePageReducerState {
    switch (action.type) {
        case RECEIVED_ALL_PICTURES: {
            return {...state,
                pictures: action.payload
            };
        }
        case LOGOUT_SUCCESSFUL: {
            return defaultState;
        }
        default: {
            return state;
        }
    }
}
