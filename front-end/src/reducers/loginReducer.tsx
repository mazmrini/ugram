import { ADD_ACCESS_TOKEN,
    LOGIN_SUCCESSFUL,
    LOGOUT_SUCCESSFUL,
    USER_DELETED } from "../constants/actionTypes";
import { ACCESS_TOKEN_KEY, LOGO_URL, HOME_LOGO_URL } from "../constants/constants";
import { LoggedUser } from "../actions/loginActions";
import { setAxiosAuthHeader, removeAxiosAuthHeader } from "../utils/axiosConfig";

interface LoginReducerState {
    isLogged: boolean;
    loggedUser: LoggedUser;
    token: string;
    logoUrl: string;
    homeLogoUrl: string;
}

const defaultState = {
    isLogged: false,
    loggedUser: {
        username: "",
        profilePicUrl: ""
    },
    token: "",
    logoUrl: LOGO_URL,
    homeLogoUrl: HOME_LOGO_URL
};

export default function reducer(state: LoginReducerState = defaultState, action: any): LoginReducerState {
    switch (action.type) {
        case ADD_ACCESS_TOKEN: {
            addAuthToken(action.payload);

            return {
                ...state,
                token: action.payload
            };
        }
        case LOGIN_SUCCESSFUL: {
            return {
                ...state,
                isLogged: true,
                loggedUser: action.payload
            };
        }
        case USER_DELETED:
        case LOGOUT_SUCCESSFUL:
            removeAuthToken();

            return defaultState;
        default:
            return state;
    }
}

function addAuthToken(token: string): void {
    localStorage.setItem(ACCESS_TOKEN_KEY, token);

    const oauthToken = "Bearer " + token;
    setAxiosAuthHeader(oauthToken);
}

function removeAuthToken(): void {
    localStorage.removeItem(ACCESS_TOKEN_KEY);

    removeAxiosAuthHeader();
}
