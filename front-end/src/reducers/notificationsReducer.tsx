import { RECEIVED_NOTIFICATIONS, LOGOUT_SUCCESSFUL, DELETE_NOTIFICATIONS } from "../constants/actionTypes";

interface NotificationsReducerState {
    notifications: string[];
}
const defaultState = {
    notifications: []
};

export default function reducer(state: NotificationsReducerState = defaultState, action: any):
                                                                                    NotificationsReducerState {
    switch (action.type) {
        case RECEIVED_NOTIFICATIONS: {
            return {...state,
                notifications: state.notifications.concat(action.payload)
            };
        }
        case DELETE_NOTIFICATIONS:
        case LOGOUT_SUCCESSFUL: {
            return defaultState;
        }
        default: {
            return state;
        }
    }
}
