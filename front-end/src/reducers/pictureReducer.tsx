import { RECEIVED_PICTURE, LOGOUT_SUCCESSFUL } from "../constants/actionTypes";
import { Picture } from "../actions/pictureActions";

interface PictureReducerState {
    picture: Picture;
}

const defaultPicture = {
    picture: {
        id: 0,
        profilePicUrl: "",
        pictureUrl: "",
        description: "",
        userName: "",
        tags: [],
        mentions: [],
        likes: [],
        comments: []
    }
};

export default function reducer(state: PictureReducerState = defaultPicture, action: any): PictureReducerState {
    switch (action.type) {
        case RECEIVED_PICTURE: {
            return {...state,
                picture: action.payload
            };
        }
        case LOGOUT_SUCCESSFUL:
            return defaultPicture;
        default: {
            return state;
        }
    }
}
