import { combineReducers } from "redux";
import user from "./userReducer";
import login from "./loginReducer";
import signUp from "./signUpReducer";
import users from "./usersReducer";
import pictures from "./homePageReducer";
import picture from "./pictureReducer";
import notifications from "./notificationsReducer";
import tags from "./searchPageReducer";

const rootReducer = combineReducers({
    user,
    users,
    pictures,
    login,
    signUp,
    picture,
    tags,
    notifications
});

export default rootReducer;
