import { TAGS_RECEIVED, LOGOUT_SUCCESSFUL } from "../constants/actionTypes";

interface SearchPageReducerState {
    tags: string[];
}

const defaultState = {
    tags: []
};

export default function reducer(state: SearchPageReducerState = defaultState, action: any): SearchPageReducerState {
    switch (action.type) {
        case TAGS_RECEIVED: {
            return {...state,
                tags: action.payload
            };
        }
        case LOGOUT_SUCCESSFUL: {
            return defaultState;
        }
        default: {
            return state;
        }
    }
}
