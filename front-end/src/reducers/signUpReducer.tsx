import { SIGN_UP_NEEDED, SIGN_UP_SUCCESSFUL, LOGOUT_SUCCESSFUL } from "../constants/actionTypes";

interface SignUpReducerState {
    isSignUpNeeded: boolean;
}

const defaultState = {
    isSignUpNeeded: false
};

export default function reducer(state: SignUpReducerState = defaultState, action: any): SignUpReducerState {
    switch (action.type) {
        case SIGN_UP_NEEDED: {
            return {
                ...state,
                isSignUpNeeded: true
            };
        }
        case SIGN_UP_SUCCESSFUL: {
            return {
                ...state,
                isSignUpNeeded: false
            };
        }
        case LOGOUT_SUCCESSFUL: {
            return defaultState;
        }
        default:
            return state;
    }
}
