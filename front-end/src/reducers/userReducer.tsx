import { USER_FETCHED,
    USER_EDITED,
    USER_PICTURES_RECEIVED,
    LOGOUT_SUCCESSFUL } from "../constants/actionTypes";

interface UserReducerState {
    user: any;
    userPictures: any[];
}

const defaultState = {
    user: {},
    userPictures: []
};

export default function reducer(state: UserReducerState = defaultState, action: any): UserReducerState {
    switch (action.type) {
        case USER_FETCHED: {
            return {
                ...state,
                user: action.payload,
            };
        }
        case USER_EDITED: {
            return {
                ...state,
                user: action.payload,
            };
        }
        case USER_PICTURES_RECEIVED: {
            return {...state,
                userPictures: action.payload
            };
        }
        case LOGOUT_SUCCESSFUL: {
            return defaultState;
        }
        default: {
            return state;
        }
    }
}
