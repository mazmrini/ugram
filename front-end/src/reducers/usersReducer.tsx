import { RECEIVED_ALL_USERS, LOGOUT_SUCCESSFUL } from "../constants/actionTypes";

interface UsersReducerState {
    userList: any[];
    userToProfilePicture: {};
}

const defaultState = {
    userList: [],
    userToProfilePicture: {}
};

export default function reducer(state: UsersReducerState = defaultState, action: any): UsersReducerState {
    switch (action.type) {
        case RECEIVED_ALL_USERS: {
            return {
                ...state,
                userList: action.payload.users,
                userToProfilePicture: action.payload.userToProfilePicture
            };
        }
        case LOGOUT_SUCCESSFUL: {
            return defaultState;
        }
        default: {
            return state;
        }
    }
}
