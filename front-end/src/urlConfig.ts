const baseUrl = "http://ugram-team-10-prod.us-east-1.elasticbeanstalk.com";

function buildUrl(endpoint: string): string {
    return baseUrl + endpoint;
}

export function buildLoginUrl(): string {
    return buildUrl("/login");
}

export function buildSignUpUrl(): string {
   return buildUrl("/sign-up");
}

export function buildGetUsersUrl(): string {
   return buildUrl("/users?perPage=15000");
}

export function buildGetUserUrl(userId: string): string {
   return buildUrl("/users/" + userId);
}

export function buildPutUserUrl(userId: string): string {
    return buildUrl("/users/" + userId);
}

export function buildDeleteUserUrl(userId: string): string {
    return buildUrl("/users/" + userId);
}

export function buildPostPictureUrl(userId: string): string {
    return buildUrl("/users/" + userId + "/pictures");
}

export function buildGetUserPicturesUrl(userId: string): string {
    return buildUrl("/users/" + userId + "/pictures");
}

export function buildGetPicturesUrl(): string {
    return buildUrl("/pictures?perPage=15000");
}

function buildPictureEndpoint(userId: string, pictureId: number): string {
    return buildUrl("/users/" + userId + "/pictures/" + pictureId);
}

export function buildGetPictureUrl(userId: string, pictureId: number): string {
    return buildPictureEndpoint(userId, pictureId);
}

export function buildPutPictureUrl(userId: string, pictureId: number): string {
    return buildPictureEndpoint(userId, pictureId);
}

export function buildDeletePictureUrl(userId: string, pictureId: number): string {
    return buildPictureEndpoint(userId, pictureId);
}

export function buildPutPictureLikeUrl(userId: string, pictureId: number): string {
    return buildPictureEndpoint(userId, pictureId) + "/like";
}

export function buildPutPictureCommentUrl(userId: string, pictureId: number): string {
    return buildPictureEndpoint(userId, pictureId) + "/comment";
}

export function buildNotificationsUrl(userId: string): string {
    return buildUrl("/notification/" + userId);
}

export function buildGetPicturesTrendingTagsUrl(): string {
    return buildUrl("/pictures/tags");
}
