import axios from "axios";

const AUTH_KEY = "Authorization";
const CONTENT_TYPE_KEY = "Content-Type";

export function setAxiosAuthHeader(value: string): void {
    setAxiosHeadersDefault(AUTH_KEY, value);
}

export function setAxiosContentType(value: string): void {
    setAxiosHeadersDefault(CONTENT_TYPE_KEY, value);
}

export function removeAxiosAuthHeader(): void {
    removeAxiosHeadersDefault(AUTH_KEY);
}

function removeAxiosHeadersDefault(key: string): void {
    delete axios.defaults.headers.get[key];
    delete axios.defaults.headers.post[key];
    delete axios.defaults.headers.put[key];
    delete axios.defaults.headers.delete[key];
}

function setAxiosHeadersDefault(key: string, value: string): void {
    axios.defaults.headers.get[key] = value;
    axios.defaults.headers.post[key] = value;
    axios.defaults.headers.put[key] = value;
    axios.defaults.headers.delete[key] = value;
}
