export class InputFormatter {

    public static formatDateToMMDDYYYY(date: string): string {
        return new Date(date).toLocaleDateString();
    }

    public static formatUrlPictureToGetLargePicture(pictureUrl: string): string {
        return pictureUrl + this.pictureType;
    }

    private static readonly pictureType: string = "?type=large";
}
