const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const CompressionPlugin = require("compression-webpack-plugin")
const merge = require('webpack-merge');
const common = require('./webpack.common.js');
var webpack = require('webpack');

module.exports = merge(common, {
    devtool: 'source-map',
    output: {
        filename: './js/[name].min.js'
    },
    plugins: [
        new UglifyJsPlugin({
            include: /\.min\.js$/,
            parallel: 4,
            sourceMap: true
        }),
        new CompressionPlugin({
            include: /\.min\.js$/,
            algorithm: 'gzip',
            deleteOriginalAssets: true
        }),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('production')
        })
    ]
});